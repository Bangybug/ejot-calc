import 'package:ejot_ru/data/AppLocalizations.dart';
import 'package:ejot_ru/model/AppStore.dart';
import 'package:ejot_ru/model/Block.dart';
import 'package:ejot_ru/ui/widgets/Dropdown.dart';
import 'package:ejot_ru/util/misc.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import '../theme.dart';
import 'BaseBlockEditorPage.dart';

List<int> ddValues = [2, 6, 11, 16, 24, 40, 70];

int dishDistanceToIndex(double dishDistance) {
  int ddIndex;
  if (dishDistance != null) {
    ddIndex = ddValues.length;
    for (int i = 0; i < ddValues.length; ++i) {
      if (dishDistance < ddValues[i]) {
        ddIndex = i;
        break;
      }
    }
  }
  return ddIndex;
}

double indexToDishDistance(int ddIndex) {
  switch (ddIndex) {
    case 0:
      return 1;
    case 1:
      return 4;
    case 2:
      return 8;
    case 3:
      return 13;
    case 4:
      return 20;
    case 5:
      return 32;
    case 6:
      return 55;
    case 7:
      return 72;
    default:
      return null;
  }
}

class BlockInsulatorEditorPage extends StatefulWidget {
  BlockInsulatorEditorPage(this.args) : super();
  final BlockEditorArguments args;

  @override
  State createState() => _BlockInsulatorEditorPage(args);
}

class _BlockInsulatorEditorPage extends BaseBlockPageState {
  _BlockInsulatorEditorPage(BlockEditorArguments args) : super(args);

  TextEditingController ctlBreakForceEjotInput;
  TextEditingController ctlBreakForceOtherInput;
  TextEditingController ctlBaseAnchorsAvgInput;

  @override
  bool needActionMenu() {
    return false;
  }

  @override
  Widget buildTopHint(BuildContext context) {
    return null;
  }

  String ddLabel(BuildContext context, int ddIndex) {
    final loc = AppLocalizations.of(context);
    if (ddIndex == null) return null;

    if (ddIndex == 0) {
      return '0 — ' + ddValues[0].toString() + loc.getString('mm');
    } else if (ddIndex == ddValues.length) {
      return loc.getString('from') +
          ' ' +
          ddValues.last.toString() +
          ' ' +
          loc.getString('mm');
    } else {
      return ddValues[ddIndex - 1].toString() +
          ' — ' +
          ddValues[ddIndex].toString() +
          ' ' +
          loc.getString('mm');
    }
  }

  List<DropdownMenuItem> _dropdownItems() {
    return List.generate(
        ddValues.length + 1,
        (int index) => DropdownMenuItem(
            value: index, child: Text(ddLabel(context, index))));
  }

  Widget _buildDishDistanceDropdown(BuildContext context) {
    final AppStore appStore = args.appStore;
    final loc = AppLocalizations.of(context);

    int ddIndex = dishDistanceToIndex(appStore.dishDistance);

    return Observer(builder: (context) {
      return Dropdown(
          value: ddIndex,
          dropdownName: loc.getString('select'),
          items: _dropdownItems(),
          valueToLabel: (value, ctx) => ddLabel(ctx, value),
          onChanged: (indexValue) {
            appStore.dishDistance = indexToDishDistance(indexValue);
            ++args.appStore.lastUpdatedCount;
          });
    });
  }

  Widget _wrapPanel(Widget child) {
    return Container(
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            new BoxShadow(
                color: Theme.of(context).buttonColor,
                offset: new Offset(0.0, 0.0),
                blurRadius: 20.0,
                spreadRadius: 10.0)
          ],
          shape: BoxShape.rectangle,
        ),
        child: child);
  }

  @override
  Widget buildGlobalParams(BuildContext context) {
    final AppStore appStore = args.appStore;
    final loc = AppLocalizations.of(context);

    if (null == ctlBreakForceEjotInput) {
      ctlBreakForceEjotInput = TextEditingController(
          text: appStore.breakForceEjot == null
              ? ''
              : appStore.breakForceEjot.toString());
    }

    if (null == ctlBreakForceOtherInput) {
      ctlBreakForceOtherInput = TextEditingController(
          text: appStore.breakForceOther == null
              ? ''
              : appStore.breakForceOther.toString());
    }

    if (null == ctlBaseAnchorsAvgInput) {
      ctlBaseAnchorsAvgInput = TextEditingController(
          text: appStore.avgPerm2NFS == null
              ? ''
              : appStore.avgPerm2NFS.toString());
    }

    return _wrapPanel(
        Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
      Container(
          padding: EdgeInsets.only(left: 10, right: 10, top: 10),
          child: Text(loc.getString(BlockBinding.INSULATION_BINDING.toString()),
              style: TextStyle(fontWeight: FontWeight.bold),
              textScaleFactor: 1.1)),
      // FYI observer does not work unless there is read-access to at least some variable that has been changed since last observation
      // I use lastUpdatedCount, but technically I'm observing appStore.anchorParam.
      Observer(builder: (context) {
        if (appStore.facade == FacadeType.SFTK) {
          return Container(
              padding: EdgeInsets.only(left: 10, right: 10),
              child: buildNumericInput(
                context,
                loc.getString('pBreakForceEjot'),
                ctlBreakForceEjotInput,
                (text) {
                  appStore.breakForceEjot = stringToDouble(text);
                  ++appStore.lastUpdatedCount;
                },
                suffixText: loc.getString('kN'), needBorder: true, indent: 4
              ));
        }
        return Container();
      }),
      Observer(builder: (context) {
        if (appStore.facade == FacadeType.SFTK) {
          return Container(
              padding: EdgeInsets.only(left: 10, right: 10),
              child: buildNumericInput(
                context,
                loc.getString('pBreakForce') + ' - ' + loc.getString('otherAnchor'),
                ctlBreakForceOtherInput,
                (text) {
                  appStore.breakForceOther = stringToDouble(text);
                  ++appStore.lastUpdatedCount;
                },
                suffixText: loc.getString('kN'), needBorder: true, indent: 4
              ));
        }
        return Container();
      }),
      Container(
          padding: EdgeInsets.only(left: 10, right: 10, top: 16),
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(loc.getString('pDishDistance')),
                _buildDishDistanceDropdown(context)
              ])),
      Observer(builder: (context) {
        if (appStore.facade == FacadeType.NFS) {
          return Container(
              padding: EdgeInsets.only(left: 10, right: 10),
              child: buildNumericInput(context, loc.getString('pAvgPerMeterSq'),
                  ctlBaseAnchorsAvgInput, (text) {
                appStore.avgPerm2NFS = int.tryParse(text);
                appStore.lastUpdatedCount++;
              }, wrapLength: 40, needBorder: true, indent: 4));
        }
        return Container();
      }),
    ]));
  }
}
