const int STEPS_TOTAL = 5;
const int BUILDINGTYPES_TOTAL = 12;

Map<String, Map<String, String>> initValues() {
  Map<String, Map<String, String>> localizedValues = {
    'en': {
      // units
      'mm': 'mm',
      'm': 'm',
      '1/m': '1/m',
      '1/m2': '1/m²',
      'm2C/W': 'm²°C/W',
      'pcs/m': 'pcs/m',
      'pcs/m2': 'pcs/m²',
      'pcs': 'pcs',
      'kN': 'kN',
      'W/m2C': 'W/(m²°C)',
      'm3': 'm³',
      'm2': 'm²',
      'm/m2': 'm/m²',
      'm2/m2': 'm²/m²',
      'Gcal/year': 'GCal/year',
      'kg/m3': 'kg/m³',
      //

      'title': 'Anchor calculator',

      'step1': 'General properties',
      'step2': 'Facade type',
      'step3': 'Walls',
      'step4': 'Economic values',
      'step5': 'Check results',

      'selectCity': 'Select city',

      'stepsListTitle': 'Describe your building',
      'nextStep': 'step',
      'stepFrom': 'of',

      'buildingType': 'Select building type...',
      'buildingTypeExplain':
          'Building type defines characteristics as described Thermal protection standard SP 50.13330.2012, Figure 3.',

      'searchCity': 'Search city...',
      'inputDescription': 'Input description...',
      'cityExplain': 'City is used to match SP 131.13330.2012 standard',
      'cityNotFound': 'City not found, try neighboring one from the list',

      'innerTemperature': 'Temperature inside building',
      'innerHumidity': 'Humidity inside building',
      'climateExplain': 'Check GOST 30494-2011 for microclimate requirements',

      'select': 'Select...',
      'clearMaterial': 'Clear material',
      'areYouSure': 'Please confirm to clear material.',
      'confirmRemove': 'Clear',
      'dontRemove': 'Dont\'t clear',

      'FacadeType.SFTK': 'ETICS',
      'FacadeType.NFS': 'RVF',
      'desc_FacadeType.SFTK': 'External thermal insulation composite system',
      'desc_FacadeType.NFS': 'Rear ventilated facade',
      'mtlWallCore': 'Wall core material',
      'mtlInsulation': 'Thermal insulation material',
      'mtlAnchor': 'Insulation binding anchor',

      'baseWidth': 'Fixed wall core thickness, mm',
      'insulatorWidthExternal': 'External insulator thickness, mm',
      'insulatorWidth': 'Insulator thickness, mm',
      'inputInsulatorWidth': 'Input insulator thickness, mm',
      'calculateInsulatorWidth': 'Calculate insulator thickness, mm',
      'breakForce': 'Input break force, kN',

      'sftkClass': 'Select ETICS duty class',
      'sftkClass0': 'Heavy duty class',
      'sftkClass1': 'Middle class',
      'sftkClass2': 'Low duty class',
      'sftkClassLabel0':
          'ETICS top class for buildings with heavy duty requirements',
      'sftkClassLabel1':
          'ETICS middle class for buildings with moderate duty requirements',
      'sftkClassLabel2':
          'ETICS lower class for buildings with low duty requirements',

      'terrainType': 'Select terrain type',
      'terrainType0': 'Type A',
      'terrainType1': 'Type B',
      'terrainType2': 'Type C',
      'terrainTypeLabel0':
          'Terrains with large open areas such as seas and fields, deserts',
      'terrainTypeLabel1':
          'Terrains with obstacles taller than 10m, cities, forests',
      'terrainTypeLabel2': 'Terrains with dense obstacles taller than 25m',

      'buildingLength': 'Input building length, m',
      'buildingWidth': 'Input building width, m',
      'buildingHeight': 'Input building height, m',
      'buildingFacadeSquare': 'Facade square area, m²',
      'facadeNoHoles': 'Effective facade square (without gaps), m²',

      'selectMaterial': 'Select material',
      'customMaterials': 'Your custom materials',
      'createMaterial': 'Create custom material',
      'inputMaterialName': 'Input material name',
      'searchMaterial': 'Search material...',
      'materialNotFound': 'Not found',
      'selectThisMaterial': 'Select material',

      'fillBlockHint':
          'Please fill one or more elements if you have them in your building. This will affect calculation results.',
      'addMore': 'Add more',

      'editBlock': 'Edit element',
      'addNewElement': 'Add new element',
      'addNewBlock': 'Add new element',
      'removeThisBlock': 'Remove this element',
      'filledElementsNumber': 'Filled elements',

      'BlockBinding.INSULATION_BINDING': 'Insulation binding anchor',
      'BlockBinding.BALCONY_BINDING': 'Balcony plate binding',
      'BlockBinding.ANGLE_BINDING': 'Angle binding',
      'BlockBinding.WINDOW_BINDING': 'Window adjacent binding',
      'BlockBinding.BASEMENT_BINDING': 'Basement binding',
      'BlockBinding.UNIVERSAL': 'Custom binding',
      'BlockBinding.BRACKET_AL_BINDING': 'Aluminium brackets',
      'BlockBinding.BRACKET_STEEL_BINDING': 'Steel brackets with rail',
      'BlockBinding.FIRESTOP_BINDING': 'Firestopper steel binding',

      'pInsulationTypePerforated': 'Insulation type',

      'PerforatedInsulationType.none': 'Non-perforated',
      'PerforatedInsulationType.p1': 'Perforated 1/1',
      'PerforatedInsulationType.p3': 'Perforated 3/1',
      'PerforatedInsulationType.p5': 'Perforated 5/1',
      'PerforatedInsulationType.nte': 'NT',

      'pInsulationTypeWindow': 'Frame position',
      'InsulationTypeWindow.frameBehindInsulator': 'Frame behind insulation',
      'InsulationTypeWindow.frameInset100':
          'Frame merged into insulation by 100mm',
      'InsulationTypeWindow.frameOffset100':
          'Frame aside from insulation by 100mm',

      'pDishDistance': 'Distance to dowel head (L1)',
      'pAvgPerMeterSq': 'Avg. quantity of anchors per m²',
      'pPlateThickness': 'Plate thickness (dп)',
      'pTotalPlateLength': 'Total length of all plates',
      'pBreakForce': 'Withdrawal force (by experiment)',
      'pBreakForceEjot': 'Ejot H5 anchor withdrawal force',
      //'useExperimentalData': 'I have experimental data',

      'pInsulationOverlapThickness': 'Insulator overlapping length (dн)',
      'pBlockType': 'Block type',
      'BlockType.window': 'Window block',
      'BlockType.door': 'Door block',
      'BlockType.balcony': 'Balcony block',
      'pWindowHeight': 'Window height',
      'pWindowWidth': 'Window width',
      'pDoorHeight': 'Door height',
      'pDoorWidth': 'Door width',
      'pBlockCount': 'Count of these blocks',

      'pThermalResistanceOverlap': 'Floor thermal resistance',
      'pTotalOverlapLength': 'Total length',

      'pAngleType': 'Angle type',
      'AngleType.concave': 'Concave angle',
      'AngleType.convex': 'Convex angle',
      'pTotalAngleLength': 'Total length of elements',

      'pUsingAnchor': 'Do you already use Ejot H5?',
      'UseAnchorType.notUsingEjot': 'We use some other anchor',
      'UseAnchorType.usingEjot': 'We use Ejot H5 anchor',

      'pCustomName': 'Element name',
      'pElementDimensionType': 'Element type',
      'Dimension.linear': 'Linear element',
      'Dimension.point': 'Point element',
      'Dimension.planar': 'Planar element',
      'pUnitHeatLoss': 'Unit thermal losses',
      'pGeometricSpec': 'Geometric parameter',
      'pValuePerGeometricSpec': 'Value',
      'pDocReference': 'Doc references',
      'GeometricUnit.totalLength': 'Total length',
      'GeometricUnit.countPerMSq': 'Count per m²',

      'pBracketType': 'Bracket type',
      'pBracketPredefinedSize': 'Bracket width',
      'BracketType.rect': 'Rectangular bracket',
      'BracketType.halfRect': 'Half-rectangular bracket',
      'BracketSize.s60': '60',
      'BracketSize.s100': '100',
      'BracketSize.s150': '150',
      'BracketSize.s200': '200',
      'pBracketAvgCountPerMeterSq': 'Avg. count per m²',

      'pBracketAvgPerMeter': 'Avg. count per meter',
      'BracketAvgPerMeter.one': '1',
      'BracketAvgPerMeter.one33': '1,33',
      'BracketAvgPerMeter.one67': '1,67',
      'BracketAvgPerMeter.thre33': '3,33',
      'pBracketAvgThickness': 'Avg. bracket metal thickness',
      'pRailAvgThickness': 'Avg. rail metal thickness',

      'schWallsBasement': 'Walls with external insulation',
      'schBracketInstall': 'Bracket installation',
      'schBracket1': 'Rectangular brakcet 100 mm wide',
      'schBracket2': 'Rectangular brakcet 60 mm wide',
      'schBracket3': 'Rectangular brakcet 150 mm wide',
      'schBracket4': 'Rectangular brakcet 200 mm wide with extended base',
      'schBracket5': 'Rectangular brakcet 100 mm wide',
      'schBracket6': 'Rectangular brakcet 60 mm wide',
      'schBracket7': 'Rectangular brakcet 150 mm wide',
      'schFirestopper': 'Firestopper setup',
      'pFireStopLength': 'Total length',
      'hint': 'Schemes',
      'loading': 'Loading...',
      'start': 'Start...',

      'introMsg1':
          'Ejot H5 anchor is a fastener for thermo-insulation layer in facade systems. Thanks to innovative technologies, this anchor has very low thermal losses.',
      'introMsg2': 'This application provides solution for the tasks below:',
      'introMsg3': '• Thermal resistance estimation',
      'introMsg4': '• Estimate the quantity of H5 anchors in your project',
      'introMsg5': '• Economic justification of Anchor H5',
      'introMsg6':
          'The methodology is based on national standard documents used in Russian Federation:  СП 50.13330.2012, СП 230.1325800.2015, СП 131.13330.2018, СП 20.13330.2016, СП 293.1325800.2017, ГОСТ Р 56707-2015, ГОСТ Р 58360-2019, ГОСТ 30494-2011',

      'economicHint': 'To get economy report please fill your values below',
      'ecoCostEjot': 'Cost of Ejot H5',
      'perPiece': 'per piece',
      'ecoCostOther': 'Other anchor cost',
      'ecoCostInsulation': 'Insulation cost',
      'perM3': 'per m³',
      'ecoCostEnergy': 'Energy cost',
      'perGCal': 'per GCal',
      'economyInsulator': 'Insualtion cost economy',
      'tariffExplain': 'Base tariffs are set by regional government.',
      'exampleTariffs': 'Example tariffs',

      'ejotEast': 'EJOT WOSTOK LTD',
      'addr1': 'Moscow region, Noginskiy district, city Staraya Kupavna,',
      'addr2': 'building 12/2, office 152',
      'addr3': 'Phone +7 495 259 09 09',

      'windZoneSelect': 'Choose wind zone',
      'windZoneForCity': 'Wind zone for city',
      'windZoneManual': 'Manually selected, wind pressure',
      'pressureUnits': 'kPa',

      'inputErrors': 'Input data has errors',
      'pleaseFixInputErrors': 'please fix them',
      'fillBuildingSizeErr': 'Input building dimensions',

      'insulatorErr': 'Set insulation width and material',
      'ejotAnchorPerM2Err': 'Specify anchor count per square meter',
      'coreMaterial': 'Set core material',

      'resultsSftk': 'ETICS',
      'resultsNfs': 'RVF',
      'breakForceNorm': 'Estimated minimal withdrawal force',
      'anchorCategory': 'Anchor type category',
      'numberAnchorsPerM2': 'Required anchor quantity',
      'sftkOrdinaryZone': 'Ordinary zone',
      'sftkBoundaryZone': 'Boundary zone',
      'peakWindLoad': 'Peak wind load',
      'heightBuilding': 'Building height',
      'upto': 'up to',
      'above': 'above',
      'ejotHD': 'Ejot H5',
      'otherAnchor': 'Other anchor',

      'thermicResults': 'Thermal calculation',
      'unitHeatLossAnchor': 'Normalized heat loss through anchor',
      'rateAnchorLoss': 'Heat loss through anchor',
      'estimatedResistance': 'Construction estimated thermal resistance',
      'annualLoss': 'Annual heat loss through walls',
      'withR0norm': '(with rated thermal resistance)',
      'rNorm': 'Rated thermal resistance',
      'isoWidth': 'Insulator width:',
      'isoWidthAdjust': 'Inner insulator width:',
      'isoWidthFixed': 'Width of the outer insulation layer:',

      'economicResults': 'Economy calculation',
      'economyAnchor': 'Anchor econonmy',
      'economy': 'You save',
      'economyHeating': 'Energy cost economy',

      'saveToPdf': 'Save',
      'docPrinted': 'Document printed successfully',
      'docShared': 'Document shared successfully',
      'pdfDoc': 'PDF Document',
      'pdfTitle': 'Anchor',
      'pdfCalculation': 'Calculation',
      'pdfObject': 'Object',
      'pdfDate': 'Date',
      'pdfAnchorCalcTitle': 'Estimation of anchor quantity',
      'pdfCharacteristics': 'Characteristics',
      'page': 'Page',

      'elementOfConstruction': 'Element',
      'unitGeo': 'Unit geometric value',
      'unitFlow': 'Unit heat flow through element',

      'elementBase': 'Wall plane',
      'ejotAnchorReport': 'Anchors "EJOT H5"',
      'total': 'TOTAL',
      'value': 'Value',

      'from': 'from',
      'complyLimits': 'Value must be in range:',

      'languageChange': 'Language change',
      'ru': 'Russian',
      'en': 'English',
      'appRestart': '(please restart application)'

    },
    'ru': {
      // units
      'mm': 'мм',
      'm': 'м',
      '1/m': '1/м',
      '1/m2': '1/м²',
      'm2C/W': 'м²°C/Вт',
      'W/m2C': 'Вт/м²°C',
      'pcs/m': 'шт/м',
      'pcs/m2': 'шт/м²',
      'pcs': 'шт',
      'kN': 'кН',
      'm3': 'м³',
      'm2': 'м²',
      'Gcal/year': 'ГКал/год',
      'm/m2': 'м/м²',
      'm2/m2': 'м²/м²',
      'kg/m3': 'кг/м³',
      //

      'title': 'Расчёт анкера',

      'step1': 'Исходные данные',
      'step2': 'Тип фасадной системы',
      'step3': 'Элементы стеновых конструкций',
      'step4': 'Экономические параметры',
      'step5': 'Результаты расчёта',

      'stepsListTitle': 'Ввод параметров',
      'nextStep': 'шаг',
      'stepFrom': 'из',

      'buildingType': 'Выбор назначения постройки...',
      'buildingTypeExplain':
          'Назначение здания и/или помещения (СП 50.13330.2012 Таблица 3)',

      'searchCity': 'Найти город...',
      'inputDescription': 'Название объекта...',
      'cityExplain':
          'Город необходим для определения норм по СП 131.13330.2018',
      'cityNotFound': 'Такой город не найден, выберите ближайший',

      'innerTemperature': 'Задать температуру внутри помещения',
      'innerHumidity': 'Задать влажность внутри помещения',
      'climateExplain':
          'См. ГОСТ 30494-2011 "Здания жилые и общественные. Параметры микроклимата в помещениях."',

      'select': 'Выбрать...',
      'clearMaterial': 'Сброс материала утеплителя',
      'areYouSure': 'Подтвердите, что хотите сбросить материал.',
      'confirmRemove': 'Сбросить',
      'dontRemove': 'Не сбрасывать',

      'FacadeType.SFTK': 'СФТК',
      'FacadeType.NFS': 'НФС',
      'desc_FacadeType.SFTK':
          'Система фасадная теплоизоляционная композиционная',
      'desc_FacadeType.NFS': 'Навесная фасадная система с воздушным зазором',
      'mtlWallCore': 'Материал основания стены',
      'mtlInsulation': 'Теплоизоляционный материал',
      'mtlAnchor': 'Тарельчатый анкер',

      'baseWidth': 'Фиксированная толщина основания стены, мм',
      'insulatorWidthExternal': 'Толщина внешнего слоя утеплителя, мм',
      'insulatorWidth': 'Толщина слоя утеплителя, мм',
      'inputInsulatorWidth': 'Ввести толщину слоя утеплителя',
      'calculateInsulatorWidth':
          'Автоматически рассчитать толщину слоя утеплителя',
      'breakForce': 'Введите фактическое усилие на разрыв, кН',

      'sftkClass': 'Выберите класс надежности СФТК',
      'sftkClass0': 'СК0',
      'sftkClass1': 'СК1',
      'sftkClass2': 'СК2',
      'sftkClassLabel0':
          'СФТК высшего класса - для зданий с повышенным уровнем ответственности',
      'sftkClassLabel1':
          'СФТК среднего класса - для зданий с нормальным уровнем ответственности',
      'sftkClassLabel2':
          'СФТК пониженного класса - для зданий с пониженным уровнем ответственности',

      'terrainType': 'Выберите тип местности',
      'terrainType0': 'Тип A',
      'terrainType1': 'Тип B',
      'terrainType2': 'Тип C',
      'terrainTypeLabel0':
          'Открытые побережья морей, озер и водохранилищ, сельские местности, в том числе с постройками высотой менее 10 м, пустыни, степи, лесостепи, тундра',
      'terrainTypeLabel1':
          'Городские территории, лесные массивы и другие местности, равномерно покрытые препятствиями высотой более 10 м',
      'terrainTypeLabel2':
          'Городские районы с плотной застройкой зданиями высотой более 25 м',

      'buildingLength': 'Введите длину здания, м',
      'buildingWidth': 'Введите ширину здания, м',
      'buildingHeight': 'Введите высоту здания, м',
      'buildingFacadeSquare': 'Площадь фасада, м²',
      'facadeNoHoles': 'Площадь фасада (после вычета проёмов), м²',

      'selectMaterial': 'Выбрать материал',
      'customMaterials': 'Созданные вами материалы',
      'createMaterial': 'Добавить материал',
      'inputMaterialName': 'Название нового материала',
      'searchMaterial': 'Поиск материала...',
      'materialNotFound': 'Не найдено',
      'selectThisMaterial': 'Выбрать материал',

      'fillBlockHint':
          'Задайте один или более элементов, если они присутствуют в вашей конструкции.',
      'addMore': 'Добавить новый узел',

      'selectCity': 'Выбор города',
      'editBlock': 'Редактировать элемент',
      'addNewElement': 'Добавить новый',
      'addNewBlock': 'Добавить элемент',
      'removeThisBlock': 'Удалить этот элемент',
      'filledElementsNumber': 'Заполнено элементов',

      'BlockBinding.INSULATION_BINDING':
          'Крепеж утеплителя (тарельчатый анкер)',
      'BlockBinding.BALCONY_BINDING': 'Сопряжение с балконной плитой',
      'BlockBinding.ANGLE_BINDING': 'Углы',
      'BlockBinding.WINDOW_BINDING': 'Стыки с оконными блоками',
      'BlockBinding.BASEMENT_BINDING': 'Примыкание к цоколю',
      'BlockBinding.UNIVERSAL': 'Прочее',
      'BlockBinding.BRACKET_AL_BINDING': 'Алюминиевые кронштейны',
      'BlockBinding.BRACKET_STEEL_BINDING': 'Стальные кронштейны',
      'BlockBinding.FIRESTOP_BINDING': 'Стальная противопожарная рассечка',

      'pInsulationTypePerforated': 'Вариант теплозащиты',

      'PerforatedInsulationType.none': 'Без перфорации',
      'PerforatedInsulationType.p1': 'Перфорация 1/1',
      'PerforatedInsulationType.p3': 'Перфорация 3/1',
      'PerforatedInsulationType.p5': 'Перфорация 5/1',
      'PerforatedInsulationType.nte': 'НТЭ',

      'schPerforatedHint': 'Теплозащита с перфорацией',
      'schUnperforatedHint': 'Теплозащита без перфорации',

      'pInsulationTypeWindow': 'Расположение рамы',
      'InsulationTypeWindow.frameBehindInsulator': 'Рама сразу за утеплителем',
      'InsulationTypeWindow.frameInset100':
          'Рама сдвинута в утеплитель на 100мм',
      'InsulationTypeWindow.frameOffset100':
          'Рама сдвинута от утеплителя на 100мм',

      'pDishDistance': 'Расстояние от края распорного элемента до тарелки дюбеля (L1)',
      'pAvgPerMeterSq':
          'Среднее количество анкеров для крепления наружного слоя утеплителя на м²',
      'pPlateThickness': 'Толщина плиты (dп)',
      'pTotalPlateLength': 'Общая протяженность выходов плит',
      'pBreakForce': 'Вытягивающее усилие',
      'pBreakForceEjot': 'Вытягивающие усилие Ejot H5',
      //'useExperimentalData': 'Использовать экспериментальные данные',

      'pInsulationOverlapThickness': 'Толщина нахлёста утеплителя (dн)',
      'pBlockType': 'Тип блока',
      'BlockType.window': 'Оконный блок',
      'BlockType.door': 'Дверной блок',
      'BlockType.balcony': 'Балконный блок',
      'pWindowHeight': 'Высота оконной части',
      'pWindowWidth': 'Ширина оконной части',
      'pDoorHeight': 'Высота дверной части',
      'pDoorWidth': 'Ширина дверной части',
      'pBlockCount': 'Количество таких блоков',

      'pThermalResistanceOverlap': 'Термическое сопротивление перекрытия',
      'pTotalOverlapLength': 'Общая протяжённость примыкания',

      'pAngleType': 'Конфигурация угла',
      'AngleType.concave': 'Вогнутый угол',
      'AngleType.convex': 'Выпуклый угол',
      'pTotalAngleLength': 'Общая протяжённость таких углов',

      'pCustomName': 'Название элемента',
      'pElementDimensionType': 'Элемент конструкции',
      'Dimension.linear': 'Элемент линейный',
      'Dimension.point': 'Элемент точечный',
      'Dimension.planar': 'Элемент плоский',
      'pUnitHeatLoss': 'Удельные потери теплоты',
      'pGeometricSpec': 'Геометрический показатель',
      'pValuePerGeometricSpec': 'Значение показателя',
      'pDocReference': 'Ссылки на документы',
      'GeometricUnit.totalLength': 'Общая протяжённость',
      'GeometricUnit.countPerMSq': 'Количество на м²',

      'pBracketType': 'Тип кронштейна',
      'pBracketPredefinedSize': 'Ширина кронштейна',
      'BracketType.rect': 'П-образный кронштейн',
      'BracketType.halfRect': 'L-образный кронштейн',
      'BracketSize.s60': '60',
      'BracketSize.s100': '100',
      'BracketSize.s150': '150',
      'BracketSize.s200': '200',
      'pBracketAvgCountPerMeterSq': 'Среднее количество на м²',

      'pBracketAvgPerMeter': 'Среднее количество на метр',
      'BracketAvgPerMeter.one': '1',
      'BracketAvgPerMeter.one33': '1,33',
      'BracketAvgPerMeter.one67': '1,67',
      'BracketAvgPerMeter.thre33': '3,33',
      'pBracketAvgThickness': 'Средняя толщина металла кронштейна',
      'pRailAvgThickness': 'Средняя толщина металла направляющей',

      'schWallsBasement':
          'Стена – с наружным утеплением и тонкой облицовкой (фасад)',
      'schBracketInstall': 'Установка кронштейна',
      'schBracket1': 'П-образный кронштейн шириной 100 мм',
      'schBracket2': 'П-образный кронштейн шириной 60 мм',
      'schBracket3': 'П-образный кронштейн шириной 150 мм',
      'schBracket4':
          'П-образный кронштейн шириной 200 мм с расширенным основанием',
      'schBracket5': 'L-образный кронштейн шириной 100 мм',
      'schBracket6': 'L-образный кронштейн шириной 60 мм',
      'schBracket7': 'L-образный кронштейн шириной 150 мм',
      'schFirestopper': 'Установка противопожарной рассечки',

      'pFireStopLength': 'Общая протяжённость примыкания',
      'hint': 'Схемы',
      'loading': 'Загрузка...',
      'start': 'Старт...',

      'introMsg1':
          'Тарельчатый анкер EJOT H5 предназначен для крепления теплоизоляционного слоя фасадных систем. Благодаря заложенным инновационным технологиям, EJOT H5 имеет самые низкие удельные потери теплоты.',
      'introMsg2': 'Это приложение автоматизирует решение задач:',
      'introMsg3': '• Расчёт приведенного сопротивления теплопередачи',
      'introMsg4':
          '• Расчет требуемого количества тарельчатых анкеров для СФТК',
      'introMsg5':
          '• Экономическое обоснование выбора тарельчатого анкера EJOT H5',
      'introMsg6':
          'Методика расчётов построена на основании нормативных стандартов в сфере строительства на территории Российской Федерации: СП 50.13330.2012, СП 230.1325800.2015, СП 131.13330.2018, СП 20.13330.2016, СП 293.1325800.2017, ГОСТ Р 56707-2015, ГОСТ Р 58360-2019, ГОСТ 30494-2011',

      'economicHint':
          'Заполните значения параметров для получения расчёта экономии',
      'ecoCostEjot': 'Стоимость Ejot H5',
      'perPiece': 'за шт.',
      'ecoCostOther': 'Стоимость другого анкера',
      'ecoCostInsulation': 'Стоимость утеплителя',
      'perM3': 'за м³',
      'ecoCostEnergy': 'Тариф электроэнергии',
      'perGCal': 'за ГКал',
      'economyInsulator': 'Экономия на утеплителе',

      'ejotEast': 'ООО Эйот Восток',
      'addr1': 'Московская обл., Ногинский район, г. Старая Купавна,',
      'addr2': 'д. 12, стр. 2 , офис 152',
      'addr3': 'Телефон +7 495 259 09 09',

      'windZoneSelect': 'Выберите ветровую зону',
      'windZoneForCity': 'Ветровая зона города',
      'windZoneManual': 'Зона выбрана вручную, нормативное давление ветра',
      'pressureUnits': 'кПа',

      'inputErrors': 'Входные данные содержат ошибки',
      'pleaseFixInputErrors': 'пожалуйста исправьте их',
      'fillBuildingSizeErr': 'Введите размеры здания',

      'insulatorErr': 'Выберите материал утеплителя',
      'ejotAnchorPerM2Err': 'Укажите количество анкеров на квадратный метр',
      'coreMaterial': 'Выберите материал основания',

      'resultsSftk': 'СФТК',
      'resultsNfs': 'НФС',
      'breakForceNorm': 'Нормативное вытягивающее усилие, не менее',
      'anchorCategory': 'Категория применения анкера с дюбелем',
      'numberAnchorsPerM2': 'Требуемое количество анкеров',
      'sftkOrdinaryZone': 'Рядовая зона',
      'sftkBoundaryZone': 'Краевая зона',
      'peakWindLoad': 'Пиковая ветрвоая нагрузка',
      'heightBuilding': 'При высоте здания',
      'upto': 'до',
      'above': 'более',
      'ejotHD': 'Ejot H5',
      'otherAnchor': 'Другой анкер',

      'thermicResults': 'Теплотехнический расчёт',
      'unitHeatLossAnchor': 'Удельные потери теплоты',
      'rateAnchorLoss': 'Доля общего потока теплоты через фрагмент',
      'estimatedResistance': 'Приведенное сопротивление теплопередаче конструкции',
      'annualLoss': 'Годовые теплопотери через стены фасада',
      'withR0norm': '(при нормативном сопротивлении)',
      'rNorm': 'Нормативное сопротивление',
      'isoWidth': 'Толщина слоя утеплителя',
      'isoWidthAdjust': 'Толщина внутреннего слоя утеплителя',
      'isoWidthFixed': 'Толщина внешнего слоя утеплителя',

      'economicResults': 'Расчёт экономии',
      'economyAnchor': 'Экономия на анкерах',
      'economy': 'экономия',
      'economyHeating': 'Экономия на отоплении',
      'tariffExplain': 'Базовую стоимость энергоресурсов назначают региональные власти.',
      'exampleTariffs': 'Ссылка на документ с тарифами',

      'saveToPdf': 'Сохранить',
      'docPrinted': 'Документ отправлен на печать',
      'docShared': 'Поделиться документом',
      'pdfDoc': 'Документ PDF',
      'pdfTitle': 'Ejot H5',
      'pdfCalculation': 'Расчёт',
      'pdfObject': 'Объект',
      'pdfDate': 'Дата',
      'pdfAnchorCalcTitle': 'Расчёт количества анкеров',
      'pdfCharacteristics': 'Характеристика',
      'page': 'Страница',

      'elementOfConstruction': 'Элемент конструкции',
      'unitGeo': 'Удельный геометрический показатель',
      'unitFlow': 'Удельный поток теплоты, обусловленный элементом',
      'elementBase': 'Стена по глади',
      'ejotAnchorReport': 'Тарельчатые анкера "EJOT H5"',
      'total': 'ИТОГО',
      'value': 'Показатель',

      'from': 'от',
      'complyLimits': 'Значение должно быть в диапазоне:',

      'languageChange': 'Смена языка приложения',
      'ru': 'Русский',
      'en': 'Английский',
      'appRestart': '(необходимо вручную перезапустить приложение)'
    }
  };

  _addBuildingTypes(localizedValues);

  return localizedValues;
}

void _addBuildingTypes(Map<String, Map<String, String>> localizedValues) {
  localizedValues['en'].addAll({
    'bt0': "Living buildings",
    'bt1': "Hostels",
    'bt2': "Nursing houses",
    'bt3': "Kindergarden buildings",
    'bt4': "Hotels",
    'bt5': "Schools",
    'bt6': "Boarding school",
    'bt7': "Hospitals",
    'bt8': "Industrial buildings with dry/normal conditions",
    'bt9': "Industrial buildings with wet conditions",
    'bt10': "Other public buildings",
    'bt11': "Administrative buildings",
    'bt12': "Unlisted"
  });

  localizedValues['ru'].addAll({
    'bt0': "Жилые",
    'bt1': "Общежития",
    'bt2': "Дома для престарелых и инвалидов",
    'bt3': "Детские дошкольные учреждения",
    'bt4': "Гостиницы",
    'bt5': "Школы",
    'bt6': "Интернаты",
    'bt7': "Лечебно-профилактические",
    'bt8': "Производственные здания с сухим или нормальным режимом",
    'bt9': "Производственные здания с влажным или мокрым режимом",
    'bt10': "Общественные здания, кроме перечисленных",
    'bt11': "Административные и бытовые",
    'bt12': "Не указанные в списке"
  });
}
