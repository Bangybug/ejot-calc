import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';
import 'dart:io';
import 'package:platform/platform.dart';

// https://medium.com/better-programming/flutter-how-to-save-objects-in-sharedpreferences-b7880d0ee2e4

class IPersistenceSource {
  void setItem(String key, dynamic value) async {}
  Future<dynamic> getItem(String key) async { return null; }
  void removeItem(String key) {}
}


class Persistence {
  final _prefSource;

  Persistence() : _prefSource = LocalPlatform().isWindows ? PersistenceWithWindows() : PersistenceWithSharedPreferences();

  read(String key) async {
    return await _prefSource.getItem(key);
  }

  save(String key, value) async {
    await _prefSource.setItem(key, value);
  }

  remove(String key) async {
    await _prefSource.removeItem(key);
  }
}

class PersistenceWithSharedPreferences extends IPersistenceSource {
  void setItem(String key, dynamic value) async {
    (await SharedPreferences.getInstance()).setString(key, json.encode(value));
  }

  Future<dynamic> getItem(String key) async { 
    return json.decode( (await SharedPreferences.getInstance()).getString(key) );
  }

  void removeItem(String key) async {
    (await SharedPreferences.getInstance()).remove(key);
  }
}

class PersistenceWithWindows extends IPersistenceSource {
  static PersistenceWithWindows _instance;
  Map<String, dynamic> _jsonStore;
  bool _isReady = false;
  File _file;

  PersistenceWithWindows() {
    if (_instance == null) {
      _instance = this;
    }
  }

  Future<PersistenceWithWindows> getInstance() async {
    if (_instance._isReady) 
      return _instance;

    final Directory appDocDir = await getApplicationDocumentsDirectory();
    final String appDocPath = appDocDir.path;
    _instance._file = File(appDocPath + '/' + 'ejot.json');

    _instance._jsonStore = {};

    if (_instance._file.existsSync()) {
      try {
        _instance._jsonStore = json.decode( await _instance._file.readAsString() );
      } catch (e) {
        print('ERROR loading preferences');
      }
    }

    _instance._isReady = true;
    return _instance;
  }

  Future<void> _persist() async {
    try {
      await _instance._file.writeAsString(json.encode(_instance._jsonStore));
    } catch(e) {
      print('ERROR saving preferences '+_instance._file.path);
    }
  }

  void setItem(String key, dynamic value) async {
    await getInstance();
    _instance._jsonStore[key] = value;
    await _persist();
  }

  Future<dynamic> getItem(String key) async { 
    await getInstance();
    return _instance._jsonStore.containsKey(key) ? _instance._jsonStore[key] : null;
  }

  void removeItem(String key) async {
    await getInstance();
    if (_instance._jsonStore.containsKey(key)) {
      _instance._jsonStore.remove(key);
      await _persist();
    }
  }

}