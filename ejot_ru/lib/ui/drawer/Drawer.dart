import 'package:ejot_ru/data/AppLocalizations.dart';
import 'package:ejot_ru/model/AppStore.dart';
import 'package:ejot_ru/ui/pagenav/Item.dart';
import 'package:ejot_ru/ui/pagenav/ItemList.dart';
import 'package:ejot_ru/util/Tuple.dart';
import 'package:ejot_ru/util/misc.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:provider/provider.dart' as P;


class MyDrawer extends StatelessWidget { 

  MyDrawer({Key key, this.steps, this.handlePageChange}) : super(key: key);

  final List<Item> steps;
  final ValueChanged<Tuple2<int,bool>> handlePageChange;

  void handleChangeLanguage(BuildContext context, String newLanguage) {
    var appStore = P.Provider.of<AppStore>(context, listen: false);
    var loc = AppLocalizations.of(context);
    appStore.forceLanguage = newLanguage;
    ++appStore.lastUpdatedCount;
    showModalDialog(context, loc.getString('languageChange'), loc.getString(newLanguage), loc.getString('appRestart'));
  }
  
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Container(
            height: 80.0,
            child: DrawerHeader(
              decoration: BoxDecoration(
                color: Theme.of(context).secondaryHeaderColor,
              ),
              margin: EdgeInsets.all(0.0),
              child: Text(
                AppLocalizations.of(context).getString('stepsListTitle'),
                style: TextStyle(
                  color: Theme.of(context).accentColor,
                  fontSize: 24,
                ),
              ),
            ),
          ),
          Expanded(
            child: Observer(
              builder: (_) {
                var appStore = P.Provider.of<AppStore>(_, listen: false);
                return new ItemList(
                  items: steps,
                  selectedItem: appStore.pageIndex == 0 ? null : steps[appStore.pageIndex-1],
                  itemSelectedCallback: (item) {
                              
                    // FYI For tablets I needed to pin drawer, and also display it on the same level with the main screen.
                    // This is not possible with standard Drawers. 
                    // When Drawer is used as intended, it adds navigation route. But when Drawer is pinned, it has no route.
                    // I wanted to remove Drawer as soon as the User clicks on any of its items, so I pop the route.
                    // Popping the route when the Drawer is pinned will cut down UI into black screen.
                    if (Navigator.canPop(context))
                      Navigator.pop(context);
                  
                    handlePageChange( Tuple2(steps.indexOf(item),false) );
                  }
                );
              }
            )
          ),
          
          Row(children: <Widget>[
            GestureDetector(
              onTap: () { handleChangeLanguage(context, 'ru'); },
              child: Image(
                image: AssetImage('assets/icons/rus.png'),
                height: 50,
                width: 50,
              )
            ),
                      
            GestureDetector(
              onTap: () { handleChangeLanguage(context, 'en'); },
              child: Image(
                image: AssetImage('assets/icons/us.png'),
                height: 50,
                width: 50,
              )
            ),
          ],)

        ],
      )
    );
  }
}