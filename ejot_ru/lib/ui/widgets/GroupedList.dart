import 'package:ejot_ru/util/Tuple.dart';
import 'package:flutter/material.dart';
import 'package:sticky_infinite_list/sticky_infinite_list.dart';

class GroupedList<T> extends StatelessWidget {

  final double headerHeight = 48;
  final ValueChanged<Tuple2<int,int>> onItemSelected, onItemLongTapped;
  final ValueChanged<int> onCollapseToggle;
  final T selectedItem;
  final GroupData<T> data;
  final bool canCollapse;
  final TextStyle selectedItemStyle = TextStyle(color: Colors.green); // FYI could not override ListTile selection color via theme, looked at the source code but no luck
  final TextStyle subtitleStyle = TextStyle();

  GroupedList({Key key, this.data, this.onItemSelected, this.selectedItem, this.onItemLongTapped, this.canCollapse = false, this.onCollapseToggle}) : super(key: key);

  Widget _wrapCollapsible(Widget child, int index) {
    return GestureDetector(
      child: child,
      onTap: () { 
        if (null != onCollapseToggle) {
          onCollapseToggle(index);
        }
      }
    );
  }

  Widget _buildGroupHeader(int index) {
    Widget text = Text(
      data.getGroupName(index),
      maxLines: 3,
      style: TextStyle(
        height: .9,
        fontSize: 17,
        color: Colors.white70
      )
    );

    if (canCollapse) {
      return _wrapCollapsible(Stack(
        alignment: AlignmentDirectional.centerStart,
        children: <Widget>[
          Icon( data.isGroupCollapsed(index) ? Icons.arrow_right : Icons.arrow_drop_down, color:Colors.white),
          Container(
            margin: EdgeInsets.only(left: 30, top: 4),
            child: text
          )
        ]
      ), index);
    } else {
      return text;
    }
  }


  Widget _buildListStickyHeader(BuildContext context, StickyState<int> state, int index) {
    return Container(
      height: headerHeight,
      width: double.infinity,
      color: Colors.grey[600],
      padding: EdgeInsets.symmetric(horizontal: 8.0),
      margin: EdgeInsets.only(bottom: 2),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          _buildGroupHeader(index)
        ],
      ),
    );
  }

  Widget _buildItemContent(int index, int subIndex) {
    bool isSelected = selectedItem == data.getItem(index, subIndex);
    var style = isSelected ? selectedItemStyle : null;
    var subtitle = data.getGroupItemSubtitle(index, subIndex);

    return ListTile(
      selected: isSelected,
      onLongPress: () => onItemLongTapped(Tuple2<int,int>(index, subIndex)),
      onTap: () => onItemSelected(Tuple2<int,int>(index, subIndex)),
      title: Text( data.getGroupItemLabel(index, subIndex), style: style ),
      subtitle: subtitle != null ? Text( subtitle, style: subtitleStyle.merge(style) ) : null,
    );
  }


  Widget _buildSublist(BuildContext context, int index) {
    // FYI as of 2020 with available packages it's not possible to have a nested infinite lists with sticky headers 
    // so I prebuild sublists with Column, otherwise using ListView.builder for sublists will confuse their own scrollbars which are unwanted
    if (canCollapse && data.isGroupCollapsed(index)) {
      return SizedBox(height: 5.0,);  // FYI height must not be null, otherwise the list wont build at all! (issue in the InfiniteList)
    }
    return Container(
      width: double.infinity,
      padding: EdgeInsets.only(left: 2, right: 2, top: 0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: List.generate( data.getGroupItemCount(index), (subIndex) => _buildItemContent(index, subIndex) )
      )
    );
  }


  @override
  Widget build(BuildContext context) {
    return InfiniteList(
      posChildCount: data.getGroupCount(),
      builder: (context, index) => InfiniteListItem(
        headerStateBuilder: (context, state) => _buildListStickyHeader(context, state, index),
        contentBuilder: (context) => _buildSublist(context, index)
      )
    );
  }

}


abstract class GroupData<T> {
  int getGroupCount();
  String getGroupName(int groupIndex);
  int getGroupItemCount(int groupIndex);
  String getGroupItemLabel(int groupIndex, int itemIndex);
  String getGroupItemSubtitle(int groupIndex, int itemIndex) => null;

  Map<int, bool> collapsed;

  GroupData({collapsed}) : collapsed = collapsed == null ? Map<int, bool>() : collapsed;

  bool isGroupCollapsed(int groupIndex) {
    return null == collapsed[groupIndex] ? false : collapsed[groupIndex];
  }

  void toggleCollapseGroup(int groupIndex) {
    collapsed[groupIndex] = !isGroupCollapsed(groupIndex);
  }

  GroupData filter(String filter);

  bool hasSingleItem(String itemLabel) {
    return (getGroupCount() == 1 && getGroupItemCount(0) == 1 && getGroupItemLabel(0,0).toUpperCase() == itemLabel.toUpperCase());
  }

  T getItem(int groupIndex, int itemIndex);
}


class StringGroupData extends GroupData<String> {
  StringGroupData({this.data, collapsed}) : super(collapsed: collapsed);

  List <Tuple2<String, List<String>>> data;

  @override
  int getGroupItemCount(int groupIndex) {
    return data[groupIndex].item2.length;
  }

  @override
  String getGroupItemLabel(int groupIndex, int itemIndex) {
    return data[groupIndex].item2[itemIndex];
  }

  @override
  String getGroupName(int groupIndex) {
    return data[groupIndex].item1;
  }

  @override
  int getGroupCount() {
    return data.length;
  }



  StringGroupData filter(String filter) {
    String upFilter = filter.toUpperCase();

    List <Tuple2<String, List<String>>> filtered = new List <Tuple2<String, List<String>>>();

    data.forEach(
      (dataEntry) {
        Tuple2<String, List<String>> group = new Tuple2<String, List<String>>(dataEntry.item1, 
          dataEntry.item2.where(
            (itemString) => itemString.toUpperCase().contains(upFilter)
          ).toList()
        );
        if (group.item2.isNotEmpty) {
          filtered.add(group);
        }
      }
    );

    return StringGroupData(data: filtered, collapsed: collapsed);
  }

  @override
  String getItem(int groupIndex, int itemIndex) {
    return data[groupIndex].item2[itemIndex];
  }
}