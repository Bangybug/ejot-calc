import 'package:ejot_ru/ui/widgets/AutoSizeText.dart';
import 'package:ejot_ru/util/Tuple.dart';
import 'package:flutter/material.dart';

import 'ZoomableImagePage.dart';

class GalleryPageArguments {
  final List<Tuple2<String,String>> images;
  final String headerText;

  GalleryPageArguments({this.images, this.headerText});
}

class GalleryPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    final GalleryPageArguments args = ModalRoute.of(context).settings.arguments;
    final Orientation orientation = MediaQuery.of(context).orientation;
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: true,
        title: AutoSizeText(
          args.headerText,
          presetFontSizes: [15.0, 13.0, 11.0],
          maxLines: 4,
        ),
        leading: IconButton(icon:Icon(Icons.arrow_back),
          onPressed:() => Navigator.pop(context, false),
        )
      ),
      body: Column(
        children: <Widget>[
          Expanded( 
            child: Scrollbar(child: SafeArea(
              top: false,
              bottom: false,
              child: GridView.count(
                // shrinkWrap: true,
                crossAxisCount: (orientation == Orientation.portrait) ? 1 : 3,
                mainAxisSpacing: 4.0,
                crossAxisSpacing: 4.0,
                childAspectRatio: (orientation == Orientation.portrait) ? 1.0 : 1.3,
                children: List.generate(args.images.length, (index) {
                  return GridDemoPhotoItem(
                    photo: Photo(title: args.headerText, caption: args.images[index].item2, assetName: args.images[index].item1),
                    tileStyle: GridDemoTileStyle.twoLine,
                    onBannerTap: (Photo photo) {
                      // 
                    },
                  );
                }).toList(),
              ),
            ),
          )),
        ],
      ),
    );
  }
}




enum GridDemoTileStyle {
  imageOnly,
  oneLine,
  twoLine
}

typedef BannerTapCallback = void Function(Photo photo);

class Photo {
  Photo({
    this.assetName,
    this.assetPackage,
    this.title,
    this.caption,
    this.isFavorite = false,
  });

  final String assetName;
  final String assetPackage;
  final String title;
  final String caption;

  bool isFavorite;
  String get tag => assetName; // Assuming that all asset names are unique.

  bool get isValid => assetName != null && title != null && caption != null && isFavorite != null;
}

class _GridTitleText extends StatelessWidget {
  const _GridTitleText(this.text);

  final String text;

  @override
  Widget build(BuildContext context) {
    return FittedBox(
      fit: BoxFit.scaleDown,
      alignment: Alignment.centerLeft,
      child: Text(text),
    );
  }
}

class GridDemoPhotoItem extends StatelessWidget {
  GridDemoPhotoItem({
    Key key,
    @required this.photo,
    @required this.tileStyle,
    @required this.onBannerTap,
  }) : assert(photo != null && photo.isValid),
       assert(tileStyle != null),
       assert(onBannerTap != null),
       super(key: key);

  final Photo photo;
  final GridDemoTileStyle tileStyle;
  final BannerTapCallback onBannerTap; // User taps on the photo's header or footer.

  void showPhoto(BuildContext context) {
    Navigator.pushNamed(context, '/imageView', arguments: ZoomableImagePageArguments(
      headerText: photo.caption,
      imgPath: photo.assetName
    ));
  }

  @override
  Widget build(BuildContext context) {
    final Widget image = GestureDetector(
      onTap: () { showPhoto(context); },
      child: Hero(
        key: Key(photo.assetName),
        tag: photo.assetName,
        child: Image.asset(
          photo.assetName,
          package: photo.assetPackage,
          fit: BoxFit.scaleDown,
        ),
      ),
    );

    // final IconData icon = photo.isFavorite ? Icons.star : Icons.star_border;

    switch (tileStyle) {
      case GridDemoTileStyle.imageOnly:
        return image;

      case GridDemoTileStyle.oneLine:
        return GridTile(
          header: GestureDetector(
            onTap: () { onBannerTap(photo); },
            child: GridTileBar(
              title: _GridTitleText(photo.title),
              backgroundColor: Colors.black45,
              // leading: Icon(
              //   icon,
              //   color: Colors.white,
              // ),
            ),
          ),
          child: image,
        );

      case GridDemoTileStyle.twoLine:
        return GridTile(
          footer: GestureDetector(
            onTap: () { onBannerTap(photo); },
            child: GridTileBar(
              backgroundColor: Colors.black45,
              title: _GridTitleText(photo.title),
              subtitle: _GridTitleText(photo.caption),
              // trailing: Icon(
              //   icon,
              //   color: Colors.white,
              // ),
            ),
          ),
          child: image,
        );
    }
    assert(tileStyle != null);
    return null;
  }
}

