import 'dart:io';
import 'dart:typed_data';

import 'package:ejot_ru/data/AppLocalizations.dart';
import 'package:ejot_ru/model/AppStore.dart';
import 'package:ejot_ru/ui/widgets/AutoSizeText.dart';
import 'package:filelog/filelog.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:pdf/pdf.dart';
import 'package:printing/printing.dart';

import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';

import 'builder.dart';

class PdfPageArguments {
  final AppStore appStore;
  
  PdfPageArguments(this.appStore);
}

Future<void> savePdfAsFile(
  BuildContext context,
  LayoutCallback build,
  PdfPageFormat pageFormat,
) async {
  log("PDF: before build");
  final Uint8List bytes = await build(pageFormat);
  log("PDF: after build");

  final Directory appDocDir = await getApplicationDocumentsDirectory();
  final String appDocPath = appDocDir.path;

  log("PDF: appdocPath "+appDocPath);

  File file;
  int n = 0;
  do {
    file = File(appDocPath + '/' + 'ejot_report'+ (n == 0 ? '' : (n+1).toString())+'.pdf');
    ++n;
  } while (file.existsSync());

  log("PDF: filename "+file.path);

  log('PDF: Save as file ${file.path} ...');

  await file.writeAsBytes(bytes);

  log('PDF: saved');

  OpenFile.open(file.path);
}

class PdfPage extends StatelessWidget {
  
  void _showPrintedToast(BuildContext context) {
    final loc = AppLocalizations.of(context);
    final ScaffoldState scaffold = Scaffold.of(context);

    scaffold.showSnackBar(
      SnackBar(
        content: Text(loc.getString('docPrinted')),
      ),
    );
  }

  void _showSharedToast(BuildContext context) {
    final loc = AppLocalizations.of(context);
    final ScaffoldState scaffold = Scaffold.of(context);

    scaffold.showSnackBar(
      SnackBar(
        content: Text(loc.getString('docShared')),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {

    final List<PdfPreviewAction> actions = [];
    final loc = AppLocalizations.of(context);
    final PdfPageArguments args = ModalRoute.of(context).settings.arguments;
    final appStore = args.appStore;

    if (!kIsWeb) {
      actions.add(
        PdfPreviewAction(
          icon: const Icon(Icons.save),
          onPressed: savePdfAsFile,
        )
      );
    }

    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: true,
        title: AutoSizeText(
          loc.getString('pdfDoc'),
          presetFontSizes: [15.0, 13.0, 11.0],
          maxLines: 4,
        ),
        leading: IconButton(icon:Icon(Icons.arrow_back),
          onPressed:() => Navigator.pop(context, false),
        )
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: PdfPreview(
              //maxPageWidth: 700,
              initialPageFormat: PdfPageFormat.a4,
              build: (PdfPageFormat pageFormat) => generatePdf(loc, appStore, pageFormat),
              actions: actions,
              onPrinted: _showPrintedToast,
              onShared: _showSharedToast,
              canChangePageFormat: false,
            ),

          )
        ],
      ),
    );
  }
}



