import 'dart:collection';
import 'dart:math' as math;

import 'package:ejot_ru/calculator/AnchorCalc.dart';
import 'package:ejot_ru/model/AppStore.dart';
import 'package:ejot_ru/model/Block.dart';
import 'package:ejot_ru/model/Block.dart' as bw;
import 'package:ejot_ru/util/misc.dart';

import 'ThermicTables.dart';


class _Loss {
  final double unitGeo;
  final double unitLoss;

  _Loss(this.unitGeo, this.unitLoss);
}

class _SumLoss {
  final List<_Loss> lines;
  final double ejotAnchorLoss;
  final double otherAnchorLoss;
  final double sumLossWithoutAnchor;

  _SumLoss(this.lines, this.ejotAnchorLoss, this.otherAnchorLoss, this.sumLossWithoutAnchor);
}

class ReportRow {
  final String blockName;
  final BlockParams inst;
  final double unitGeo;
  final double unitLoss;
  final double loss;
  final Dimension dimension;
  
  double lossPercent;

  ReportRow(this.blockName, this.inst, this.unitGeo, this.unitLoss, this.dimension): this.loss = roundDouble(unitLoss*unitGeo,5);
}


class Report {
  // initially, I built diff comparisons based on two formulas:
  // sumLossWithoutAnchor + ejotAnchorLoss  vs  sumLossWithoutAnchor + otherAnchorLoss
  // but the above is not correct when isolator width is calculated and then round for two different cases: ejot and other
  // so it's possible that different R is contributed to sumLossWithoutAnchor
  // therefore sumLossWithoutAnchor had to be split into ejot and other, as well
  final double ejotAnchorLoss;
  final double otherAnchorLoss;
  final double sumLossWithoutAnchorEjot;
  final double sumLossWithoutAnchorOther;

  final double annualLossQ;
  final double facadeSquareWithoutHoles;
  final double ejotAnnualLoss, otherAnnualLoss;
  final ReportRow otherAnchor;
  final double r0norm;
  final List<ReportRow> rows;
  
  Report(this.ejotAnchorLoss, this.otherAnchorLoss, this.sumLossWithoutAnchorEjot, this.sumLossWithoutAnchorOther, this.rows, 
    this.otherAnchor, this.annualLossQ, this.facadeSquareWithoutHoles, this.r0norm, this.ejotAnnualLoss, this.otherAnnualLoss
    );
}

T sum<T extends num>(T lhs, T rhs) {
  return (lhs == null ? 0 : lhs) + (rhs == null ? 0 : rhs);
}


class ThermicCalc {
  
  ThermicCalc(AppStore appStore, AnchorCalc anchorCalc) : this.s = appStore, this.anchorCalc = anchorCalc;

  AppStore s;
  AnchorCalc anchorCalc;

  double calculatedInsulatorWidthEjot, calculatedInsulatorWidthOther;

  int get _buildingGroupType {
    int ret = 0;
    if (s.buildingType == 10 || s.buildingType == 9) {
      ret = 1;
    } else if (s.buildingType == 8 || s.buildingType == 9) {
      ret = 2;
    }
    return ret;
  }

  List<double> get _ab {
    const ab = [
      [0.00035, 1.4],
      [0.0003, 1.2],
      [0.0002, 1.0]
    ];
    return ab[_buildingGroupType];
  }

  bool get _isSpecial {
    return s.buildingType == 2 || s.buildingType == 5 || s.buildingType == 6 || s.buildingType == 7 || s.buildingType == 3;
  }

  double get _tht {
    int idx = _isSpecial ? 4 : 0;
    return s.selectedCity.values[idx];
  }

  double get _zht {
    int idx = _isSpecial ? 3 : 1;
    return s.selectedCity.values[idx];
  }

  double get _tint {
    return s.temperature.toDouble();
  }

  List<double> get _alphas {
    if (s.facade == FacadeType.SFTK) {
      return [23, 8.7];
    } else {
      return [23, 12];
    }
  }

  int get _lambdaIndex {
    String humMode = _calcHumMode(_tint, s.humidity.toDouble());
    return _calcHumCondition(humMode, s.selectedCity.zone);
  }

  double mulMillis(double a, double b, int count) {
    return (a == null ? 0.0 : a*0.001) * (b == null ? 0.0 : b*0.001) * (count == null ? 0.0 : count);
  }

  double sumMillis(List<double> d) {
    return d.reduce(sum) * 0.001;
  }
  
  int _calcHumCondition( String humMode, int humZone )
  {
    if ("dry" == humMode)
    {
      if (humZone == 3 || humZone == 2)
      {
          return 0;
      }
      else if (humZone == 1)
      {
          return 1;
      }
    }
    else if ("norm" == humMode)
    {
      if (humZone == 3)
      {
          return 0;
      }
      else if (humZone == 2 || humZone == 1)
      {
          return 1;
      }
    }
    return 1;
  }


  String _calcHumMode(double tInt, double hum)
  {
    if (tInt <= 12)
    {
      if (hum <= 60)
      {
          return "dry";
      }
      else if (hum <= 75)
      {
          return "norm";
      }
      else if (hum > 75)
      {
          return "hum";
      }
    }
    else if (tInt <= 24)
    {
      if (hum <= 50)
      {
          return "dry";
      }
      else if (hum <= 60)
      {
          return "norm";
      }
      else if (hum <= 75)
      {
          return "hum";
      }
      else if (hum > 75)
      {
          return "wet";
      }
    }
    else 
    {
      if (hum <= 40)
      {
          return "dry";
      }
      else if (hum <= 50)
      {
          return "norm";
      }
      else if (hum <= 60)
      {
          return "hum";
      }
      else if (hum > 60)
      {
          return "wet";
      }
    }
    return "norm";
  }

  List<Block> getBlocks() {
    return s.facade == FacadeType.SFTK ? s.sftkBlocks : s.nfsBlocks;
  }


  double get facadeSquareWithoutHoles {
    List<Block> blocks = getBlocks();
    double diff = 0;
    for (int i=0; i<blocks.length; ++i) {
      Block b = blocks[i];
      if (!b.enabled) continue;
      if (b.blockBinding == BlockBinding.WINDOW_BINDING) {
        for (int j=0; j<b.instances.length; ++j) {
          final inst = b.instances[j];
          final int blockCount = (inst.data['pBlockCount'].value as double).toInt();
          diff += mulMillis(inst.data['pWindowHeight'].value as double, inst.data['pWindowWidth'].value as double, blockCount);
          diff += mulMillis(inst.data['pDoorHeight'].value as double, inst.data['pDoorWidth'].value as double, blockCount);
        }
      }
    }
    return s.buildingFacadeSquare - diff;
  }

  double _getAnchorCountFromZones(List<double> boundaryZoneN, List<double> ordinaryZoneN) {
    double e = math.min(s.buildingWidth, s.buildingLength);
    double A = e/10;
    double B = math.max(s.buildingWidth, s.buildingLength) - 2*A;
    if (B > 2*A) {
      if (s.buildingHeight <= 16) {
        return roundDouble(ordinaryZoneN[0], 1);
      } else if (s.buildingHeight <= 40) {
        return roundDouble( (ordinaryZoneN[0] + ordinaryZoneN[1]) / 2, 1);
      } else {
        return roundDouble( (ordinaryZoneN[0] + ordinaryZoneN[1] + ordinaryZoneN[2]) / 3, 1);
      }
    } else {
      if (s.buildingHeight <= 16) {
        return roundDouble( (ordinaryZoneN[0] + boundaryZoneN[0]) / 2, 1);
      } else if (s.buildingHeight <= 40) {
        return roundDouble( (ordinaryZoneN[0] + boundaryZoneN[0] + ordinaryZoneN[1] + boundaryZoneN[1]) / 4, 1);
      } else {
        return roundDouble( (ordinaryZoneN[0] + boundaryZoneN[0] + ordinaryZoneN[1] + boundaryZoneN[1] + ordinaryZoneN[2] + boundaryZoneN[2]) / 6, 1);
      }
    }
  }

  double get otherAnchorPerM2 {
    if (s.facade == FacadeType.SFTK) {
      if (anchorCalc.cache.length > 3) {       
        return _getAnchorCountFromZones(anchorCalc.cache[2]['nm'], anchorCalc.cache[3]['nm']);
      }
    }
    else if (s.facade == FacadeType.NFS) {
      return s.avgPerm2NFS != null ? s.avgPerm2NFS.toDouble() : null;
    }

    return null;
  }

  double get ejotAnchorPerM2 {

    if (s.facade == FacadeType.SFTK) {
      if (anchorCalc.cache.length > 1) {
        return _getAnchorCountFromZones(anchorCalc.cache[0]['nm'], anchorCalc.cache[1]['nm']);
      }
    }
    else if (s.facade == FacadeType.NFS) {
      return s.avgPerm2NFS != null ? s.avgPerm2NFS.toDouble() : null;
    }

    return null;
  }

  LinkedHashSet<String> canCalculate() {
    LinkedHashSet<String> ret = LinkedHashSet<String>();
    if (s.buildingType == -1) {
      ret.add('buildingType');
    }
    if (s.selectedCity == null) {
      ret.add('selectCity');
    }
    if (!s.shouldCalculateInsulatorWidth && (s.insulatorWidth == null || s.insulatorWidth <= 0) || s.insulationMaterial1 == null) {
      ret.add('insulatorErr');
    }
    if (s.buildingFacadeSquare == null || s.buildingFacadeSquare <= 0) {
      ret.add('buildingFacadeSquare');
    } else {
      if (facadeSquareWithoutHoles <= 0) {
        ret.add('facadeSquareNegativeErr');
      }
    }
    if (null == ejotAnchorPerM2) {
      if (s.facade == FacadeType.SFTK) {
        ret.add('fillBuildingSizeErr');
      }
      else {
        ret.add('ejotAnchorPerM2Err');
      }
    }
    
    if (s.baseWidth == null || s.baseWidth <= 0) {
      ret.add('baseWidth');
    }
    if (s.coreMaterial == null) {
      ret.add('coreMaterial');
    }

    if (s.facade == FacadeType.SFTK && (anchorCalc.breakForceEjot() == null || anchorCalc.breakForceEjot() <= 0)) {
      ret.add('pBreakForceEjot');
    }
    if (s.facade == FacadeType.NFS) {
      if (s.avgPerm2NFS == null || s.avgPerm2NFS <= 0) {
        ret.add('pAvgPerMeterSq');
      }
      if (s.insulationMaterial2 == null) {
        if (s.insulatorFixedWidthNFS != null && s.insulatorFixedWidthNFS > 0)
          ret.add('insulatorErr');
      }
      if (s.insulatorFixedWidthNFS != null && s.insulatorFixedWidthNFS > 0) {
        if (s.insulationMaterial2 == null)
          ret.add('insulatorWidthExternal');
      }
    }

    return ret;
  }


  bool hasComparison() {   
    return (s.dishDistance != null && s.dishDistance > 0.0) && 
      (s.facade == FacadeType.NFS || (s.breakForceOther != null && s.breakForceOther > 0.0)) &&
      (s.facade == FacadeType.SFTK || (s.avgPerm2NFS != null && s.avgPerm2NFS > 0.0));
  }

  double _calcPerimeterMeters(BlockParams bp) {
    var wh = bp.data['pWindowHeight'].value;
    var ww = bp.data['pWindowWidth'].value;
    var dh = bp.data['pDoorHeight'].value;
    var dw = bp.data['pDoorWidth'].value;

    double total = 0;
    if (wh != null && ww != null && wh * ww > 0)
      total += 2 * (ww + wh);
    if (dh != null && dw != null && dh * dw > 0)
      total += 2 * (dh + dw);
    return total * 0.001;
  }

  _SumLoss _calcLoss(double rIso, double lambdaWall, double A, double u) {
    List<_Loss> lines = [];
    double sumLossWithoutAnchor = u;
    double ejotAnchorLoss = ejotAnchorPerM2 * 0.001;
    double otherAnchorLoss = 0.0;

    List<Block> blocks = getBlocks();

    for (int i=0; i<blocks.length; ++i) {
      Block b = blocks[i];

      if ( b.blockBinding == BlockBinding.INSULATION_BINDING) {
        if (hasComparison()) {
          final unitGeo = otherAnchorPerM2;
          final unitLoss = lerpTable(1, s.dishDistance, anchorLoss, true);
          otherAnchorLoss = unitGeo * unitLoss;
          lines.add( _Loss(unitGeo, unitLoss) );
        } else {
          lines.add(null);
        }
        continue;
      }

      if (!b.enabled) continue;

      for (int j=0; j<b.instances.length; ++j) {
        final inst = b.instances[j];
        if (!inst.isValid()) continue;

        _Loss loss;

        try {
          switch (b.blockBinding) {
            
            case BlockBinding.ANGLE_BINDING:
              final unitGeo = roundDouble(inst.data['pTotalAngleLength'].value/A, 3);
              final unitLoss = lerpZxy(lambdaWall, rIso, 
                inst.data['pAngleType'].value == AngleType.concave ? angleLossConcave : angleLossConvex);
              sumLossWithoutAnchor += unitGeo * unitLoss;
              loss = _Loss(unitGeo, unitLoss);
            break;

            case BlockBinding.BALCONY_BINDING:
              if (inst.data['pPlateThickness'].value > 0) {
                double d160 = lerpZxy(lambdaWall, rIso, balconyByPerfLoss160[inst.data['pInsulationTypePerforated'].value]);
                double d210 = lerpZxy(lambdaWall, rIso, balconyByPerfLoss210[inst.data['pInsulationTypePerforated'].value]);
                final unitGeo = roundDouble(inst.data['pTotalPlateLength'].value/A, 3);
                final unitLoss = lerp(inst.data['pPlateThickness'].value, 160, 210, d160, d210);
                sumLossWithoutAnchor += unitGeo * unitLoss;
                loss = _Loss(unitGeo, unitLoss);
              }
            break;

            case BlockBinding.WINDOW_BINDING:
              final tableByOverlap = [
                [0.0,  lerpZxy(lambdaWall, rIso, windowByFrameLoss0[inst.data['pInsulationTypeWindow'].value])],
                [20.0, lerpZxy(lambdaWall, rIso, windowByFrameLoss20[inst.data['pInsulationTypeWindow'].value])],
                [60.0, lerpZxy(lambdaWall, rIso, windowByFrameLoss60[inst.data['pInsulationTypeWindow'].value])]
              ];
              final unitGeo = roundDouble( _calcPerimeterMeters(inst) * inst.data['pBlockCount'].value / A, 3);
              final unitLoss = lerpTable(1, inst.data['pInsulationOverlapThickness'].value, tableByOverlap, true);
              sumLossWithoutAnchor += unitGeo * unitLoss;
              loss = _Loss(unitGeo, unitLoss);
            break;

            case BlockBinding.BASEMENT_BINDING:
              if (inst.data['pThermalResistanceOverlap'].value > 0) {
                final unitGeo = inst.data['pTotalOverlapLength'].value / A;
                final unitLoss = lerpZaxy(inst.data['pThermalResistanceOverlap'].value, lambdaWall, rIso, basementByRLoss);
                sumLossWithoutAnchor += unitGeo * unitLoss;
                loss = _Loss(unitGeo, unitLoss);
              }
            break;

            case BlockBinding.BRACKET_AL_BINDING:
              final unitGeo = roundDouble(inst.data['pBracketAvgCountPerMeterSq'].value, 3);
              final unitLoss = lerpZxy(lambdaWall, rIso, 
                inst.data['pBracketType'].value == BracketType.rect ? alBracketRectLoss[inst.data['pBracketPredefinedSize'].value] : alBracketHalfRectLoss[inst.data['pBracketPredefinedSize'].value]);
              sumLossWithoutAnchor += unitGeo * unitLoss;
              loss = _Loss(unitGeo, unitLoss);
            break;

            case BlockBinding.BRACKET_STEEL_BINDING:
              int nk = (inst.data['pBracketAvgPerMeter'].value as BracketAvgPerMeter).index;
              final unitGeo = roundDouble(inst.data['pBracketAvgCountPerMeterSq'].value, 3);
              final unitLoss = lerpZxy( lambdaWall, rIso, steelBracketByNLoss[nk].item2 );
              sumLossWithoutAnchor += unitGeo * unitLoss;
              loss = _Loss(unitGeo, unitLoss);
            break;

            case BlockBinding.FIRESTOP_BINDING:
              final unitGeo = roundDouble(inst.data['pFireStopLength'].value/A, 3);
              final unitLoss = lerpZxy( lambdaWall, rIso, fireStopperLoss);
              sumLossWithoutAnchor += unitGeo * unitLoss;
              loss = _Loss(unitGeo, unitLoss);
            break;

            case BlockBinding.UNIVERSAL:
              final unitGeo = roundDouble(inst.data['pValuePerGeometricSpec'].value * 
                1.0 / ((inst.data['pElementDimensionType'].value as Dimension) == Dimension.point ? 1.0 : A), 
              3);
              final unitLoss = inst.data['pUnitHeatLoss'].value;
              sumLossWithoutAnchor += unitGeo * unitLoss;
              loss = _Loss(unitGeo, unitLoss);
            break;

            default:
            break;
          }

          lines.add(loss);
        }
        catch (e) {
          print('ERROR '+b.blockBinding.toString() + ', '+ b.blockName);
          print(e.toString());
          //debugPrintStack();
          throw e;
        }
      }
    }

    return _SumLoss(lines, ejotAnchorLoss, otherAnchorLoss, sumLossWithoutAnchor);
  }


  double _findRiso(bool forEjot) {
    
    final alphas = _alphas;
    final lambdaIndex = _lambdaIndex;
    final lambdaWall = s.coreMaterial.conductivity[lambdaIndex];

    final ab = _ab;
    final ddhp = roundDouble( (_tint - _tht) * _zht, 3);
    final r0norm = roundDouble( ab[0] * ddhp + ab[1], 4);

    double r = s.facade == FacadeType.SFTK ? 0.6 : 0.95;
    double rUslEst = r0norm / r;
    double rIsoEst = rUslEst - 1/alphas[0] - 1/alphas[1] - s.baseWidth * 0.001 / lambdaWall;

    double u = roundDouble( 1/rUslEst, 4 );
    final A = facadeSquareWithoutHoles;
    _SumLoss losses = _calcLoss(rIsoEst, lambdaWall, A, u);
    
    double rEst = 1/(losses.sumLossWithoutAnchor + (forEjot ? losses.ejotAnchorLoss : losses.otherAnchorLoss));

    // dichotomy: given nonlinear function rEst(rUslEst), find pair of arguments rUslEstPrev / rUslEst, between these values 
    // we later find rUslEst' that best matches r0norm
    double rUslEstPrev, rEstPrev;

    const ITERATIONS = 16;

    if (rEst < r0norm) {
      for (int i=0; i<ITERATIONS && rEst < r0norm; ++i) {
        rUslEstPrev = rUslEst;
        rEstPrev = rEst;
        rUslEst *= 1.1;
        u = roundDouble( 1/rUslEst, 5 );
        rIsoEst = rUslEst - 1/alphas[0] - 1/alphas[1] - s.baseWidth * 0.001 / lambdaWall;
        losses = _calcLoss(rIsoEst, lambdaWall, A, u);
        rEst = 1/(losses.sumLossWithoutAnchor + (forEjot ? losses.ejotAnchorLoss : losses.otherAnchorLoss));
      }
    } else {
      for (int i=0; i<ITERATIONS && rEst > r0norm; ++i) {
        rUslEstPrev = rUslEst;
        rEstPrev = rEst;
        rUslEst *= 0.9;
        u = roundDouble( 1/rUslEst, 3 );
        rIsoEst = rUslEst - 1/alphas[0] - 1/alphas[1] - s.baseWidth * 0.001 / lambdaWall;
        losses = _calcLoss(rIsoEst, lambdaWall, A, u);
        rEst = 1/(losses.sumLossWithoutAnchor + (forEjot ? losses.ejotAnchorLoss : losses.otherAnchorLoss));
      }
    }

    // could not find sides?
    if ((rEst - r0norm).sign == (rEstPrev - r0norm).sign) {
      print('could not find rEst around r0norm '+rEstPrev.toString()+', '+rEst.toString());
    }
    else {
      for (int i=0; i<ITERATIONS; ++i) {
        double rUslEstHalf = rUslEstPrev + (rUslEst - rUslEstPrev) * 0.5;
        u = roundDouble( 1/rUslEstHalf, 5 );
        double rIsoEstHalf = rUslEstHalf - 1/alphas[0] - 1/alphas[1] - s.baseWidth * 0.001 / lambdaWall;
        losses = _calcLoss(rIsoEstHalf, lambdaWall, A, u);
        double rEstHalf = 1/(losses.sumLossWithoutAnchor + (forEjot ? losses.ejotAnchorLoss : losses.otherAnchorLoss));

        if ( (rEstHalf - r0norm).sign != (rEstPrev - r0norm).sign ) {
          rUslEst = rUslEstHalf;
          rEst = rEstHalf;
        } else {
          rUslEstPrev = rUslEstHalf;
          rEstPrev = rEstHalf;
        }
      }
    }

    final double delta0 = (rEstPrev - r0norm).abs() / r0norm;
    final double delta1 = (rEst - r0norm).abs() / r0norm;

    if (delta0 < delta1) {
      rIsoEst = rUslEstPrev - 1/alphas[0] - 1/alphas[1] - s.baseWidth * 0.001 / lambdaWall;
    } else {
      rIsoEst = rUslEst - 1/alphas[0] - 1/alphas[1] - s.baseWidth * 0.001 / lambdaWall;
    }

    return roundDouble(rIsoEst, 5);
  }

  double calculateRIsoFixedOptional(int lambdaIndex) {
    if (FacadeType.NFS == s.facade) {
      if (s.insulationMaterial2 != null && s.insulatorFixedWidthNFS != null && s.insulatorFixedWidthNFS > 0)
        return roundDouble( s.insulatorFixedWidthNFS * 0.001 / s.insulationMaterial2.conductivity[lambdaIndex], 5 );
    }
    return 0.0;
  }


  Report calculate() {
    final ab = _ab;
    final ddhp = roundDouble( (_tint - _tht) * _zht, 3);
    final r0norm = roundDouble( ab[0] * ddhp + ab[1], 5);

    final alphas = _alphas;
    final lambdaIndex = _lambdaIndex;
    final lambdaIso = s.insulationMaterial1.conductivity[lambdaIndex];
    final lambdaWall = s.coreMaterial.conductivity[lambdaIndex];

    // sum iso fixed part + variable part
    double rIso;

    // iso fixed part
    double rIsoFixedPart = 0.0;
    if (FacadeType.NFS == s.facade) 
      rIsoFixedPart = calculateRIsoFixedOptional(lambdaIndex);

    if (s.shouldCalculateInsulatorWidth) {
      // find matching iso resitance for use with ejot (we derive width from it, then round it, then recalculate rIso)
      rIso = _findRiso(true);

      // iso variable part width with ejot
      int d = ((rIso - rIsoFixedPart) * lambdaIso * 1000).round();
      if (d % 5 == 0)
        d -= 1;
      calculatedInsulatorWidthEjot = (d / 10.0).round() * 10.0;

      // as the width was just round, we should recalculate resistance of variable part
      final double rIsoEjotAfterRounding = rIsoFixedPart + calculatedInsulatorWidthEjot * 0.001 / lambdaIso;
      rIso = rIsoEjotAfterRounding;
      
      // should also calculate and round iso width for use with other anchor
      double otherAnchorMatchingRIso = _findRiso(false); 
      d = ((otherAnchorMatchingRIso - rIsoFixedPart) * lambdaIso * 1000).round();
      if (d % 5 == 0)
        d += 1;
      calculatedInsulatorWidthOther = (d / 10.0).round() * 10.0;
    } else {
      rIso = roundDouble( s.insulatorWidth * 0.001 / lambdaIso, 5 );
      if (FacadeType.NFS == s.facade) 
        rIso += calculateRIsoFixedOptional(lambdaIndex);
    }

    // calculate losses report per block, based on calculated iso resistance (rIso)
    double rusl = roundDouble( 1/alphas[0] + 1/alphas[1] + rIso + s.baseWidth * 0.001 / lambdaWall, 5 ); 
    double u = roundDouble( 1/rusl, 5 );
    final A = facadeSquareWithoutHoles;
    _SumLoss ejotAnchorLosses = _calcLoss(rIso, lambdaWall, A, u);

    ReportRow otherAnchor;
    List<Block> blocks = getBlocks();
    final List<ReportRow> lines = [
      ReportRow('elementBase', null, 1.0, u, Dimension.planar),
      ReportRow('ejotAnchor', null, ejotAnchorPerM2, 0.001, Dimension.point)  // ejot anchor has lowest losses, 0.001
    ];

    int lossIndex = 0;
    for (int i=0; i<blocks.length; ++i) {
      Block b = blocks[i];

      if (b.blockBinding == BlockBinding.INSULATION_BINDING) {
        _Loss loss = ejotAnchorLosses.lines[lossIndex++];
        if (null != loss) {
          otherAnchor = ReportRow( 'otherAnchor', null, loss.unitGeo, loss.unitLoss, Dimension.point );
          lines.add(otherAnchor);
        }
        continue;
      }

      if (!b.enabled) continue;      

      for (int j=0; j<b.instances.length; ++j) {
        final inst = b.instances[j];
        if (!inst.isValid()) continue;

        _Loss loss = ejotAnchorLosses.lines[lossIndex++];
        if (null == loss) continue;

        try {
          switch (b.blockBinding) {
            case BlockBinding.ANGLE_BINDING:
              lines.add(ReportRow( b.blockName, inst, loss.unitGeo, loss.unitLoss, Dimension.linear ));
            break;

            case BlockBinding.BALCONY_BINDING:
              lines.add(ReportRow( b.blockName, inst, loss.unitGeo, loss.unitLoss, Dimension.linear ));
            break;

            case BlockBinding.WINDOW_BINDING:
              lines.add(ReportRow( b.blockName, inst, loss.unitGeo, loss.unitLoss, Dimension.linear ));
            break;

            case BlockBinding.BASEMENT_BINDING:
              lines.add(ReportRow( b.blockName, inst, loss.unitGeo, loss.unitLoss, Dimension.linear ));
            break;

            case BlockBinding.BRACKET_AL_BINDING:
              lines.add(ReportRow( b.blockName, inst, loss.unitGeo, loss.unitLoss, Dimension.point ));
            break;

            case BlockBinding.BRACKET_STEEL_BINDING:
              lines.add(ReportRow( b.blockName, inst, loss.unitGeo, loss.unitLoss, Dimension.point ));
            break;

            case BlockBinding.FIRESTOP_BINDING:
              lines.add(ReportRow( b.blockName, inst, loss.unitGeo, loss.unitLoss, Dimension.linear ));
            break;

            case BlockBinding.UNIVERSAL:
              lines.add(ReportRow( b.blockName, inst, loss.unitGeo, loss.unitLoss, 
                inst.data['pElementDimensionType'].value == bw.Dimension.linear ? Dimension.linear : Dimension.point
              ));
            break;

            default:
            break;
          }
        }
        catch (e) {
          print('ERROR '+b.blockBinding.toString() + ', '+ b.blockName);
          print(e.toString());
          //debugPrintStack();
          throw e;
        }
      }
    }

    // calculate total heat loss
    
    double sumLoss = ejotAnchorLosses.sumLossWithoutAnchor + ejotAnchorLosses.ejotAnchorLoss;
    for (var i=0; i<lines.length; ++i) {
      final line = lines[i];
      if (line.blockName != 'otherAnchor') {
        line.lossPercent = roundDouble( (line.loss / sumLoss) * 100, 1 );
      }
    }
   
    double annualLossQ = 0.0000206 * ddhp * A;
    double ejotAnnualLoss = roundDouble(annualLossQ*(ejotAnchorLosses.sumLossWithoutAnchor + ejotAnchorLosses.ejotAnchorLoss), 2);

    double otherAnnualLoss;
    double otherAnchorLoss;
    double sumLossWithoutAnchorOther;

    if (s.shouldCalculateInsulatorWidth) {
      // iso width is calculated and may be different for ejot and other anchor schemes,
      // so we calculate other anchor loss based on other anchor calculated r
      double otherAnchorMatchingRIso = rIsoFixedPart + calculatedInsulatorWidthOther * 0.001 / lambdaIso;
      final rusl = roundDouble( 1/alphas[0] + 1/alphas[1] + otherAnchorMatchingRIso + s.baseWidth * 0.001 / lambdaWall, 5 );    
      final u = roundDouble( 1/rusl, 5 );
      _SumLoss otherAnchorLosses = _calcLoss(otherAnchorMatchingRIso, lambdaWall, A, u);
      otherAnnualLoss = roundDouble(annualLossQ*(otherAnchorLosses.sumLossWithoutAnchor + otherAnchorLosses.otherAnchorLoss), 2);
      otherAnchorLoss = otherAnchorLosses.otherAnchorLoss;
      sumLossWithoutAnchorOther = otherAnchorLosses.sumLossWithoutAnchor;
    } else {
      // when iso width is fixed, we already know other anchor r 
      otherAnnualLoss = roundDouble(annualLossQ*(ejotAnchorLosses.sumLossWithoutAnchor + ejotAnchorLosses.otherAnchorLoss), 2);
      otherAnchorLoss = ejotAnchorLosses.otherAnchorLoss;
      sumLossWithoutAnchorOther = ejotAnchorLosses.sumLossWithoutAnchor;
    }

    return Report(ejotAnchorLosses.ejotAnchorLoss, otherAnchorLoss, ejotAnchorLosses.sumLossWithoutAnchor, sumLossWithoutAnchorOther, lines, otherAnchor, annualLossQ, A, r0norm, 
      ejotAnnualLoss, otherAnnualLoss);
  }


}