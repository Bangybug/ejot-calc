import 'package:ejot_ru/ui/pages/BasePage.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';

mixin ScrollControllerMixin {
  ScrollController scrollController = new ScrollController();
  bool _isScrollingDown = false;
  

  void initScroll(GlobalKey<BasePageState> _pageKey) {
    scrollController.addListener(() {
      if (scrollController.position.userScrollDirection == ScrollDirection.reverse) {
        if (!_isScrollingDown) {
          _isScrollingDown = true;
          _pageKey.currentState.animateTopContent(false);
          
        }
      }
      // if (scrollController.position.userScrollDirection == ScrollDirection.forward) {
      //   if (_isScrollingDown) {
      //     _isScrollingDown = false;
      //     _pageKey.currentState.animateTopContent(true);
      //   }
      // }
    });
  }

  void disposeScroll() {
    scrollController.dispose();
  }

  
}
