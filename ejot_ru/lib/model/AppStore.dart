import 'package:ejot_ru/calculator/AnchorCalc.dart';
import 'package:ejot_ru/calculator/EconomicCalc.dart';
import 'package:ejot_ru/calculator/ThermicCalc.dart';
import 'package:ejot_ru/data/persistence.dart';
import 'package:ejot_ru/model/Block.dart';
import 'package:ejot_ru/model/WindZone.dart';
import 'package:mobx/mobx.dart';

import 'City.dart';
import 'Material.dart';
import 'SftkClass.dart';
import 'TerrainType.dart';
part 'AppStore.g.dart';

class AppStore extends _AppStore with _$AppStore {

  static const CURRENT_VERSION = 'July 30th 2020';

  AnchorCalc anchorCalc;
  ThermicCalc thermicCalc;
  EconomicCalc ecoCalc;
  Report thermicReport;

  Persistence persistence = new Persistence();

  Map<String, dynamic> toJson() => {
    'currentVersion': CURRENT_VERSION,
    'forceLanguage': forceLanguage,
    'description': description,
    'buildingType': buildingType,
    'baseWidth': baseWidth,
    'selectedCity': selectedCity?.toJson(),
    'temperature': temperature,
    'humidity': humidity,
    'facade': facade.index,
    'buildingLength': buildingLength,
    'buildingHeight': buildingHeight,
    'buildingWidth': buildingWidth,
    'buildingFacadeSquare': buildingFacadeSquare,
    'sftkClass': sftkClass?.id,
    'terrainType': terrainType?.id,
    'insulationMaterial1': insulationMaterial1?.toJson(),
    'insulationMaterial2': insulationMaterial2?.toJson(),
    'coreMaterial': coreMaterial?.toJson(),
    'blockAngleBinding': blockAngleBinding.toJson(),
    'blockBalconyBinding': blockBalconyBinding.toJson(), 
    'blockWindowBinding': blockWindowBinding.toJson(),
    'blockBasementBinding': blockBasementBinding.toJson(),
    'blockUniversalBinding': blockUniversalBinding.toJson(),
    'blockAlBracketBinding': blockAlBracketBinding.toJson(),
    'blockSteelBracketBinding': blockSteelBracketBinding.toJson(),
    'blockFirestopBinding': blockFirestopBinding.toJson(),
    'econonmyAnchorCostEnabled': econonmyAnchorCostEnabled,
    'econonmyInsulatorCostEnabled': econonmyInsulatorCostEnabled,
    'econonmyEnergyCostEnabled': econonmyEnergyCostEnabled,
    'economyCostAnchor': economyCostAnchor,
    'economyCostOtherAnchor': economyCostOtherAnchor,
    'economyCostInsulator': economyCostInsulator,
    'economyCostEnergy': economyCostEnergy,
    'windZone': windZone != null ? windZones.indexOf(windZone) : null,
    'insulatorWidth': insulatorWidth,
    'insulatorFixedWidthNFS': insulatorFixedWidthNFS,
    'shouldCalculateInsulatorWidth': shouldCalculateInsulatorWidth,
    'avgPerm2NFS': avgPerm2NFS,
    'breakForceEjot': breakForceEjot,
    'breakForceOther': breakForceOther,
    'dishDistance': dishDistance
  };
  
  load() async {
    try 
    {  
      Map<String, dynamic> jstate = await persistence.read('state');
      // Map<String, dynamic> jstate = json.decode(testStateJson);

      String version = jstate['currentVersion'];
      if (version == null || version != CURRENT_VERSION) {
        return;
      }

      forceLanguage = jstate['forceLanguage'];
      description = jstate['description'];
      buildingType = jstate['buildingType'];

      if (jstate.containsKey('selectedCity')) {
        if (jstate['selectedCity'] != null) 
          selectedCity = City.fromJson(jstate['selectedCity']);
      }

      temperature = jstate['temperature'];
      if (jstate.containsKey('humidity')) {
        humidity = jstate['humidity'];
      }
      facade = FacadeType.values[jstate['facade']];
      insulatorWidth = jstate['insulatorWidth'];
      insulatorFixedWidthNFS = jstate['insulatorFixedWidthNFS'];
      shouldCalculateInsulatorWidth = jstate['shouldCalculateInsulatorWidth'] ?? false;
      //baseWidth = jstate['baseWidth'];
      baseWidth = 250.0;
      buildingLength = jstate['buildingLength'];
      buildingHeight = jstate['buildingHeight'];
      buildingWidth = jstate['buildingWidth'];
      buildingFacadeSquare = jstate['buildingFacadeSquare'];
      if (jstate.containsKey('sftkClass')) {
        if (jstate['sftkClass'] != null) 
          sftkClass = sftkClasses[ jstate['sftkClass'] ];
      }
      if (jstate.containsKey('terrainType')) {
        if (jstate['terrainType'] != null) 
          terrainType = terrainTypes[ jstate['terrainType'] ];
      }

      if (jstate.containsKey('insulationMaterial1')) {
        if (jstate['insulationMaterial1'] != null) 
          insulationMaterial1 = Material.fromJson(jstate['insulationMaterial1'], jstate['insulationMaterial1']['id']);
      }

      if (jstate.containsKey('insulationMaterial2')) {
        if (jstate['insulationMaterial2'] != null) 
          insulationMaterial2 = Material.fromJson(jstate['insulationMaterial2'], jstate['insulationMaterial2']['id']);
      }

      if (jstate.containsKey('coreMaterial')) {
        if (jstate['coreMaterial'] != null) 
          coreMaterial = Material.fromJson(jstate['coreMaterial'], jstate['coreMaterial']['id']);
      }

      if (jstate.containsKey('blockAngleBinding'))
        blockAngleBinding.fromJson(jstate['blockAngleBinding']);

      if (jstate.containsKey('blockBalconyBinding'))
        blockBalconyBinding.fromJson(jstate['blockBalconyBinding']);

      if (jstate.containsKey('blockWindowBinding'))
        blockWindowBinding.fromJson(jstate['blockWindowBinding']);

      if (jstate.containsKey('blockBasementBinding'))
        blockBasementBinding.fromJson(jstate['blockBasementBinding']);

      if (jstate.containsKey('blockUniversalBinding'))
        blockUniversalBinding.fromJson(jstate['blockUniversalBinding']);

      if (jstate.containsKey('blockAlBracketBinding'))
        blockAlBracketBinding.fromJson(jstate['blockAlBracketBinding']);

      if (jstate.containsKey('blockSteelBracketBinding'))
        blockSteelBracketBinding.fromJson(jstate['blockSteelBracketBinding']);

      if (jstate.containsKey('blockFirestopBinding'))
        blockFirestopBinding.fromJson(jstate['blockFirestopBinding']);

      breakForceEjot = jstate['breakForceEjot'] ?? 0.35;
      breakForceOther = jstate['breakForceOther'];
      dishDistance = jstate['dishDistance'];

      econonmyAnchorCostEnabled = jstate['econonmyAnchorCostEnabled'] ?? false;
      econonmyInsulatorCostEnabled = jstate['econonmyInsulatorCostEnabled'] ?? false;
      econonmyEnergyCostEnabled = jstate['econonmyEnergyCostEnabled'] ?? false;

      economyCostAnchor = jstate['economyCostAnchor'];
      economyCostOtherAnchor = jstate['economyCostOtherAnchor'];
      economyCostInsulator = jstate['economyCostInsulator'];
      economyCostEnergy = jstate['economyCostEnergy'];

      if (jstate.containsKey('windZone') && jstate['windZone'] != null) {
        windZone = windZones[jstate['windZone']];
      }

      avgPerm2NFS = jstate['avgPerm2NFS'] ?? 5;

    }
    catch (e) {
      print('load configuration ERROR');  
      print(e);
    }
  }

  save() async {
    Map<String, dynamic> params = toJson();
    persistence.save('state', params);
  }

  bool recalculate() {
    anchorCalc ??= AnchorCalc(this);
    thermicCalc ??= ThermicCalc(this, anchorCalc);
    ecoCalc ??= EconomicCalc(this, thermicCalc);

    final hadErrors = hasErrors();
    errCodesAnchor.clear();
    anchorCalc.cache = [];
    if (facade == FacadeType.SFTK) {
      errCodesAnchor.addAll( anchorCalc.canCalculate() );
      if (errCodesAnchor.isEmpty)
        anchorCalc.calculate();
    }

    errCodesThermic.clear();
    errCodesThermic.addAll(thermicCalc.canCalculate());
    if (errCodesAnchor.isEmpty && errCodesThermic.isEmpty) {
      thermicReport = thermicCalc.calculate();
    }

    if (ecoCalc.canCalculate()) {
      ecoCalc.calculate(thermicReport);
    }

    // FYI I didn't modify any observables in this method, because I want it
    // to be called within widget.build. If I change state during build, it will be 
    // a problem. 
    // So the rule of thumb is as follows: don't change states inside build. It cannot be worked around.    
    return hadErrors != hasErrors();
  }

  bool hasErrors() {
    return errCodesAnchor.isNotEmpty || errCodesThermic.isNotEmpty;
  }

  bool hasTwoInsulators() {
    return (facade == FacadeType.NFS && insulatorFixedWidthNFS != null && insulatorFixedWidthNFS > 0);
  }

}

// FYI to start autogen, run this
// flutter packages pub run build_runner watch

enum FacadeType { SFTK, NFS }

abstract class _AppStore with Store {
  String forceLanguage;
  String description;

  @observable
  int pageIndex = 0;

  int buildingType = -1;

  @observable
  City selectedCity;

  @observable
  int temperature = 21;

  @observable
  int humidity = 55;

  @observable 
  FacadeType facade = FacadeType.SFTK;

  int avgPerm2NFS = 5;

  double baseWidth = 250;

  double insulatorWidth;
  double insulatorFixedWidthNFS;

  @observable
  bool shouldCalculateInsulatorWidth = false;

  double buildingLength;

  double buildingHeight;

  double buildingWidth;

  double buildingFacadeSquare; 

  @observable
  SftkClass sftkClass;

  @observable
  TerrainType terrainType;

  @observable
  Material insulationMaterial1;

  @observable
  Material insulationMaterial2;

  @observable
  Material coreMaterial;

  // FYI although blocks are generalized enough, I cannot observe them individually within a list
  // so there is no point in observing block references, since I made blocks mutable
  // as a result, I turned my Block class to a Store
  final Block blockInsulationBinding = Block(BlockBinding.INSULATION_BINDING);
  final Block blockAngleBinding = Block(BlockBinding.ANGLE_BINDING);
  final Block blockBalconyBinding = Block(BlockBinding.BALCONY_BINDING);
  final Block blockWindowBinding = Block(BlockBinding.WINDOW_BINDING);
  final Block blockBasementBinding = Block(BlockBinding.BASEMENT_BINDING);
  final Block blockUniversalBinding = Block(BlockBinding.UNIVERSAL);
  final Block blockAlBracketBinding = Block(BlockBinding.BRACKET_AL_BINDING);
  final Block blockSteelBracketBinding = Block(BlockBinding.BRACKET_STEEL_BINDING);
  final Block blockFirestopBinding = Block(BlockBinding.FIRESTOP_BINDING);

  // FYI observable list can observe its own reference and its items references
  // but it is not possible to write an Observer for an individual item within a list

  // So if you have quite a large observable list, and some item changes, you will rebuild entire list, because observers dont work on individual items
  // but if you have more granular observers per each item, you could do more granular reactions, every item must be a store.

  // FYI observables cannot be static due to mobs, so I have to initialize some of them 
  List<Block> _sftkBlocks;
  List<Block> get sftkBlocks {
    if (null == _sftkBlocks) {
      _sftkBlocks = [ blockInsulationBinding, blockBalconyBinding, blockWindowBinding, blockBasementBinding, blockAngleBinding, blockUniversalBinding ];
    }
    return _sftkBlocks;
  }
  
  List<Block> _nfsBlocks;
  List<Block> get nfsBlocks {
    if (null == _nfsBlocks) {
      _nfsBlocks = [ 
        blockInsulationBinding, blockAlBracketBinding, blockSteelBracketBinding, blockFirestopBinding, 
        blockBalconyBinding, blockWindowBinding, blockBasementBinding, blockAngleBinding, blockUniversalBinding
      ];
    }
    return _nfsBlocks;
  }
  
  

  // FYI for custom blocks I can use ObservableList, as this list is modifiable
  bool econonmyAnchorCostEnabled = false;
  bool econonmyInsulatorCostEnabled = false;
  bool econonmyEnergyCostEnabled = false;

  double economyCostAnchor;
  double economyCostOtherAnchor;
  double economyCostInsulator;
  double economyCostEnergy;


  @observable
  WindZone windZone;

  @observable
  int lastUpdatedCount = 0;

  @observable
  int lastCalculatedCount = -1;

  Set<String> errCodesAnchor = Set<String>();
  Set<String> errCodesThermic = Set<String>();

  bool shouldScrollToError = false;

  double breakForceEjot;
  double breakForceOther;
  double dishDistance;

}
