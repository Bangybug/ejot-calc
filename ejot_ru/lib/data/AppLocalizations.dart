import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart' show SynchronousFuture;

import 'constants.dart';

class AppLocalizations {
  AppLocalizations(this.locale);

  static String forceLanguage;

  final Locale locale;

  static AppLocalizations of(BuildContext context) {
    return Localizations.of<AppLocalizations>(context, AppLocalizations);
  }

  static Map<String, Map<String, String>> _localizedValues = initValues();
  
  String getString(String id) {
    if (id == null) {
      return '';
    }
    String languageCode = forceLanguage != null ? forceLanguage : locale.languageCode;
    var loc = _localizedValues[languageCode][id];
    // var loc = _localizedValues['ru'][id];
    if (loc == null) {
      loc = '%'+id+'%';
    }
    return loc;
  }

  String get title {
    return getString('title');
  }

  List<String> get stepNames {
    return new List<String>.generate(STEPS_TOTAL, (i) => getString('step'+(i+1).toString()));
  }
}


class AppLocalizationsDelegate extends LocalizationsDelegate<AppLocalizations> {
  const AppLocalizationsDelegate();

  @override
  bool isSupported(Locale locale){
    return ['en', 'ru'].contains(locale.languageCode);
  } 

  @override
  Future<AppLocalizations> load(Locale locale) {
    // Returning a SynchronousFuture here because an async "load" operation
    // isn't needed to produce an instance of AppLocalizations.
    return SynchronousFuture<AppLocalizations>(AppLocalizations(locale));
  }

  @override
  bool shouldReload(AppLocalizationsDelegate old) => false;
}