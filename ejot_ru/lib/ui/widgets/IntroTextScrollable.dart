import 'package:ejot_ru/data/AppLocalizations.dart';
import 'package:ejot_ru/model/AppStore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:provider/provider.dart' as P;

// https://dart-lang.github.io/markdown/

class IntroTextScrollable extends StatefulWidget {
  IntroTextScrollable(this.handlePageChange) : super();

  final ValueChanged<int> handlePageChange;

  @override
  _IntroTextScrollableState createState() => _IntroTextScrollableState();
}

class MyBullet extends StatelessWidget {
  MyBullet(this.color, this.onTap): super();
  final Color color;
  final GestureTapCallback onTap;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        height: 16.0,
        width: 16.0,
        margin: EdgeInsets.only(right: 20),
        decoration: BoxDecoration(
          color: color,
          shape: BoxShape.circle,
        ),
      )
    );
  }
}


class _IntroTextScrollableState extends State<IntroTextScrollable> {

  List<String> _slides;
  int _slideIndex = 0;

  Widget _buildBullets(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: List.generate(3, (int i) => MyBullet(i == _slideIndex ? Theme.of(context).secondaryHeaderColor : Colors.grey, () {
        setState(() {
          _slideIndex = i;
        });
      }))
    );
  }

  Widget _buildSwitcher(BuildContext context) {
    return AnimatedSwitcher(
      duration: const Duration(milliseconds: 500),
      transitionBuilder: (Widget child, Animation<double> animation) {
        return ScaleTransition(child: child, scale: animation);
      },
      child: _buildText(context, _slideIndex)
    );
  }

  Widget _buildText(BuildContext context, int slideIndex) {
    return Scrollbar(
      child: SingleChildScrollView(
        padding: const EdgeInsets.all(16),
        child: MarkdownBody(
          styleSheet: MarkdownStyleSheet(
            tableHead: const TextStyle(fontSize: 1.0),
            tableCellsPadding: const EdgeInsets.fromLTRB(16, 4, 16, 4),
          ),
          selectable: true,
          data: _slides == null ? '' : _slides[slideIndex+1],
        ),
      )
    );
    // return Markdown(
    //   // controller: controller,
    //   selectable: true,
    //   data: _slides == null ? '' : _slides[slideIndex+1],
    // );
  }


  @override
  void initState() {
    super.initState();
  }

  void fetchSlides() async {
    var loc = AppLocalizations.of(context);
    var appStore = P.Provider.of<AppStore>(context, listen: false);
    String languageCode = appStore.forceLanguage ?? loc.locale.languageCode;
    String mdfile = await rootBundle.loadString('assets/intro_$languageCode.md');
    setState(() {
      _slides = mdfile.split('##').map((e) => '##' + e).toList();
    });
  }


  @override
  Widget build(BuildContext context) {
    if (_slides == null) {
      fetchSlides();
    }
    return Container(
      padding: EdgeInsets.all(16),
      child: Column(
        children: [
          _buildBullets(context),
          SizedBox(height: 20),
          Expanded(child:  _buildSwitcher(context))
        ]
      )
    );
  }
}

