import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SegmentedControl<T> extends StatelessWidget {
  SegmentedControl({this.header, this.value, this.children, this.onValueChanged});
  final Widget header;
  final T value;
  final Map<T, Widget> children;
  final ValueChanged<T> onValueChanged;

  @override
  Widget build(BuildContext context) {
    List<Widget> elements = [];
    if (null != header)
      elements.add(header);
    elements.add(        
      SizedBox(
        width: double.infinity,
        child: CupertinoSegmentedControl<T>(
          children: children,
          groupValue: value,
          selectedColor: Colors.grey[600],
          unselectedColor: Colors.white,
          onValueChanged: onValueChanged,
          borderColor: Colors.grey,
        ),
      ),
    );

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: elements
    );
  }
}