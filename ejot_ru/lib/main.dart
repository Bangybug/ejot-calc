import 'dart:typed_data';

import 'package:ejot_ru/data/AppLocalizations.dart';
import 'package:ejot_ru/ui/pagenav/PageNav.dart';
import 'package:ejot_ru/ui/pages/CreateMaterialPage.dart';
import 'package:ejot_ru/ui/pages/MaterialSelectorPage.dart';
import 'package:ejot_ru/ui/pages/CitySelectPage.dart';
import 'package:ejot_ru/ui/pdf/PdfAssetPreview.dart';
import 'package:ejot_ru/ui/pdf/PdfPage.dart';
import 'package:ejot_ru/ui/theme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:mobx/mobx.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart' as P;

import 'lifecycle_manager.dart';
import 'model/AppStore.dart';
import 'ui/pages/BaseBlockEditorPage.dart';
import 'ui/pages/GalleryPage.dart';
import 'ui/pages/ZoomableImagePage.dart';

import 'package:platform/platform.dart';
import 'package:filelog/filelog.dart';

final AppStore _appStore = AppStore();

// FYI this testcase is magically affecting pdf creation bug 
// https://github.com/flutter/flutter/issues/64311
void testcase() {
  final byteData = new ByteData(8);
  byteData.setUint8(0, 1);
  byteData.setUint8(2, 2);
  byteData.setUint8(4, 3);
  byteData.setUint8(6, 4);
  log('byteData.lengthInBytes: ${byteData.lengthInBytes}');

  final unmodifiableByteDataView = UnmodifiableByteDataView(byteData);
  log('unmodifiableByteDataView.lengthInBytes: ${unmodifiableByteDataView.lengthInBytes}');

  final uint8View = Uint8List.view(unmodifiableByteDataView.buffer, 0, 4);
  log('uint8View.lengthInBytes: ${uint8View.lengthInBytes}');
}

void main() async { 
  WidgetsFlutterBinding.ensureInitialized();

  await _appStore.load();

  if (LocalPlatform().isWindows) {
    try {
      await initLog((await getApplicationDocumentsDirectory()).path);
    } catch (e) {
      print(e);
    }

    testcase();

    autorun((_){
      if (_appStore.lastUpdatedCount > 0) {
        try {
          _appStore.save();
        }
        catch (e) {
          log("SAVE Exception");
        }
      }
    });
  }

  AppLocalizations.forceLanguage = _appStore.forceLanguage;
  runApp(MobxApp());
}

class MobxApp extends StatelessWidget {
  final GlobalKey<PageNavState> _navKey = GlobalKey<PageNavState>();

  @override
  Widget build(BuildContext context) {
    return LifeCycleManager( 
      appStore: _appStore,
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        
        supportedLocales: [
          const Locale('en', ''),
          const Locale('ru', '')
        ],
      
        localizationsDelegates: [
          const AppLocalizationsDelegate(),
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate, // FYI without these, the text selection does NPE on ios
          DefaultCupertinoLocalizations.delegate
        ],

        routes: {
          '/materialSelectInsulator': (context) => MaterialSelectorPage(matType: MatType.INSULATION),
          '/materialSelectOther': (context) => MaterialSelectorPage(matType: MatType.OTHER),
          '/materialCreate': (context) => MaterialCreatePage(),
          '/blockEdit': (context) => BaseBlockEditorPageWrapper(),
          '/citySelect': (context) => CitySelectPage(),
          '/imageView': (context) => ZomableImagePage(),
          '/imageGallery': (context) => GalleryPage(),
          '/pdf': (context) => PdfPage(),
          '/pdfPreviewAsset': (context) => PdfAssetPreview()
        },

        onGenerateTitle: (context) => AppLocalizations.of(context).title,
  
        theme: createTheme(context),
        home:  P.Provider(
          create: (_) => _appStore,
          child: Builder(
            builder: (context) { // FYI without builder, the localization context wont be picked up
              return PageNav(key: _navKey, appStore: _appStore);
            }
          )
        )
      )
    );
  }

}


// FYI the sad part is that implementing widget as stateful does not help its state survive, in case the widget is disposed.
// Disposal of widgets in mobile dev is typically controlled by OS, in our case it is by Flutter foundation components.
// I was surprised that the Pageview component is disposing page states in IOS simulator, but not doing so on real Android device.
// This is an issue. To address it I may do this:
// 1) Keep the states in the top-parent object (lifting states). This requires delivering messages through some bus, i.e. streams, sinks, notifications or even callbacks. 
// 2) Persist (commit) and restore states in the code of each widget.
// 3) Mark some widgets as Offstage, i.e. never disposing. 

// I first tried redux. Good explanation is here https://github.com/brianegan/flutter_architecture_samples/tree/master/redux
// Then I went curious and put Mobx in, it was better due to its codegeneration and less boilerplate to be written manually.