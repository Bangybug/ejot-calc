import 'dart:collection';

import 'package:ejot_ru/model/AppStore.dart';
import 'dart:math' as math;

import 'package:ejot_ru/util/misc.dart';

enum SurfaceType {
  A, // boundary
  B, // ordinary
  // C,D,E not used for walls
}

class AnchorCalc {
  AnchorCalc(AppStore appStore) : this.s = appStore;

  AppStore s;

  List<Map<String, dynamic>> cache = [];

  double get _d {
    // d - размер здания (без учета его стилобатной части) в направлении, перпендикулярном расчетному направлению ветра (поперечный размер).
    // XXX как считать?
    //return math.min(s.buildingWidth, s.buildingLength);
    return s.buildingWidth;
  }


  double _ze(double z) {
    // Значение эквивалентной высоты ze
    double d = _d;
    double h = s.buildingHeight;
    if (h <= d) {
      return h;
    }
    if (d < h && h <= 2 * d) {
      if (z >= (h - d)) return h;
      if (0 < z && z <= (h - d)) return d;
    }
    // if (h > 2*_d) {
    if (z >= (h - d)) return h;
    if (d < z && z < (h - d)) return z;
    if (0 < z && z <= d) return d;
    return null;
  }

  double _kze(double z) {
    double ze = _ze(z);
    if (ze < 5) {
      return [0.75, 0.5, 0.4][s.terrainType.id];
    }
    if (ze < 10) {
      return [1.0, 0.65, 0.4][s.terrainType.id];
    }
    return s.terrainType.k10 * math.pow(ze / 10, 2 * s.terrainType.alpha);
  }

  double _sze(double z) {
    double ze = _ze(z);
    if (ze < 5) {
      return [0.85, 1.22, 1.75][s.terrainType.id];
    }
    if (ze < 10) {
      return [0.76, 1.06, 1.78][s.terrainType.id];
    }
    return s.terrainType.s10 * math.pow(ze / 10, -s.terrainType.alpha);
  }

  List<double> get _v {
    // Коэффициенты корреляции ветровой нагрузки ѵ +(-)
    if (s.buildingFacadeSquare < 5) {
      return [1, 1];
    } else if (s.buildingFacadeSquare < 10) {
      return [0.9, 0.85];
    } else if (s.buildingFacadeSquare < 20) {
      return [0.8, 0.75];
    } else {
      return [0.75, 0.65];
    }
  }

  // Аэродинамические коэффициенты c p,+  и c p,-
  double get _cpP {
    return 1.2;
  }

  double _cpM(SurfaceType surface) {
    return [-2.2, -1.2, -3.4, -2.4, -1.5][surface.index];
  }

  double get _fNorm {
    // Нормативное вытягивающее усилие F рч норм, кН, не менее, в зависимости от класса надежности СФТК по применению
    const dynamic table = [
      [0.35, 0.3, 0.2],
      [0.35, 0.3, 0.2],
      [0.25, 0.2, 0.15],
      [0.25, 0.2, 0.15],
      [0.25, 0.15, 0.15]
    ];
    return table[s.coreMaterial.anchorType][s.sftkClass.id];
  }

  double get _minAnchorsPerM2 {
    return s.sftkClass.id < 2 ? 5 : 0;
  }

  List<double> _wpm(SurfaceType surface, int heightZoneIndex) {
    // Пиковая ветровая нагрузка wp+/-
    double heightZ;
    switch (heightZoneIndex) {
      case 0:
        heightZ = s.buildingHeight > 16 ? 16 : s.buildingHeight;
        break;
      case 1:
        heightZ = 40;
        break;
      default:
        heightZ = s.buildingHeight;
        break;
    }
    double t = s.windZone.w0Pressure * _kze(heightZ) * (1 + _sze(heightZ));
    var cpm = _cpM(surface);
    var cpp = _cpP;
    var v = _v;
    return [roundDouble(t * cpp * v[0], 3), roundDouble(t * cpm * v[1], 3)];
  }

  int heightZoneCount() {
    if (s.buildingHeight > 40) 
      return 3;
    else if (s.buildingHeight > 16)
      return 2;
    else return 1;
  }

  LinkedHashSet<String> canCalculate() {
    LinkedHashSet<String> ret = LinkedHashSet<String>();
    if (s.buildingWidth == null ||
        s.buildingHeight == null ||
        s.buildingLength == null ||
        s.buildingFacadeSquare == null) {
      ret.add('fillBuildingSizeErr');
    }
    if (s.terrainType == null) {
      ret.add('terrainType');
    }
    if (s.coreMaterial == null) {
      ret.add('mtlWallCore');
    }
    if (s.sftkClass == null) {
      ret.add('sftkClass');
    }
    if (s.windZone == null) {
      ret.add('windZoneSelect');
    }
    if (s.facade == FacadeType.SFTK &&
        (breakForceEjot() == null || breakForceEjot() <= 0)) {
      ret.add('pBreakForceEjot');
    }
    if (s.facade == FacadeType.NFS &&
        (s.avgPerm2NFS == null || s.avgPerm2NFS <= 0)) {
      ret.add('pAvgPerMeterSq');
    }

    if (ret.isNotEmpty) {
      cache = [];
    }
    return ret;
  }

  bool hasComparison() {
    return s.facade == FacadeType.NFS ||
        (s.breakForceOther != null && s.breakForceOther > 0.0);
  }

  double breakForceEjot() {
    return s.breakForceEjot;
  }

  List<Map<String, dynamic>> calculate() {
    List<Map<String, dynamic>> ret = [];
    double fNorm = _fNorm;

    int heightZones = heightZoneCount();

    List<List<double>> wpmA = [];
    List<double> nm = [];
    for (int i=0; i<heightZones; ++i) {
      List<double> wpmAi = _wpm(SurfaceType.A, i);
      double nmP = math.max( (wpmAi[0] / breakForceEjot()).abs(), _minAnchorsPerM2);
      double nmM = math.max( (wpmAi[1] / breakForceEjot()).abs(), _minAnchorsPerM2);      
      nm.add( roundDouble(math.max(math.max(nmP, nmM), 5), 1) );
      wpmA.add(wpmAi);
    }

    ret.add({
      'category': s.coreMaterial.anchorType,
      'fNorm': fNorm,
      'breakForce': breakForceEjot(),
      'nm': nm,
      'wp': wpmA
    });


    List<List<double>> wpmB = [];
    nm = [];
    for (int i=0; i<heightZones; ++i) {
      List<double> wpmBi = _wpm(SurfaceType.B, i);
      double nmP = math.max( (wpmBi[0] / breakForceEjot()).abs(), _minAnchorsPerM2);
      double nmM = math.max( (wpmBi[1] / breakForceEjot()).abs(), _minAnchorsPerM2);      
      nm.add( roundDouble(math.max(math.max(nmP, nmM), 5), 1) );
      wpmB.add(wpmBi);
    }

    ret.add({
      'category': s.coreMaterial.anchorType,
      'fNorm': fNorm,
      'breakForce': breakForceEjot(),
      'nm': nm,
      'wp': wpmB
    });

    // calculate for non-ejot anchor
    if (hasComparison()) {
      List<double> nm = [];
      wpmA = [];
      for (int i=0; i<heightZones; ++i) {
        List<double> wpmAi = _wpm(SurfaceType.A, i);
        double nmP = math.max( (wpmAi[0] / s.breakForceOther).abs(), _minAnchorsPerM2);
        double nmM = math.max( (wpmAi[1] / s.breakForceOther).abs(), _minAnchorsPerM2);      
        nm.add( roundDouble(math.max(math.max(nmP, nmM), 5), 1) );
        wpmA.add(wpmAi);
      }

      ret.add({
        'category': s.coreMaterial.anchorType,
        'fNorm': fNorm,
        'breakForce': s.breakForceOther,
        'nm': nm,
        'wp': wpmA
      });

      nm = [];
      wpmB = [];
      for (int i=0; i<heightZones; ++i) {
        List<double> wpmBi = _wpm(SurfaceType.B, i);
        double nmP = math.max( (wpmBi[0] / s.breakForceOther).abs(), _minAnchorsPerM2);
        double nmM = math.max( (wpmBi[1] / s.breakForceOther).abs(), _minAnchorsPerM2);      
        nm.add( roundDouble(math.max(math.max(nmP, nmM), 5), 1) );
        wpmB.add(wpmBi);
      }

      ret.add({
        'category': s.coreMaterial.anchorType,
        'fNorm': fNorm,
        'breakForce': s.breakForceOther,
        'nm': nm,
        'wp': wpmB
      });
    }

    cache = ret;

    return ret;
  }
}
