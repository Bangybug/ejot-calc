import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:platform/platform.dart';

class LinkText extends StatelessWidget {

  final String title;
  final String hyperlink;

  const LinkText({Key key, this.title, this.hyperlink}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    final colorScheme = Theme.of(context).colorScheme;
    final textTheme = Theme.of(context).textTheme;
    final bodyTextStyle = textTheme.bodyText1.apply(color: colorScheme.onPrimary);

    return RichText(
      text: TextSpan(
        children: [
          TextSpan(
            style: bodyTextStyle.copyWith(
              color: colorScheme.primary,
            ),
            text: title,
            recognizer: TapGestureRecognizer()
              ..onTap = () async {
                if (LocalPlatform().isWindows || await canLaunch(hyperlink)) {
                  await launch(
                    hyperlink,
                    forceSafariVC: false,
                  );
                }
              },
          ),
        ],
      )
    );
  }
  
}

