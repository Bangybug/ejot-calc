import 'package:ejot_ru/ui/widgets/Stepfab.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MyHeader extends StatelessWidget {
  MyHeader({ Key key, this.pageIndex }) : super(key: key);

  final int pageIndex;

  @override
  Widget build(BuildContext context) {

    return Material( 
      color: Colors.grey[600],
      textStyle: TextStyle(color: Colors.white),
      type: MaterialType.canvas,
      child: Container(
        padding: EdgeInsets.all(8),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: localizeStep(pageIndex, context)
        )
      )
    );
  } 

}