import 'package:flutter/material.dart';

import 'Item.dart';

class ItemList extends StatelessWidget {
  ItemList({
    this.itemSelectedCallback,
    this.selectedItem, 
    this.items,
  });

  final ValueChanged<Item> itemSelectedCallback;
  final Item selectedItem;
  final List<Item> items;

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: items.map((item) {
        return ListTile(
          title: Text(item.title, style: TextStyle(fontWeight: selectedItem == item ? FontWeight.bold : FontWeight.normal)),
          onTap: () => itemSelectedCallback(item),
          selected: selectedItem == item,
        );
      }).toList(),
    );
  }
}