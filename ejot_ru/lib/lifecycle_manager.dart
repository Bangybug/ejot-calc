
import 'package:ejot_ru/model/AppStore.dart';
import 'package:flutter/widgets.dart';

class LifeCycleManager extends StatefulWidget {
  final Widget child;
  final AppStore appStore;
  LifeCycleManager({Key key, this.child,  this.appStore}) : super(key: key);

  _LifeCycleManagerState createState() => _LifeCycleManagerState();
}

class _LifeCycleManagerState extends State<LifeCycleManager>
    with WidgetsBindingObserver {
  @override
  void initState() {
    WidgetsBinding.instance.addObserver(this);
    super.initState();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (AppLifecycleState.inactive == state) {
      print("SAVING");
      this.widget.appStore.save();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: widget.child,
    );
  }
}
