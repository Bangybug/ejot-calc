import 'dart:io';

File logOutputFile;

Future<void> initLog(String path) async {
  if (path != null)
    logOutputFile = File(path + '/' + 'ejot_log.txt');
}

Future<void> clearLog() async {
}

void log(String message) {
  try {
    if (null != logOutputFile) {
      // ignore: deprecated_member_use
      logOutputFile.writeAsStringSync(message+"\n", mode: FileMode.APPEND);
    }
  }
  catch (e) {
  }
  print(message);
}