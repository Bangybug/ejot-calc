import 'package:ejot_ru/data/AppLocalizations.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class BottomBar extends StatelessWidget { 
  BottomBar({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var loc = AppLocalizations.of(context);
    TextStyle smallText = TextStyle(fontSize: 9.0, color: Theme.of(context).primaryColor);

    return Container(
      height: 50,
      padding: EdgeInsets.only(left: 8.0, top:8.0, right: 8.0),
      decoration: BoxDecoration(
        color: Theme.of(context).bottomAppBarColor
      ),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(loc.getString('ejotEast'), style: TextStyle(fontSize: 9.0, fontWeight: FontWeight.w800, color: Theme.of(context).primaryColor)),
          SizedBox(width: 20),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(loc.getString('addr1'), style: smallText),
              Text(loc.getString('addr2'), style: smallText),
              Text(loc.getString('addr3'), style: smallText)
            ]
          )
        ],
      )
    );
  }
}
