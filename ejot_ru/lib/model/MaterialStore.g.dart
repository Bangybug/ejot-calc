// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'MaterialStore.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$MaterialStore on _MaterialStore, Store {
  final _$matInsulatorsAtom = Atom(name: '_MaterialStore.matInsulators');

  @override
  List<MaterialGroup> get matInsulators {
    _$matInsulatorsAtom.context.enforceReadPolicy(_$matInsulatorsAtom);
    _$matInsulatorsAtom.reportObserved();
    return super.matInsulators;
  }

  @override
  set matInsulators(List<MaterialGroup> value) {
    _$matInsulatorsAtom.context.conditionallyRunInAction(() {
      super.matInsulators = value;
      _$matInsulatorsAtom.reportChanged();
    }, _$matInsulatorsAtom, name: '${_$matInsulatorsAtom.name}_set');
  }

  final _$matCoresAtom = Atom(name: '_MaterialStore.matCores');

  @override
  List<MaterialGroup> get matCores {
    _$matCoresAtom.context.enforceReadPolicy(_$matCoresAtom);
    _$matCoresAtom.reportObserved();
    return super.matCores;
  }

  @override
  set matCores(List<MaterialGroup> value) {
    _$matCoresAtom.context.conditionallyRunInAction(() {
      super.matCores = value;
      _$matCoresAtom.reportChanged();
    }, _$matCoresAtom, name: '${_$matCoresAtom.name}_set');
  }

  final _$fetchMaterialsInsulatorAsyncAction =
      AsyncAction('fetchMaterialsInsulator');

  @override
  Future<List<MaterialGroup>> fetchMaterialsInsulator(
      BuildContext context, String languageCode) {
    return _$fetchMaterialsInsulatorAsyncAction
        .run(() => super.fetchMaterialsInsulator(context, languageCode));
  }

  final _$fetchMaterialsCoresAsyncAction = AsyncAction('fetchMaterialsCores');

  @override
  Future<List<MaterialGroup>> fetchMaterialsCores(
      BuildContext context, String languageCode) {
    return _$fetchMaterialsCoresAsyncAction
        .run(() => super.fetchMaterialsCores(context, languageCode));
  }

  final _$createCustomMaterialAsyncAction = AsyncAction('createCustomMaterial');

  @override
  Future<Material> createCustomMaterial(MatType matType, String name,
      double lambdaA, double lambdaB, double rho) {
    return _$createCustomMaterialAsyncAction.run(
        () => super.createCustomMaterial(matType, name, lambdaA, lambdaB, rho));
  }

  @override
  String toString() {
    final string =
        'matInsulators: ${matInsulators.toString()},matCores: ${matCores.toString()}';
    return '{$string}';
  }
}
