import 'package:ejot_ru/data/AppLocalizations.dart';
import 'package:ejot_ru/model/AppStore.dart';
import 'package:flutter/widgets.dart';

typedef ErrorCheckFunction = bool Function();

class InputErrorHandling {

  static void scrollIfError(AppStore appStore, ScrollController scrollController, double jumpTo) {
    if (appStore.shouldScrollToError) {
      appStore.shouldScrollToError = false;
      if (appStore.hasErrors()) {
        WidgetsBinding.instance.addPostFrameCallback((Duration ts) {
          scrollController.animateTo(jumpTo, duration: Duration(milliseconds: 1000), curve: Curves.easeIn);
        });
      }
    }
  }

  static String checkError(AppStore appStore, AppLocalizations loc, String errCode, {ErrorCheckFunction check}) {
    errCode += 'Err';
    if (appStore.errCodesThermic.contains(errCode) || appStore.errCodesAnchor.contains(errCode)) {
      if (check == null || check()) 
        return loc.getString(errCode);
    }
    return null;
  }

  static bool greaterThanZero(dynamic value) {
    return null != value && value > 0;
  }

}