import 'package:ejot_ru/data/AppLocalizations.dart';
import 'package:ejot_ru/data/constants.dart';
import 'package:ejot_ru/model/AppStore.dart';
import 'package:ejot_ru/model/City.dart';
import 'package:ejot_ru/model/CityStore.dart';
import 'package:ejot_ru/ui/pages/BasePage.dart';
import 'package:ejot_ru/ui/pages/CitySelectPage.dart';
import 'package:ejot_ru/ui/widgets/Dropdown.dart';
import 'package:ejot_ru/ui/widgets/MyHeader.dart';
import 'package:ejot_ru/ui/widgets/MyTextField.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:provider/provider.dart' as P;

class Step1GeneralPage extends StatefulWidget {
  @override
  _Step1GeneralPageState createState() => _Step1GeneralPageState();
}

class _Step1GeneralPageState extends State<Step1GeneralPage> {

  final TextEditingController _descriptionController = TextEditingController();
  static List<DropdownMenuItem> _buildingTypeItems;
  final GlobalKey<DropdownState> _buildingTypeDropdownKey = GlobalKey();
  AppStore _appStore;


  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _appStore ??= P.Provider.of<AppStore>(context);
    _descriptionController.text = _appStore.description;
  }

  List<DropdownMenuItem> _getBuildingTypeItems(BuildContext context){ 
    var loc = AppLocalizations.of(context);
    if (null == _buildingTypeItems) {
      _buildingTypeItems = List<DropdownMenuItem>.generate(BUILDINGTYPES_TOTAL, (i) => 
        DropdownMenuItem( 
          value: 'bt'+i.toString(), 
          child: Text(loc.getString('bt'+i.toString()))
        )
      );
    }
    return _buildingTypeItems;
  }

 
  Widget _buildTypeInfo(BuildContext context) {
    var loc = AppLocalizations.of(context);
    return Padding(
      padding: EdgeInsets.only(left: 0, bottom: 16),
      child: Text(loc.getString('buildingTypeExplain'))
    );
  }

  
  Widget _buildCityInfo(BuildContext context) {
    var loc = AppLocalizations.of(context);
    return Padding(
      padding: EdgeInsets.only(left: 0, bottom: 16),
      child: Text(loc.getString('cityExplain'))
    );
  }


  Widget _buildHint(BuildContext context, String hintId) {
    var loc = AppLocalizations.of(context);
    // FYI wrapping this in material may interfere with the PageNav left column material elevation in tablet mode :(
    return Padding(
        padding: EdgeInsets.only(left: 16, bottom: 16, top: 0),
        child: Text(loc.getString(hintId))
      );
  }


  Widget _buildTemperatureSelect(BuildContext context) {
    var loc = AppLocalizations.of(context);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      mainAxisAlignment: MainAxisAlignment.start,

      children: <Widget>[

        Padding(
          padding: EdgeInsets.only(left: 16, bottom: 3, top: 16),
          child: Observer( 
            builder: (_) => Text(
              _appStore.temperature.toString() + ' °C',  
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
            )
          )
        ),

        Padding(
          padding: EdgeInsets.only(left: 16, bottom: 2),
          child: Text(loc.getString('innerTemperature'), style: TextStyle(color: Theme.of(context).primaryColorDark))
        ),
        
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 10),
          child: Observer( 
            builder: (_) => Slider.adaptive(
              value: _appStore.temperature.toDouble(),
              min: 5,
              max: 25,
              divisions: 25,
              onChanged: (value) {
                _appStore.temperature = value.round();
                _appStore.lastUpdatedCount++;
              }
            )
          )
        ),

      ],
    );
  }


  Widget _buildHumiditySelect(BuildContext context) {
    var loc = AppLocalizations.of(context);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      mainAxisAlignment: MainAxisAlignment.start,

      children: <Widget>[

        Padding(
          padding: EdgeInsets.only(left: 16, bottom: 3, top: 0),
          child: Observer( 
            builder: (_) => Text(
              _appStore.humidity.toString() + ' %',  
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
            )
          )
        ),

        Padding(
          padding: EdgeInsets.only(left: 16, bottom: 2),
          child: Text(loc.getString('innerHumidity'), style: TextStyle(color: Theme.of(context).primaryColorDark))
        ),
        
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 10),
          child: Observer( 
            builder: (_) => Slider.adaptive(
              value: _appStore.humidity.toDouble(),
              min: 0,
              max: 100,
              divisions: 20,
              onChanged: (value) {
                _appStore.humidity = value.round();
                _appStore.lastUpdatedCount++;
              }
            )
          )
        ),

      ],
    );
  }


  Widget _buildPage(BuildContext context) {
    var loc = AppLocalizations.of(context);
    
    if (_appStore.selectedCity == null) {
      P.Provider.of<CityStore>(context).fetchRegions(context, _appStore.forceLanguage ?? loc.locale.languageCode).then( (regions) {
        geolocate(regions).then((city) {
          if (city != null) {
            _appStore.selectedCity = city;
            _appStore.lastUpdatedCount++;
            if (_appStore.selectedCity.windZone != null)
              _appStore.windZone = _appStore.selectedCity.windZone;
          }
        }); 
      });
    }

    return BasePage(
      headerContent: MyHeader(pageIndex: 0),
      mainContent: buildScrollable(child:Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[

          ListTile(
            title: MyTextField(
              hintText: loc.getString('inputDescription'),
              controller: _descriptionController,
              onChanged: (text) { 
                _appStore.lastUpdatedCount++; 
                _appStore.description = text; 
              }
            )
          ),
          
          Divider(color: Colors.grey, height: 1),

          ListTile(
            title: Padding( 
              padding: EdgeInsets.symmetric(vertical:10), 
              child: Observer( 
                builder: (_) => Text(
                  _appStore.selectedCity == null ? loc.getString('selectCity') : _appStore.selectedCity.name,
                  style: Theme.of(context).textTheme.subtitle1  // to match dropdownbutton style for building types below
                ) 
              )
            ), 
            subtitle: _buildCityInfo(context),
            trailing: Icon(Icons.navigate_next, color: Colors.grey),
            onTap: () {
              Navigator.pushNamed(context, '/citySelect', arguments: CitySelectArguments(_appStore, P.Provider.of<CityStore>(context, listen: false)));
            },
          ),

          Divider(color: Colors.grey, height: 1),

          ListTile(
            title: Dropdown(
              key: _buildingTypeDropdownKey,
              dropdownName: loc.getString('buildingType'), 
              items: _getBuildingTypeItems(context), 
              value: _appStore.buildingType > -1 ? 'bt'+_appStore.buildingType.toString() : null,
              valueToLabel: valueToLabelLocalized, 
              likeButton: false,
              onChanged: (v) {
                _appStore.buildingType = int.tryParse( v.toString().substring(2) );
                ++_appStore.lastUpdatedCount;
              },
              icon: Icon(Icons.navigate_next, color: Colors.grey,), iconSize: 24,),
            onTap: () { _buildingTypeDropdownKey.currentState.openDropdown(); },
            subtitle: _buildTypeInfo(context),
          ),
          
          Divider(color: Colors.grey, height: 1),

          _buildTemperatureSelect(context),

          SizedBox(height: 2.0),

          _buildHumiditySelect(context),
          _buildHint(context, 'climateExplain'),

          SizedBox(height: 60.0)

        ],
      )),
    );
  }

  Widget buildScrollable( { Widget child }) {
    return SingleChildScrollView(
      child: ConstrainedBox(
        constraints: BoxConstraints(
        ),
        child: child
      ),
    );
    // return child;
  }

  @override
  Widget build(BuildContext context) {
    return P.Provider( 
      create: (_) => CityStore(),
      child: Builder(  // FYI builder provides child context for the accessors of CityStore
        builder: (context) => Scrollbar( child: _buildPage(context) )
      )
    );
  }

}