import 'package:ejot_ru/data/AppLocalizations.dart';
import 'package:ejot_ru/model/AppStore.dart';
import 'package:ejot_ru/model/MaterialStore.dart';
import 'package:ejot_ru/model/SftkClass.dart';
import 'package:ejot_ru/model/WindZone.dart';
import 'package:ejot_ru/model/TerrainType.dart';
import 'package:ejot_ru/ui/widgets/AutoSizeText.dart';
import 'package:ejot_ru/ui/widgets/Dropdown.dart';
import 'package:ejot_ru/ui/widgets/ImgSchemeSlider.dart';
import 'package:ejot_ru/ui/widgets/MyHeader.dart';
import 'package:ejot_ru/ui/widgets/SegmentedControl.dart';
import 'package:ejot_ru/util/InputErrorHandling.dart';
import 'package:ejot_ru/util/misc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:provider/provider.dart' as P;
import 'package:ejot_ru/ui/theme.dart';

import 'BasePage.dart';
import 'MaterialSelectorPage.dart';

class Step2FacadePage extends StatefulWidget {
  Step2FacadePage({Key key}) : super(key: key);

  final List<GlobalKey> _buttonKeys =
      List<GlobalKey>.generate(4, (int i) => GlobalKey());

  @override
  State<StatefulWidget> createState() {
    return Step2FacadePageState();
  }
}

class Step2FacadePageState extends State<Step2FacadePage> {
  AppStore _appStore;
  GlobalKey<ImgSchemeSliderState> slider = GlobalKey();

  final ctlBaseWidthInput = TextEditingController();
  final ctlInsulatorWidthInput = TextEditingController();
  final ctlBreakForceInput = TextEditingController();
  final ctlBaseAnchorsAvgInput = TextEditingController();

  final ctlBuildingWidthInput = TextEditingController();
  final ctlBuildingLengthInput = TextEditingController();
  final ctlBuildingHeightInput = TextEditingController();
  final ctlBuildingFacadeSquareInput = TextEditingController();
  final ctlInsulatorWidthExternalInput = TextEditingController();

  final _scrollController = ScrollController();

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _appStore ??= P.Provider.of<AppStore>(context);

    ctlBaseWidthInput.text = doubleToString(_appStore.baseWidth);
    ctlInsulatorWidthInput.text = doubleToString(_appStore.insulatorWidth);

    ctlBuildingWidthInput.text = doubleToString(_appStore.buildingWidth);
    ctlBuildingLengthInput.text = doubleToString(_appStore.buildingLength);
    ctlBuildingHeightInput.text = doubleToString(_appStore.buildingHeight);
    ctlBuildingFacadeSquareInput.text =
        doubleToString(_appStore.buildingFacadeSquare);
    ctlInsulatorWidthExternalInput.text =
        doubleToString(_appStore.insulatorFixedWidthNFS);
  }

  Widget _buildHint(BuildContext context) {
    var loc = AppLocalizations.of(context);
    return Observer(
        builder: (context) => 
            Column(
              children: <Widget>[
                Padding(
                    padding: EdgeInsets.only(left: 8, bottom: 16, top: 16),
                    child: Text(
                      loc.getString('desc_' + _appStore.facade.toString()),
                      textScaleFactor: 1.2,
                    )),
              ],
            ));
  }

  Widget _buildFacadeSelector(BuildContext context) {
    var loc = AppLocalizations.of(context);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Padding(
            padding: EdgeInsets.only(left: 0, right: 0, top: 16),
            child: Observer(
                builder: (context) => SegmentedControl<FacadeType>(
                      value: _appStore.facade,
                      children: {
                        FacadeType.SFTK:
                            Text(loc.getString(FacadeType.SFTK.toString())),
                        FacadeType.NFS:
                            Text(loc.getString(FacadeType.NFS.toString())),
                      },
                      onValueChanged: (facadeType) {
                        _appStore.facade = facadeType;
                        _appStore.lastUpdatedCount++;
                        slider.currentState.select(
                            facadeType == FacadeType.SFTK ? 0 : 1, true);
                      },
                    )))
      ],
    );
  }

  Widget _buildFacadeGraphics(BuildContext context) {
    var loc = AppLocalizations.of(context);

    return ImgSchemeSlider(
        key: slider,
        selectedIndex: _appStore.facade.index,
        entries: [
          SchemeEntry(
              imgPath: 'assets/icons/FacadeType.SFTK.svg',
              buttonTargetX: [0.7, 0.3, 0, 0.7]),
          SchemeEntry(
              imgPath: 'assets/icons/FacadeType.NFS.svg',
              buttonTargetX: [0.7, 0.53, 0.4, 0.8]),
        ],
        buttons: [
          SchemeButton(
              hint: loc.getString('mtlWallCore'),
              buttonKey: widget._buttonKeys[0],
              button: _buildCoreChangeButton(context)),
          SchemeButton(
              hint: loc.getString('mtlInsulation'),
              buttonKey: widget._buttonKeys[1],
              button: _buildInsulationChangeButton(context, 0)),
          SchemeButton(
              buttonKey: widget._buttonKeys[2],
              button: _buildInsulationChangeButton(context, 1)),
          SchemeButton(
              hint: loc.getString('mtlAnchor') + ' EJOT H5',
              buttonKey: widget._buttonKeys[3],
              button: Container(
                  key: widget._buttonKeys[3],
                  height: 60,
                  margin: EdgeInsets.only(top: 6),
                  child: Row(children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(
                          left: 2, right: 16, top: 4, bottom: 4),
                      child: Image.asset('assets/icons/anchor.png',
                          alignment: Alignment.topLeft),
                    ),
                  ]),
              )
            )
        ]);
  }

  Widget buildScrollable({Widget child}) {
    return SingleChildScrollView(
      controller: _scrollController,
      child: ConstrainedBox(constraints: BoxConstraints(), child: child),
    );
  }

  Widget _buildSizeInput(BuildContext context) {
    var loc = AppLocalizations.of(context);
    return Observer(builder: (context) {
      return createInputGroup(context, useDecoration: true, children: <Widget>[
        buildNumericInput(
          context, loc.getString('baseWidth'), ctlBaseWidthInput, 
          (text) {
            _appStore.baseWidth = stringToDouble(text);
            _appStore.lastUpdatedCount++;
          }, 
          readOnly: true, needBorder: true, indent: 4
        ),
        Observer(builder: (context) {
          return Dropdown(
              value: _appStore.shouldCalculateInsulatorWidth,
              onChanged: (b) {
                _appStore.shouldCalculateInsulatorWidth = b;
                _appStore.lastUpdatedCount++;
              },
              dropdownName: loc.getString('insulatorWidth'),
              items: [
                DropdownMenuItem(
                    child: Text(localizeInsulatorWidth(false, context)),
                    value: false),
                DropdownMenuItem(
                    child: Text(localizeInsulatorWidth(true, context)),
                    value: true)
              ],
              valueToLabel: localizeInsulatorWidth);
        }),
        Observer(builder: (context) {
          if (_appStore.shouldCalculateInsulatorWidth) {
            return Container();
          }
          return buildNumericInput(
            context,
            loc.getString('insulatorWidth'),
            ctlInsulatorWidthInput,
            (text) {
              _appStore.insulatorWidth = stringToDouble(text);
              _appStore.lastUpdatedCount++;
            },
            needBorder: true, indent: 4
            // isError: InputErrorHandling.checkError(_appStore, loc, 'insulatorWidth')
          );
        }),
        Observer(builder: (context) {
          if (_appStore.facade == FacadeType.NFS) {
            return buildNumericInput(
                context,
                loc.getString('buildingFacadeSquare'),
                ctlBuildingFacadeSquareInput, (text) {
              _appStore.buildingFacadeSquare = stringToDouble(text);
              _appStore.lastUpdatedCount++;
            }, needBorder: true, indent: 4);
          }
          return Container();
        }),
        Observer(builder: (context) {
          if (_appStore.facade == FacadeType.SFTK) {
            return Container();
          }
          return buildNumericInput(
            context,
            loc.getString('insulatorWidthExternal'),
            ctlInsulatorWidthExternalInput,
            (text) {
              _appStore.insulatorFixedWidthNFS = stringToDouble(text);
              _appStore.lastUpdatedCount++;
            }, needBorder: true, indent: 4
          );
        }),
      ]);
    });
  }

  Widget _buildGeometricInput(BuildContext context) {
    return Observer(builder: (context) {
      if (_appStore.facade == FacadeType.SFTK) {
        var loc = AppLocalizations.of(context);
        return createInputGroup(context, useDecoration: true,  children: <Widget>[
          buildNumericInput(
            context,
            loc.getString('buildingLength'),
            ctlBuildingLengthInput,
            (text) {
              _appStore.buildingLength = stringToDouble(text);
              _appStore.lastUpdatedCount++;
            }, needBorder: true, indent: 4
            // isError: InputErrorHandling.checkError(_appStore, loc, 'fillBuildingSize', check: () => InputErrorHandling.greaterThanZero(_appStore.buildingLength))
          ),
          buildNumericInput(
            context,
            loc.getString('buildingWidth'),
            ctlBuildingWidthInput,
            (text) {
              _appStore.buildingWidth = stringToDouble(text);
              _appStore.lastUpdatedCount++;
            }, needBorder: true, indent: 4
            // isError: InputErrorHandling.checkError(_appStore, loc, 'fillBuildingSize', check: () => InputErrorHandling.greaterThanZero(_appStore.buildingWidth))
          ),
          buildNumericInput(
            context,
            loc.getString('buildingHeight'),
            ctlBuildingHeightInput,
            (text) {
              _appStore.buildingHeight = stringToDouble(text);
              _appStore.lastUpdatedCount++;
            }, needBorder: true, indent: 4
            // isError: InputErrorHandling.checkError(_appStore, loc, 'fillBuildingSize', check: () => InputErrorHandling.greaterThanZero(_appStore.buildingHeight))
          ),
          buildNumericInput(
            context,
            loc.getString('buildingFacadeSquare'),
            ctlBuildingFacadeSquareInput,
            (text) {
              _appStore.buildingFacadeSquare = stringToDouble(text);
              _appStore.lastUpdatedCount++;
            }, needBorder: true, indent: 4
            // isError: InputErrorHandling.checkError(_appStore, loc, 'fillBuildingSize', check: () => InputErrorHandling.greaterThanZero(_appStore.buildingFacadeSquare))
          ),
        ]);
      } else {
        return Container();
      }
    });
  }

  Widget _buildDropdownInput(BuildContext context) {
    return Observer(builder: (context) {
      if (_appStore.facade == FacadeType.SFTK) {
        var loc = AppLocalizations.of(context);
        return createInputGroup(context, useDecoration: true, children: <Widget>[
          Observer(builder: (context) {
            return Dropdown(
                value: _appStore.sftkClass,
                onChanged: (sc) {
                  _appStore.sftkClass = sc;
                  _appStore.lastUpdatedCount++;
                },
                dropdownName: loc.getString('sftkClass'),
                items: sftkClasses
                    .map((sc) => DropdownMenuItem(
                        child: Text(localizeSftkClass(sc, context)), value: sc))
                    .toList(),
                valueToLabel: localizeSftkClass);
          }),
          Observer(builder: (context) {
            return Text(localizeSftkClassLabel(_appStore.sftkClass, context),
                style: hintTextStyle());
          }),
          Dropdown(
              value: _appStore.terrainType,
              onChanged: (tt) {
                _appStore.terrainType = tt;
                _appStore.lastUpdatedCount++;
              },
              dropdownName: loc.getString('terrainType'),
              items: terrainTypes
                  .map((tt) => DropdownMenuItem(
                      child: Text(localizeTerrainType(tt, context)), value: tt))
                  .toList(),
              valueToLabel: localizeTerrainType),
          Observer(builder: (context) {
            return Text(
                localizeTerrainTypeLabel(_appStore.terrainType, context),
                style: hintTextStyle());
          }),
          Dropdown(
              value: _appStore.windZone,
              onChanged: (wz) {
                _appStore.windZone = wz;
                _appStore.lastUpdatedCount++;
              },
              dropdownName: loc.getString('windZoneSelect'),
              items: windZones
                  .map((wz) => DropdownMenuItem(child: Text(wz.id), value: wz))
                  .toList(),
              valueToLabel: localizeWindZone),
          Observer(builder: (context) {
            return Text(
                localizeWindZoneHint(
                    _appStore.selectedCity, _appStore.windZone, context),
                style: hintTextStyle());
          }),
        ]);
      } else {
        return Container();
      }
    });
  }

  Widget _buildPage(BuildContext context) {
    return BasePage(
        headerContent: MyHeader(pageIndex: 1),
        mainContent: buildScrollable(
            child: Padding(
                padding: const EdgeInsets.only(
                    top: 0, left: 0, bottom: 16, right: 0),
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      _buildFacadeSelector(context),
                      _buildHint(context),
                      _buildFacadeGraphics(context),
                      SizedBox(height: 20),
                      _buildSizeInput(context),
                      _buildDropdownInput(context),
                      _buildGeometricInput(context),
                      SizedBox(height: 60)
                    ]))));
  }

  Widget _buildCoreChangeButton(BuildContext context) {
    var loc = AppLocalizations.of(context);
    var mtlStore = P.Provider.of<MaterialStore>(context);
    var appStore = P.Provider.of<AppStore>(context);
    return Observer(builder: (context) {
      return RaisedButton(
          padding: EdgeInsets.all(6),
          key: widget._buttonKeys[0],
          onPressed: () => _handleCoreChange(mtlStore, appStore),
          elevation: 4,
          child: AutoSizeText(
            appStore.coreMaterial == null
                ? loc.getString('select')
                : appStore.coreMaterial.name,
            presetFontSizes: [15.0, 13.0, 11.0],
            maxLines: 4,
          ));
    });
  }

  Widget _buildInsulationChangeButton(BuildContext context, int insIndex) {
    var loc = AppLocalizations.of(context);
    var mtlStore = P.Provider.of<MaterialStore>(context);
    var appStore = P.Provider.of<AppStore>(context);

    return Observer(builder: (context) {
      String text;
      if (insIndex == 0) {
        text = appStore.insulationMaterial1 == null
            ? loc.getString('select')
            : appStore.insulationMaterial1.name;
      } else {
        text = appStore.insulationMaterial2 == null
            ? loc.getString('select')
            : appStore.insulationMaterial2.name;
      }

      return RaisedButton(
          padding: EdgeInsets.all(6),
          key: widget._buttonKeys[1 + insIndex],
          onPressed: () =>
              _handleInsulationChange(mtlStore, appStore, insIndex),
          elevation: 4,
          child: AutoSizeText(
            text,
            presetFontSizes: [15.0, 13.0, 11.0],
            maxLines: 4,
          ));
    });
  }

  void _handleInsulationChange(
      MaterialStore mtlStore, AppStore appStore, int insIndex) async {
    FocusScope.of(context).unfocus();
    var loc = AppLocalizations.of(context);
    var ret = await Navigator.pushNamed(context, '/materialSelectInsulator',
        arguments: MaterialSelectArguments(mtlStore));
    if (false != ret) {
      if (insIndex == 0) {
        appStore.insulationMaterial1 = (ret as MaterialSelectResult).material;
      }
      if (insIndex == 1) {
        appStore.insulationMaterial2 = (ret as MaterialSelectResult).material;
      }
      _appStore.lastUpdatedCount++;
    } else {
      if (insIndex == 1 && appStore.insulationMaterial2 != null) {
        var choice = await showConfirmationDialog(context, loc.getString('clearMaterial'), loc.getString('areYouSure'), '', loc.getString('dontRemove'), loc.getString('confirmRemove')) as bool;
        if (choice) {
          appStore.insulationMaterial2 = null;
          appStore.insulatorFixedWidthNFS = 0;
          _appStore.lastUpdatedCount++;
        }
      }
    }
    slider.currentState.updateLines();
  }

  void _handleCoreChange(MaterialStore mtlStore, AppStore appStore) async {
    // FYI Unhandled Exception: type 'MaterialPageRoute<dynamic>' is not a subtype of type 'Route<MaterialSelectResult>'
    // Solution: don't specify type of ret, use var
    FocusScope.of(context).unfocus();
    var ret = await Navigator.pushNamed(context, '/materialSelectOther',
        arguments: MaterialSelectArguments(mtlStore));
    if (false != ret) {
      appStore.coreMaterial = (ret as MaterialSelectResult).material;
      _appStore.lastUpdatedCount++;
    }
    slider.currentState.updateLines();
  }

  @override
  Widget build(BuildContext context) {
    if (!_appStore.errCodesThermic.contains('mtlWallCore')) // dont scroll if core material error
      InputErrorHandling.scrollIfError(_appStore, _scrollController, 300);

    var loc = AppLocalizations.of(context);

    return P.Provider(
        create: (_) => MaterialStore(loc),
        child: Builder(builder: (context) {
          return Scrollbar(child: _buildPage(context));
        }));
  }
}
