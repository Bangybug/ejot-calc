import 'package:ejot_ru/data/AppLocalizations.dart';
import 'package:ejot_ru/data/constants.dart';
import 'package:ejot_ru/model/AppStore.dart';
import 'package:ejot_ru/ui/drawer/Drawer.dart';
import 'package:ejot_ru/ui/pages/GreetingsPage.dart';
import 'package:ejot_ru/ui/pages/Step1GeneralPage.dart';
import 'package:ejot_ru/ui/pages/Step2FacadePage.dart';
import 'package:ejot_ru/ui/pages/Step3WallConstructionPage.dart';
import 'package:ejot_ru/ui/pages/Step4Economic.dart';
import 'package:ejot_ru/ui/pages/Step5Results.dart';
import 'package:ejot_ru/ui/pdf/PdfPage.dart';
import 'package:ejot_ru/ui/pdf/builder.dart';
import 'package:ejot_ru/ui/widgets/Stepfab.dart';
import 'package:ejot_ru/util/TabletDetector.dart';
import 'package:ejot_ru/util/Tuple.dart';
import 'package:ejot_ru/util/misc.dart';
import 'package:flutter/material.dart';
import 'package:platform/platform.dart';
import 'package:pdf/pdf.dart' as Pdf;
import 'package:provider/provider.dart';

import 'Item.dart';
import 'ItemList.dart';

class PageNav extends StatefulWidget {

  static const STEPPER_WIDTH = 200.0;

  final AppStore appStore;

  const PageNav({Key key, this.appStore}) : super(key: key);

  @override
  PageNavState createState() => PageNavState();
}

class PageNavState extends State<PageNav> {
  Item _selectedItem;
  List<Widget> _pages;
  static List<Item> _steps;

  PageView _pageView;

  final PageController _pageController = PageController(
    initialPage: 0,
    keepPage: true,
  );

  PageView _getPageView(BuildContext context) {
    if (_pageView == null) {
      _pageView = PageView(
        controller: _pageController,
        onPageChanged: (index) {
          if (index == STEPS_TOTAL) {
            widget.appStore.recalculate();
            ++widget.appStore.lastCalculatedCount;
          }
          widget.appStore.pageIndex = index;
          setState(() {
            _selectedItem = index > 0 ? _steps[index-1] : null;
          });
          // FYI numeric keyboards do not have done button on ios, so they are always visible, I hide them with unfocus 
          FocusScope.of(context).unfocus();
        },
        children: _pages,
      );
    }
    return _pageView;
  }
 
  void changePageNoError(int index) {
    changePage(Tuple2(index,false));
  }

  void changePage(Tuple2<int,bool> page) {
    final diff = (_pageController.page - (page.item1+1)).abs();

    _selectedItem = _steps[page.item1];
    widget.appStore.shouldScrollToError = page.item2;
    if (diff == 1)
      _pageController.animateToPage(page.item1+1, duration: Duration(milliseconds: 500), curve: Curves.ease);  
    else 
      _pageController.jumpToPage(page.item1+1);
    setState(() {
      // update selectedindex
    });
  }

  Future<bool> _onWillPop() async {
    _pageController.previousPage(  // FYI without this, the view will be popped on Android and app will collapse
      duration: Duration(milliseconds: 200),
      curve: Curves.linear,
    );
    return false;
  }

  Widget _buildMobileLayout(BuildContext context) {
    return WillPopScope( 
      child: _getPageView(context), 
      onWillPop: _onWillPop
    );
  }

  Widget _buildNavList(BuildContext context) {
    return ItemList(
      items: _getSteps(context),
      itemSelectedCallback: (item) {
        setState(() {
          _selectedItem = item;
          changePage(Tuple2(_steps.indexOf(item), false));
        });
      },
      selectedItem: _selectedItem,
    );
  }

  void _handleChangeLanguage(BuildContext context, String newLanguage) {
    var appStore = Provider.of<AppStore>(context, listen: false);
    var loc = AppLocalizations.of(context);
    appStore.forceLanguage = newLanguage;
    ++appStore.lastUpdatedCount;
    showModalDialog(context, loc.getString('languageChange'), loc.getString(newLanguage), loc.getString('appRestart'));
  }

  Widget _buildTabletLayout(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Material(
          elevation: 4.0,
          child: SizedBox(width: PageNav.STEPPER_WIDTH, 
            child: Column(
              children: [
                Expanded( child: _buildNavList(context) ),
                Row(children: <Widget>[
                  GestureDetector(
                    onTap: () { _handleChangeLanguage(context, 'ru'); },
                    child: Image(
                      image: AssetImage('assets/icons/rus.png'),
                      height: 50,
                      width: 50,
                    )
                  ),
                            
                  GestureDetector(
                    onTap: () { _handleChangeLanguage(context, 'en'); },
                    child: Image(
                      image: AssetImage('assets/icons/us.png'),
                      height: 50,
                      width: 50,
                    )
                  ),
                ],)
              ]
            ) 
          ),
        ),

       
        Flexible(
          flex: 3,
          child: WillPopScope( 
            child: _getPageView(context), 
            onWillPop: _onWillPop
          )
        ),
      ],
    );
  }

  List<Item> _getSteps(BuildContext context) {
    if (null == _steps) {
      var loc =  AppLocalizations.of(context);
      _steps = List<Item>.generate(STEPS_TOTAL, (i) => Item(
        title: loc.getString('step'+(i+1).toString()),
      ));
    }
    return _steps;
  }

  void showPdf(BuildContext context) async {
    if (LocalPlatform().isWindows) {
      var loc =  AppLocalizations.of(context);
      await savePdfAsFile(context, (Pdf.PdfPageFormat pageFormat) => generatePdf(loc, widget.appStore, pageFormat), Pdf.PdfPageFormat.a4);
    } else {
      Navigator.pushNamed(context, '/pdf', arguments: PdfPageArguments(widget.appStore));
    }
  }

  @override
  Widget build(BuildContext context) {
    final isTablet = TabletDetector.isTablet(MediaQuery.of(context));

    _pages = <Widget>[
      GreetingsPage(handlePageChange: changePageNoError),
      Step1GeneralPage(),
      Step2FacadePage(),
      Step3WallConstructionPage(),
      Step4Economic(),
      Step5Results(handlePageChange: changePage),
      // TestPage()
    ];

    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        title: Text(AppLocalizations.of(context).title)
      ),
      drawer: isTablet ? null  : MyDrawer(steps: _getSteps(context), handlePageChange: changePage),

      // FYI numeric keyboards do not have done button on ios, so they are always visible, I hide them with unfocus 
      body: GestureDetector( 
        onTap: () => FocusScope.of(context).unfocus(),
        child: isTablet ? _buildTabletLayout(context) : _buildMobileLayout(context)
      ),

      floatingActionButton: Stepfab(handlePageChange: changePageNoError, showPdf: showPdf )
    );
  }
}