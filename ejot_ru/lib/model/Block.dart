import 'dart:collection';

import 'package:ejot_ru/util/Tuple.dart';
import 'package:mobx/mobx.dart';
part 'Block.g.dart';

enum BlockBinding {
  INSULATION_BINDING,
  BALCONY_BINDING,
  ANGLE_BINDING,
  WINDOW_BINDING,
  BASEMENT_BINDING,
  UNIVERSAL,
  BRACKET_AL_BINDING,
  BRACKET_STEEL_BINDING,
  FIRESTOP_BINDING
}

enum ElementType { linear, point, planar }

enum ParamTypeDesc { NUMERIC, DROPDOWN, TEXT }

class DropdownItem {
  final dynamic id;

  DropdownItem({this.id});
}

class Param {
  final String name;
  final ParamTypeDesc type;
  final List<DropdownItem> items;
  final String suffixText;
  final List<dynamic> limits;

  double numericValue;
  String textValue;
  int indexValue;
  dynamic enumValue;

  bool checkLimits(dynamic from) {
    bool ret = true;
    if (null != limits) {
      double _numericValue = numericValue;
      String _textValue = textValue;
      int _indexValue = indexValue;
      dynamic _enumValue = enumValue;

      this.value = from;

      if (limits.length == 1) {
        ret = value >= limits[0];
      } else if (limits.length == 2) {
        ret = value >= limits[0] && value <= limits[1];
      }

      numericValue = _numericValue;
      textValue = _textValue;
      indexValue = _indexValue;
      enumValue = _enumValue;
    }
    return ret;
  }

  set value(dynamic from) {
    if (from == null) {
      numericValue = null;
      textValue = null;
      indexValue = null;
      enumValue = null;
    } else if (from is String) {
      if (type == ParamTypeDesc.NUMERIC || type == ParamTypeDesc.DROPDOWN) {
        numericValue = double.tryParse(from);
        textValue = from;
        indexValue = int.tryParse(from);
      } else if (type == ParamTypeDesc.TEXT) {
        numericValue = indexValue = null;
        textValue = from;
      }
    } else if (from is int) {
      if (type == ParamTypeDesc.NUMERIC || type == ParamTypeDesc.DROPDOWN) {
        numericValue = from.toDouble();
        textValue = from.toString();
        indexValue = from;
      } else if (type == ParamTypeDesc.TEXT) {
        numericValue = from.toDouble();
        indexValue = from;
        textValue = from.toString();
      }
    } else if (from is double) {
      if (type == ParamTypeDesc.NUMERIC || type == ParamTypeDesc.DROPDOWN) {
        numericValue = from;
        textValue = from.toString();
        indexValue = from.toInt();
      } else if (type == ParamTypeDesc.TEXT) {
        numericValue = from;
        indexValue = from.toInt();
        textValue = from.toString();
      }
    } else if (type == ParamTypeDesc.DROPDOWN) {
      var item = items.singleWhere((di) => di.id == from);
      indexValue = items.indexOf(item);
      numericValue = indexValue.toDouble();
      textValue = from.toString();
    }
  }

  get value {
    if (type == ParamTypeDesc.NUMERIC) return numericValue;
    if (type == ParamTypeDesc.DROPDOWN && indexValue != null)
      return items[indexValue].id;
    if (type == ParamTypeDesc.TEXT) return textValue;
    return null;
  }

  Param(
      {this.type,
      this.name,
      this.items,
      this.suffixText,
      this.limits,
      dynamic initValue}) {
    if (null != initValue) this.value = initValue;
  }

  Map<String, dynamic> toJson() => {
        'name': name,
        'value': type == ParamTypeDesc.DROPDOWN ? indexValue : value
      };
}

class ParamWithAlternative extends Param {
  ParamWithAlternative(this._alternative,
      {this.hint, type, name, items, suffixText})
      : super(type: type, name: name, items: items, suffixText: suffixText);

  final Param _alternative;
  final String hint;
  Param _current;

  Param getCurrent() {
    return (_current == null) ? this : _current;
  }

  void toggleAlternative() {
    _current = getCurrent() == this ? _alternative : this;
  }

  bool getState() {
    return this != getCurrent();
  }

  @override
  get value {
    if (getCurrent() == this) {
      return super.value;
    } else {
      return getCurrent().value;
    }
  }

  @override
  set value(dynamic from) {
    if (getCurrent() == this) {
      super.value = from;
    } else {
      getCurrent().value = from;
    }
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> ret = super.toJson();
    ret['alternative'] = _alternative.value;
    ret['isAlternative'] = getCurrent() == this;
    return ret;
  }
}

typedef ValidationFunction = bool Function(BlockParams bp);

class BlockParams {
  final LinkedHashMap<String, Param> data; // FYI to preserve insertion order
  ValidationFunction customValidation;

  BlockParams(this.data);

  factory BlockParams.fromList(Iterable<Param> params) {
    // return BlockParams( { for (var v in params) v.paramName : v } );  -- FYI supported by dart 2.3+
    return BlockParams(
      LinkedHashMap.fromIterable(params, key: (v) => v.name, value: (v) => v),
    );
  }

  bool isValid() {
    if (null != customValidation) return customValidation(this);

    return null ==
        data.values.firstWhere(
            (v) => v.type != ParamTypeDesc.TEXT && v.value == null,
            orElse: () => null);
  }

  factory BlockParams.fromJson(List<dynamic> json, BlockBinding blockBinding) {
    BlockParams ret = blockParamsDef[blockBinding].item1(blockBinding);
    json.forEach((e) {
      var p = ret.data[e['name']];
      if (p == null) return;

      if (e.containsKey('alternative') && p is ParamWithAlternative) {
        p._alternative.value = e['alternative'];
        if (e['isAlternative'] != (p.getCurrent() == p)) {
          p.toggleAlternative();
        }
      }

      p.value = e['value'];
    });
    return ret;
  }

  List<dynamic> toJson(BlockBinding blockBinding) =>
      (data.entries.map((e) => e.value.toJson()).toList());
}

enum AngleType { convex, concave }

BlockParams _angleBindingParams(_) {
  return BlockParams.fromList([
    Param(type: ParamTypeDesc.DROPDOWN, name: 'pAngleType', items: [
      DropdownItem(id: AngleType.convex),
      DropdownItem(id: AngleType.concave),
    ]),
    Param(
        type: ParamTypeDesc.NUMERIC, name: 'pTotalAngleLength', suffixText: 'm')
  ]);
}

enum PerforatedInsulationType { none, p1, p3, p5, nte }

BlockParams _balconyBindingParams(_) {
  return BlockParams.fromList([
    Param(
        type: ParamTypeDesc.DROPDOWN,
        name: 'pInsulationTypePerforated',
        items: [
          DropdownItem(id: PerforatedInsulationType.none),
          DropdownItem(id: PerforatedInsulationType.p1),
          DropdownItem(id: PerforatedInsulationType.p3),
          DropdownItem(id: PerforatedInsulationType.p5),
          DropdownItem(id: PerforatedInsulationType.nte),
        ]),
    Param(
        type: ParamTypeDesc.NUMERIC,
        name: 'pPlateThickness',
        suffixText: 'mm',
        limits: [160, 210]),
    Param(
        type: ParamTypeDesc.NUMERIC, name: 'pTotalPlateLength', suffixText: 'm')
  ]);
}

enum InsulationTypeWindow {
  frameBehindInsulator,
  frameInset100,
  frameOffset100
}

enum BlockType { window, door, balcony }

BlockParams _windowBindingParams(_) {
  var bp = BlockParams.fromList([
    Param(type: ParamTypeDesc.DROPDOWN, name: 'pInsulationTypeWindow', items: [
      DropdownItem(id: InsulationTypeWindow.frameBehindInsulator),
      DropdownItem(id: InsulationTypeWindow.frameInset100),
      DropdownItem(id: InsulationTypeWindow.frameOffset100)
    ]),
    Param(
        type: ParamTypeDesc.NUMERIC,
        name: 'pInsulationOverlapThickness',
        suffixText: 'mm',
        limits: [0, 60]),
    // Param(
    //   type: ParamTypeDesc.DROPDOWN,
    //   name: 'pBlockType',
    //   items: [
    //     DropdownItem(id: BlockType.window),
    //     DropdownItem(id: BlockType.door),
    //     DropdownItem(id: BlockType.balcony)
    //   ]
    // ),
    Param(type: ParamTypeDesc.NUMERIC, name: 'pWindowHeight', suffixText: 'mm'),
    Param(type: ParamTypeDesc.NUMERIC, name: 'pWindowWidth', suffixText: 'mm'),
    Param(type: ParamTypeDesc.NUMERIC, name: 'pDoorHeight', suffixText: 'mm'),
    Param(type: ParamTypeDesc.NUMERIC, name: 'pDoorWidth', suffixText: 'mm'),
    Param(type: ParamTypeDesc.NUMERIC, name: 'pBlockCount', suffixText: 'pcs'),
  ]);
  bp.customValidation = validateWindowBlock;
  return bp;
}

bool validateWindowBlock(BlockParams bp) {
  if (bp.data['pInsulationTypeWindow'].value != null) {
    var pv = bp.data['pInsulationOverlapThickness'].value;
    if (pv != null && pv >= 0) {
      var bc = bp.data['pBlockCount'].value;
      if (bc != null && bc > 0) {
        var wh = bp.data['pWindowHeight'].value;
        var ww = bp.data['pWindowWidth'].value;
        var dh = bp.data['pDoorHeight'].value;
        var dw = bp.data['pDoorWidth'].value;

        double total = 0;
        if (wh != null && ww != null) total += ww * wh;
        if (dh != null && dw != null) total += dh * dw;
        return (total > 0);
      }
    }
  }
  return false;
}

BlockParams _basementBindingParams(_) {
  return BlockParams.fromList([
    Param(
        type: ParamTypeDesc.NUMERIC,
        name: 'pThermalResistanceOverlap',
        suffixText: 'm2C/W'),
    Param(
        type: ParamTypeDesc.NUMERIC,
        name: 'pTotalOverlapLength',
        suffixText: 'm')
  ]);
}

enum Dimension {
  linear,
  point,
  planar
}

enum GeometricUnit { totalLength, countPerMSq }

BlockParams _universalBindingParams(_) {
  return BlockParams.fromList([
    Param(type: ParamTypeDesc.TEXT, name: 'pCustomName'),
    Param(type: ParamTypeDesc.DROPDOWN, name: 'pElementDimensionType', items: [
      DropdownItem(id: Dimension.linear),
      DropdownItem(id: Dimension.point)
    ]),
    Param(type: ParamTypeDesc.NUMERIC, name: 'pUnitHeatLoss'),
    Param(type: ParamTypeDesc.DROPDOWN, name: 'pGeometricSpec', items: [
      DropdownItem(id: GeometricUnit.totalLength),
      DropdownItem(id: GeometricUnit.countPerMSq)
    ]),
    Param(type: ParamTypeDesc.NUMERIC, name: 'pValuePerGeometricSpec'),
    Param(type: ParamTypeDesc.TEXT, name: 'pDocReference'),
  ]);
}

enum BracketType { rect, halfRect }

enum BracketSize { s60, s100, s150, s200 }

BlockParams _bracketALBindingParams(_) {
  return BlockParams.fromList([
    Param(type: ParamTypeDesc.DROPDOWN, name: 'pBracketType', items: [
      DropdownItem(id: BracketType.rect),
      DropdownItem(id: BracketType.halfRect)
    ]),
    Param(
        type: ParamTypeDesc.DROPDOWN,
        name: 'pBracketPredefinedSize',
        items: [
          DropdownItem(id: BracketSize.s60),
          DropdownItem(id: BracketSize.s100),
          DropdownItem(id: BracketSize.s150),
          DropdownItem(id: BracketSize.s200)
        ],
        suffixText: 'mm'),
    Param(
        type: ParamTypeDesc.NUMERIC,
        name: 'pBracketAvgCountPerMeterSq',
        suffixText: '1/m2')
  ]);
}

enum BracketAvgPerMeter { one, one33, one67, thre33 }

BlockParams _bracketSteelBindingParams(_) {
  return BlockParams.fromList([
    Param(
        type: ParamTypeDesc.DROPDOWN,
        name: 'pBracketAvgPerMeter',
        items: [
          DropdownItem(id: BracketAvgPerMeter.one),
          DropdownItem(id: BracketAvgPerMeter.one33),
          DropdownItem(id: BracketAvgPerMeter.one67),
          DropdownItem(id: BracketAvgPerMeter.thre33)
        ],
        suffixText: 'pcs/m'),
    Param(
        type: ParamTypeDesc.NUMERIC,
        name: 'pBracketAvgThickness',
        suffixText: 'mm'),
    Param(
        type: ParamTypeDesc.NUMERIC,
        name: 'pRailAvgThickness',
        suffixText: 'mm'),
    Param(
        type: ParamTypeDesc.NUMERIC,
        name: 'pBracketAvgCountPerMeterSq',
        suffixText: 'pcs/m2')
  ]);
}

BlockParams _fireStopBindingParams(_) {
  return BlockParams.fromList([
    Param(type: ParamTypeDesc.NUMERIC, name: 'pFireStopLength', suffixText: 'm')
  ]);
}

typedef BlockParamsFactory = BlockParams Function(BlockBinding b);
typedef BlockImageChooser = List<Tuple2<String, String>> Function(
    Block b, BlockParams params);

Map<BlockBinding, List<Tuple2<String, String>>> _bindingImages = {
  BlockBinding.INSULATION_BINDING: [
    Tuple2('assets/icons/sch_1.png', 'mtlAnchor')
  ],
  BlockBinding.ANGLE_BINDING: [
    Tuple2('assets/icons/sch_angle_convex.png', 'AngleType.convex'),
    Tuple2('assets/icons/sch_angle_concave.png', 'AngleType.concave')
  ],
  BlockBinding.BALCONY_BINDING: [
    Tuple2('assets/icons/sch_balc_noperf.png', 'schUnperforatedHint'),
    Tuple2('assets/icons/sch_balc_perf.png', 'schPerforatedHint'),
    Tuple2('assets/icons/sch_balc_nte.png', 'PerforatedInsulationType.nte')
  ],
  BlockBinding.WINDOW_BINDING: [
    Tuple2('assets/icons/sch_window_adj.png',
        'InsulationTypeWindow.frameBehindInsulator'),
    Tuple2('assets/icons/sch_window_offset.png',
        'InsulationTypeWindow.frameOffset100'),
    Tuple2('assets/icons/sch_window_inset.png',
        'InsulationTypeWindow.frameInset100'),
  ],
  BlockBinding.BASEMENT_BINDING: [
    Tuple2('assets/icons/sch_basement.png', 'schWallsBasement')
  ],
  BlockBinding.UNIVERSAL: [],
  BlockBinding.BRACKET_AL_BINDING: [
    Tuple2('assets/icons/sch_bracket_install1.png', 'schBracketInstall'),
    Tuple2('assets/icons/sch_bracket_install2.png', 'schBracketInstall'),
    Tuple2('assets/icons/sch_bracket_1.png', 'schBracket1'),
    Tuple2('assets/icons/sch_bracket_2.png', 'schBracket2'),
    Tuple2('assets/icons/sch_bracket_3.png', 'schBracket3'),
    Tuple2('assets/icons/sch_bracket_4.png', 'schBracket4'),
    Tuple2('assets/icons/sch_bracket_5.png', 'schBracket5'),
    Tuple2('assets/icons/sch_bracket_6.png', 'schBracket6'),
    Tuple2('assets/icons/sch_bracket_7.png', 'schBracket7'),
  ],
  BlockBinding.BRACKET_STEEL_BINDING: [
    Tuple2('assets/icons/sch_steelbracket_rail.png', 'schSteelBracketRail'),
    Tuple2('assets/icons/sch_steelbracket_rail1.png', 'schSteelBracketRail1'),
  ],
  BlockBinding.FIRESTOP_BINDING: [
    Tuple2('assets/icons/sch_firestopper.png', 'schFirestopper'),
  ]
};

List<Tuple2<String, String>> _bindingImg(Block b, BlockParams params) {
  if (_bindingImages.containsKey(b.blockBinding))
    return _bindingImages[b.blockBinding];
  return [];
}

Map<BlockBinding, Tuple2<BlockParamsFactory, BlockImageChooser>>
    blockParamsDef = {
  BlockBinding.INSULATION_BINDING: Tuple2(null, _bindingImg),
  BlockBinding.ANGLE_BINDING: Tuple2(_angleBindingParams, _bindingImg),
  BlockBinding.BALCONY_BINDING: Tuple2(_balconyBindingParams, _bindingImg),
  BlockBinding.WINDOW_BINDING: Tuple2(_windowBindingParams, _bindingImg),
  BlockBinding.BASEMENT_BINDING: Tuple2(_basementBindingParams, _bindingImg),
  BlockBinding.UNIVERSAL: Tuple2(_universalBindingParams, _bindingImg),
  BlockBinding.BRACKET_AL_BINDING: Tuple2(_bracketALBindingParams, _bindingImg),
  BlockBinding.BRACKET_STEEL_BINDING:
      Tuple2(_bracketSteelBindingParams, _bindingImg),
  BlockBinding.FIRESTOP_BINDING: Tuple2(_fireStopBindingParams, _bindingImg)
};

class Block extends _BlockStore with _$Block {
  Block(BlockBinding blockBinding) : super(blockBinding);

  void fromJson(Map<String, dynamic> json) {
    enabled = json["enabled"];
    instances.clear();
    instances.addAll((json["instances"] as List<dynamic>)
        .map((e) => BlockParams.fromJson(e, blockBinding))
        .toList()
        .cast<BlockParams>());
  }

  Map<String, dynamic> toJson() => {
        'enabled': enabled,
        'instances':
            instances.map((element) => element.toJson(blockBinding)).toList()
      };
}

abstract class _BlockStore with Store {
  final BlockBinding blockBinding;
  final ObservableList<BlockParams> instances;
  final String blockName;

  @observable
  bool enabled;

  _BlockStore(this.blockBinding)
      : instances = blockParamsDef[blockBinding].item1 != null
            ? ObservableList<BlockParams>.of(
                [blockParamsDef[blockBinding].item1(blockBinding)])
            : null,
        blockName = blockBinding.toString(),
        enabled = blockParamsDef[blockBinding].item1 == null ? true : false;

  bool isValid() {
    return instances == null ||
        instances.length > 0 &&
            null ==
                instances.firstWhere((bp) => !bp.isValid(), orElse: () => null);
  }

  int countValidInstances() {
    int ret = 0;
    if (null != instances) {
      instances.forEach((inst) {
        if (inst.isValid()) ret++;
      });
    } else {
      ret = 1;
    }
    return ret;
  }
}
