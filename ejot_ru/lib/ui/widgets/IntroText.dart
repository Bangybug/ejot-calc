import 'dart:async';

import 'package:ejot_ru/data/AppLocalizations.dart';
import 'package:ejot_ru/ui/widgets/AutoSizeText.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

const int TOTAL_BLOCKS = 3;

class IntroText extends StatefulWidget {
  IntroText(this.handlePageChange) : super();

  final ValueChanged<int> handlePageChange;

  @override
  _IntroTextState createState() => _IntroTextState();
}

class MyBullet extends StatelessWidget {
  MyBullet(this.color, this.onTap): super();
  final Color color;
  final GestureTapCallback onTap;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        height: 16.0,
        width: 16.0,
        margin: EdgeInsets.only(right: 20),
        decoration: BoxDecoration(
          color: color,
          shape: BoxShape.circle,
        ),
      )
    );
  }
}

class _IntroTextState extends State<IntroText> {
  //final AutoSizeGroup _sizeGroup = AutoSizeGroup();
  int _index = 0;
  bool _timerRunning = true;
  Timer _timer;

  Widget _buildText(BuildContext context, int index) {
    var loc = AppLocalizations.of(context);
    if (0 == index) {
      return AutoSizeText(loc.getString('introMsg1'), presetFontSizes: [16.0, 14.0, 10.0]);
    } else if (1 == index) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          AutoSizeText(loc.getString('introMsg2') + '\n' + loc.getString('introMsg3') + '\n' + loc.getString('introMsg4') + '\n' + loc.getString('introMsg5'), 
            presetFontSizes: [16.0, 14.0, 10.0],),
        ]);
    } else if (2 == index) {
      return AutoSizeText(loc.getString('introMsg6'), presetFontSizes: [16.0, 14.0, 10.0] );
    }
    
    // return RichText(
    //   text: TextSpan(
    //     text: 'Hello ',
    //     style: DefaultTextStyle.of(context).style,
    //     children: <TextSpan>[
    //       TextSpan(text: 'bold', style: TextStyle(fontWeight: FontWeight.bold)),
    //       TextSpan(text: ' world!'),
    //     ],
    //   ),
    // );
    return null;
  }

  final timeout = const Duration(seconds: 5);

  Timer startTimeout() {
    return new Timer.periodic(timeout, handleTimeout);
  }

  void handleTimeout(Timer t) { 
    if (_timerRunning) {
      setState(() {
        if (++_index == TOTAL_BLOCKS) _index = 0;
      });
    }
  }

  Widget _buildSwitcher(BuildContext context) {
    return AnimatedSwitcher(
      duration: const Duration(milliseconds: 500),
      transitionBuilder: (Widget child, Animation<double> animation) {
        return ScaleTransition(child: child, scale: animation);
      },
      child: _buildText(context, _index)
    );
  }

  void initState() {
    super.initState();
    _timer = startTimeout();
  }

  void dispose() {
    super.dispose();
    _timer.cancel();
  }

  Widget _buildBullets(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: List.generate(TOTAL_BLOCKS, (int i) => MyBullet(i == _index ? Theme.of(context).secondaryHeaderColor : Colors.grey, () {
        setState(() {
          _index = i;
          _timerRunning = false;
        });
      }))
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(16),
      child: Column(
        children: [
          _buildBullets(context),
          Expanded(child: _buildSwitcher(context))
        ]
      )
    );
  }
}

