import 'dart:typed_data';

import 'package:ejot_ru/data/AppLocalizations.dart';
import 'package:ejot_ru/model/AppStore.dart';
import 'package:ejot_ru/model/Block.dart';
import 'package:ejot_ru/ui/widgets/cells/cellwidgets.dart';
import 'package:filelog/filelog.dart';
import 'package:ejot_ru/util/misc.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart' as pw;
import 'package:pdf/widgets.dart';

import 'CustomTable.dart';

Future<Uint8List> generatePdf(AppLocalizations loc, AppStore s, PdfPageFormat pageFormat) async {
  

  final pdfDoc = Doc(
    loc,
    s,
    baseColor: PdfColors.grey600,
    accentColor: PdfColors.grey600,
  );

  return await pdfDoc.buildPdf(pageFormat);
}

class Doc {
  Doc(this.loc, this.s, {
    this.baseColor,
    this.accentColor,
  });

  final PdfColor baseColor;
  final PdfColor accentColor;
  final AppLocalizations loc;
  final AppStore s;
  Font defaultFont;

  static const _darkColor = PdfColors.blueGrey800;
  static const _lightColor = PdfColors.white;

  PdfColor get _baseTextColor =>
      baseColor.luminance < 0.5 ? _lightColor : _darkColor;

  PdfColor get _accentTextColor =>
      baseColor.luminance < 0.5 ? _lightColor : _darkColor;


  PdfImage _logo;
  PdfImage _sample;

  Future<Uint8List> buildPdf(PdfPageFormat pageFormat) async {   
    // Create a PDF document.
    final doc = pw.Document(compress: false);

    final font1 = await rootBundle.load('assets/Roboto-Light.ttf');
    final font2 = await rootBundle.load('assets/Roboto-Black.ttf');
    final font3 = await rootBundle.load('assets/Roboto-LightItalic.ttf');

    _logo = PdfImage.file(
      doc.document,
      bytes: (await rootBundle.load('assets/logo.png')).buffer.asUint8List(),
    );

    _sample = PdfImage.file(
      doc.document,
      bytes: (await rootBundle.load('assets/icons/installsample.png')).buffer.asUint8List(),
    );

    final tFont1 = font1 != null ? pw.Font.ttf(font1) : null;
    final tFont2 = font2 != null ? pw.Font.ttf(font2) : null;
    final tFont3 = font3 != null ? pw.Font.ttf(font3) : null;

    // final tFont1 = Font.courier();
    // final tFont2 = Font.courierBold();
    // final tFont3 = Font.courierOblique();

    defaultFont = tFont1;

    // Add page to the PDF
    doc.addPage(
      pw.MultiPage(
        pageTheme: _buildTheme(
          pageFormat,
          tFont1, tFont2, tFont3
        ),
        header: _buildHeader,
        footer: _buildFooter,
        build: (context) => [
          s.facade == FacadeType.SFTK ? _header1(context, loc.getString('pdfAnchorCalcTitle')) : pw.Container(),
          s.facade == FacadeType.SFTK ? _anchorReport(context) : pw.Container(),
          pw.SizedBox(height: s.facade == FacadeType.SFTK ? 20 : 0),
          _header1(context, loc.getString('thermicResults')),
          _thermicReportTable(context),
          pw.SizedBox(height: 20),
          _thermicReport(context),
          pw.SizedBox(height: 20),
          s.ecoCalc.canCalculate() ? _header1(context, loc.getString('economicResults')) : pw.Container(),
          s.ecoCalc.canCalculate() ? _ecoReport(context) : pw.Container(),
          pw.SizedBox(height: 20),
          pw.Image(_sample),
        ],
      ),
    );

    Uint8List ret;
    try {
      ret = doc.save();
    }
    catch (e, stacktrace) {
      log(e.toString());
      if (null != stacktrace) 
        log(stacktrace.toString());
      else 
        log("stacktrace is null");
      return ret;
    }
    finally {
      defaultFont = null;
    }

    // Return the PDF file content
    return Future.value( ret );
  }

  pw.Widget _buildHeader(pw.Context context) {
    if (context.pageNumber > 1) {
      return pw.Row(
        children: [ pw.SizedBox(height: 20) ]
      );
    }

    return pw.Column(
      children: [
        pw.Row(
          crossAxisAlignment: pw.CrossAxisAlignment.start,
          children: [
            pw.Expanded(
              child: pw.Column(
                children: [
                  pw.Container(
                    height: 50,
                    padding: const pw.EdgeInsets.only(left: 20),
                    alignment: pw.Alignment.centerLeft,
                    child: pw.Text(
                      loc.getString('pdfTitle'),
                      style: pw.TextStyle(
                        color: baseColor,
                        fontWeight: pw.FontWeight.bold,
                        fontSize: 40,
                      ),
                    ),
                  ),
                  pw.Container(
                    decoration: pw.BoxDecoration(
                      borderRadius: 2,
                      color: accentColor,
                    ),
                    padding: const pw.EdgeInsets.only(
                        left: 40, top: 10, bottom: 10, right: 20),
                    alignment: pw.Alignment.centerLeft,
                    height: 65,
                    child: pw.DefaultTextStyle(
                      style: pw.TextStyle(
                        color: _accentTextColor,
                        fontSize: 12,
                      ),
                      child: pw.GridView(
                        crossAxisCount: 1,
                        children: [
                          pw.Text(s.description ?? '', maxLines: 1),
                          pw.Text(s.selectedCity.name),
                          pw.Text(loc.getString('pdfDate') + ': ' + _formatDate(s.forceLanguage ?? loc.locale.languageCode, DateTime.now())),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            pw.Expanded(
              child: pw.Column(
                mainAxisSize: pw.MainAxisSize.min,
                children: [
                  pw.Container(
                    alignment: pw.Alignment.topRight,
                    padding: const pw.EdgeInsets.only(bottom: 8, left: 30),
                    height: 72,
                    child: _logo != null ? pw.Image(_logo) : pw.PdfLogo(),
                  ),
                  // pw.Container(
                  //   color: baseColor,
                  //   padding: pw.EdgeInsets.only(top: 3),
                  // ),
                ],
              ),
            ),
          ],
        ),
        pw.SizedBox(height: (context.pageNumber > 1 ? 20 : 0) )
      ],
    );
  }

  pw.Widget _buildFooter(pw.Context context) {
    return pw.Row(
      mainAxisAlignment: pw.MainAxisAlignment.spaceBetween,
      crossAxisAlignment: pw.CrossAxisAlignment.end,
      children: [
        pw.Container(
          height: 20,
          width: 100,
          child: pw.BarcodeWidget(
            textStyle: pw.TextStyle(fontBoldItalic: defaultFont, font: defaultFont, fontNormal: defaultFont, fontItalic: defaultFont),
            barcode: pw.Barcode.pdf417(),
            data: 'http://www.ejot.ru',
          ),
        ),
        pw.Text(
          loc.getString('page') + ' ${context.pageNumber}/${context.pagesCount}',
          style: const pw.TextStyle(
            fontSize: 12,
            color: PdfColors.grey,
          ),
        ),
      ],
    );
  }

  pw.PageTheme _buildTheme(
      PdfPageFormat pageFormat, pw.Font base, pw.Font bold, pw.Font italic) {
    return pw.PageTheme(
      pageFormat: pageFormat,
      theme: pw.ThemeData.withFont(
        base: base,
        bold: bold,
        italic: italic,
        boldItalic: bold
      ),
      buildBackground: (context) => pw.FullPage(
        ignoreMargins: true,
        child: pw.Stack(
          children: [
            pw.Positioned(
              bottom: 0,
              left: 0,
              child: pw.Container(
                height: 20,
                width: pageFormat.width / 2,
                decoration: pw.BoxDecoration(
                  gradient: pw.LinearGradient(
                    colors: [baseColor, PdfColors.white],
                  ),
                ),
              ),
            ),
            pw.Positioned(
              bottom: 20,
              left: 0,
              child: pw.Container(
                height: 20,
                width: pageFormat.width / 4,
                decoration: pw.BoxDecoration(
                  gradient: pw.LinearGradient(
                    colors: [accentColor, PdfColors.white],
                  ),
                ),
              ),
            ),
            context.pageNumber == 1 ?pw.Positioned(
              top: pageFormat.marginTop + 72,
              left: 0,
              right: 0,
              child: pw.Container(
                height: 3,
                color: baseColor,
              ),
            ) : pw.Container()
          ],
        ),
      ),
    );
  }

  pw.Widget _header1(pw.Context context, String text) {
    return pw.Row(
      crossAxisAlignment: pw.CrossAxisAlignment.start,
      children: [
        pw.Expanded(
          child: pw.Container(
            margin: const pw.EdgeInsets.symmetric(horizontal: 20, vertical: 10),
            height: 30,
            child: pw.FittedBox(
              child: pw.Text(
                text,
                style: pw.TextStyle(
                  color: baseColor,
                  fontStyle: pw.FontStyle.italic,
                ),
              ),
            ),
          ),
        ),
        
      ],
    );
  }


  List<String> _buildHeightZone(int index) {
    if (index == 0)
      return [loc.getString('heightBuilding') + ' ' + loc.getString('upto') + ' 16 '+ loc.getString('m'), '', ''];
    else if (index == 1)
      return [loc.getString('heightBuilding') + ' ' + loc.getString('from') + ' 16 '+ loc.getString('upto') + ' 40 ' + loc.getString('m'), '', ''];
    else if (index == 2)
      return [loc.getString('heightBuilding') + ' ' + loc.getString('above') + ' 40 ' + loc.getString('m'), '', ''];
    return null;
  }


  pw.Widget _anchorReportWithComparison(pw.Context context) {
    final calc = s.anchorCalc.cache;
    final calcOrdinaryEjot = calc[1];
    final calcBoundaryEjot = calc[0];
    final calcOrdinaryOther = calc[3];
    final calcBoundaryOther = calc[2];
    final abcd = ['A','B','C','D','E'];

    pw.TextStyle errStyle = pw.TextStyle(color: PdfColors.red, fontWeight: pw.FontWeight.bold);

    List<List<dynamic>> data = [
      [ loc.getString('anchorCategory'), abcd[calcOrdinaryEjot['category']], abcd[calcBoundaryEjot['category']] ],
      [ loc.getString('breakForceNorm') + ', ' + loc.getString('kN'), doubleToString(calcOrdinaryEjot['fNorm']), doubleToString(calcBoundaryEjot['fNorm'])  ],
      [ loc.getString('pBreakForce') + ' - '+loc.getString('ejotHD')+', ' + loc.getString('kN'), 
        calcOrdinaryEjot['breakForce'] < calcOrdinaryEjot['fNorm'] ? pw.Text(doubleToString(calcOrdinaryEjot['breakForce']), style: errStyle) : doubleToString(calcOrdinaryEjot['breakForce']), 
        calcBoundaryEjot['breakForce'] < calcBoundaryEjot['fNorm'] ? pw.Text(doubleToString(calcBoundaryEjot['breakForce']), style: errStyle) : doubleToString(calcBoundaryEjot['breakForce']), 
      ],
      [ loc.getString('pBreakForce') + ' - '+loc.getString('otherAnchor')+', ' + loc.getString('kN'), 
        calcOrdinaryOther['breakForce'] < calcOrdinaryOther['fNorm'] ? pw.Text(doubleToString(calcOrdinaryOther['breakForce']), style: errStyle) : doubleToString(calcOrdinaryOther['breakForce']), 
        calcBoundaryOther['breakForce'] < calcBoundaryOther['fNorm'] ? pw.Text(doubleToString(calcBoundaryOther['breakForce']), style: errStyle) : doubleToString(calcBoundaryOther['breakForce']), 
      ],
    ];

    int heightZoneCount = s.anchorCalc.heightZoneCount();
    for (int i=0; i<heightZoneCount; ++i) {
      if (heightZoneCount > 1)
        data.add(_buildHeightZone(i));
      data.addAll([
        [ loc.getString('numberAnchorsPerM2') + ' - '+loc.getString('ejotHD')+', ' + loc.getString('pcs/m2'), doubleToString(calcOrdinaryEjot['nm'][i]), doubleToString(calcBoundaryEjot['nm'][i]) ],
        [ loc.getString('numberAnchorsPerM2') + ' - '+loc.getString('otherAnchor')+', ' + loc.getString('pcs/m2'), doubleToString(calcOrdinaryOther['nm'][i]), doubleToString(calcBoundaryOther['nm'][i]) ],
        [ loc.getString('peakWindLoad') + ', ' + loc.getString('pressureUnits'), doubleToString(calcOrdinaryEjot['wp'][i][0]) + ' / ' + doubleToString(calcOrdinaryEjot['wp'][i][1]), doubleToString(calcBoundaryEjot['wp'][i][0]) + ' / ' + doubleToString(calcBoundaryEjot['wp'][i][1]) ]
      ]);
    }

    return CustomTable.fromTextArray(
      context: context, 
      
      headerDecoration: pw.BoxDecoration(
        borderRadius: 2,
        color: baseColor,
      ),
      headerHeight: 25,
      headerStyle: pw.TextStyle(
        color: _baseTextColor,
        fontSize: 10,
        fontWeight: pw.FontWeight.bold,
      ),
      headers: [
        loc.getString('pdfCharacteristics'),
        loc.getString('sftkOrdinaryZone'),
        loc.getString('sftkBoundaryZone'),
      ],

      data: data
    );
  }

  pw.Widget _anchorReportNoComparison(pw.Context context) {

    final calc = s.anchorCalc.cache;
    final calcOrdinaryEjot = calc[0];
    final calcBoundaryEjot = calc[1];
    final abcd = ['A','B','C','D','E'];

    pw.TextStyle errStyle = pw.TextStyle(background: pw.BoxDecoration(color: PdfColors.red));

    List<List<dynamic>> data = [
      [ loc.getString('anchorCategory'), abcd[calcOrdinaryEjot['category']], abcd[calcBoundaryEjot['category']] ],
      [ loc.getString('breakForceNorm') + ', ' + loc.getString('kN'), doubleToString(calcOrdinaryEjot['fNorm']), doubleToString(calcBoundaryEjot['fNorm'])  ],
      [ loc.getString('pBreakForce') + ', ' + loc.getString('kN'), 
        calcOrdinaryEjot['breakForce'] < calcOrdinaryEjot['fNorm'] ? pw.Text(doubleToString(calcOrdinaryEjot['breakForce']), style: errStyle) : doubleToString(calcOrdinaryEjot['breakForce']), 
        calcBoundaryEjot['breakForce'] < calcBoundaryEjot['fNorm'] ? pw.Text(doubleToString(calcBoundaryEjot['breakForce']), style: errStyle) : doubleToString(calcBoundaryEjot['breakForce']), 
      ],
    ];

    int heightZoneCount = s.anchorCalc.heightZoneCount();
    for (int i=0; i<heightZoneCount; ++i) {
      if (heightZoneCount > 1)
        data.add(_buildHeightZone(i));
      data.addAll([
        [ loc.getString('numberAnchorsPerM2') + ' - '+loc.getString('ejotHD')+', ' + loc.getString('pcs/m2'), doubleToString(calcOrdinaryEjot['nm'][i]), doubleToString(calcBoundaryEjot['nm'][i]) ],
        [ loc.getString('peakWindLoad') + ', ' + loc.getString('pressureUnits'), doubleToString(calcOrdinaryEjot['wp'][i][0]) + ' / ' + doubleToString(calcOrdinaryEjot['wp'][i][1]), doubleToString(calcBoundaryEjot['wp'][i][0]) + ' / ' + doubleToString(calcBoundaryEjot['wp'][i][1]) ]
      ]);
    }

    return CustomTable.fromTextArray(
      context: context, 
      
      headerDecoration: pw.BoxDecoration(
        borderRadius: 2,
        color: baseColor,
      ),
      headerHeight: 25,
      headerStyle: pw.TextStyle(
        color: _baseTextColor,
        fontSize: 10,
        fontWeight: pw.FontWeight.bold,
      ),
      headers: [
        loc.getString('pdfCharacteristics'),
        loc.getString('sftkOrdinaryZone'),
        loc.getString('sftkBoundaryZone'),
      ],
      
      data: data
    );
  }

  pw.Widget _thermicReportTable(pw.Context context) {
    final rep = s.thermicReport;

    final List<List<String>> items = [];

    List<String> anchorRow;
    List<String> dimLoc = ['Dimension.linear', 'Dimension.point', 'Dimension.planar'];
    List<String> dimUnits = ['m/m2', '1/m2', 'm2/m2'];
    
    rep.rows.forEach((row) {
      
      if (row.blockName == 'otherAnchor') return;

      String itemName;
      if (row.blockName == 'ejotAnchor')
        itemName = 'ejotAnchorReport';
      else 
        itemName = row.blockName;
      
      if (itemName == BlockBinding.UNIVERSAL.toString())
        itemName = row.inst.data['pCustomName'].textValue;
      else itemName = loc.getString(itemName);

      final List<String> r = [
        loc.getString(dimLoc[row.dimension.index]) + '\n' + itemName,
        roundDouble(row.unitGeo, 5).toString() + ' ' + loc.getString(dimUnits[row.dimension.index]),
        roundDouble(row.unitLoss,5).toString(),
        row.loss.toString(),
        row.lossPercent.toString()
      ];

      if (row.blockName == 'ejotAnchor') {
        anchorRow = r;
      } else {
        items.add(r);
      }
    });

    items.add(anchorRow);

    items.add([loc.getString('total'), '', '', roundDouble(rep.ejotAnchorLoss + rep.sumLossWithoutAnchorEjot, 5).toString(), '100']);

    return CustomTable.fromTextArray(
      context: context, 
      columnWidths: {
        0: pw.FlexColumnWidth(0.2),
        1: pw.FlexColumnWidth(0.2),
        2: pw.FlexColumnWidth(0.2),
        3: pw.FlexColumnWidth(0.2),
        4: pw.FlexColumnWidth(0.2),
      },
      headerDecoration: pw.BoxDecoration(
        borderRadius: 2,
        color: baseColor,
      ),
      headerHeight: 78,
      headerStyle: pw.TextStyle(
        color: _baseTextColor,
        fontSize: 10,
        fontWeight: pw.FontWeight.bold,
      ),
      headers: [
        loc.getString('elementOfConstruction'),
        loc.getString('unitGeo'),
        loc.getString('pUnitHeatLoss') + ', ' + loc.getString('W/m2C'),
        loc.getString('unitFlow') + ', ' + loc.getString('W/m2C'),
        loc.getString('rateAnchorLoss') + ', %'
      ],
      data: items
    );
  }


  pw.Widget _thermicReport(pw.Context context) {
    final rep = s.thermicReport;
    final hasComparison = s.thermicCalc.hasComparison();

    final Map<int, pw.TableColumnWidth> widths = {
      0: pw.FlexColumnWidth(hasComparison ? 0.7 : 0.8),
      1: pw.FlexColumnWidth(hasComparison ? 0.15 : 0.2),
    };
    if (hasComparison)
      widths[2] = pw.FlexColumnWidth(0.15);
    
    final List<String> headers = [
      loc.getString('value'),
      loc.getString('ejotHD')
    ];
    if (hasComparison) 
      headers.add(loc.getString('otherAnchor'));

    final List<List<String>> items = [
      [ loc.getString('unitHeatLossAnchor') + ', ' + loc.getString('W/m2C'), roundDouble(rep.rows[1].unitLoss, 3).toString() ],
      [ loc.getString('rateAnchorLoss') + ', %', roundDouble(100/(rep.sumLossWithoutAnchorEjot/rep.ejotAnchorLoss + 1), 1).toString() + ' %'],
      [ loc.getString('estimatedResistance') + ', ' + loc.getString('m2C/W'), roundDouble(1/(rep.sumLossWithoutAnchorEjot + rep.ejotAnchorLoss), 4).toString() ],
      [ loc.getString('rNorm') +', ' + loc.getString('m2C/W'), roundDouble(rep.r0norm, 4).toString(), roundDouble(rep.r0norm, 4).toString() ],
    ];

    // #4?
    if (s.hasTwoInsulators()) {
      items.add([
        loc.getString('isoWidthFixed') + ' ' + loc.getString('mm'),
        s.insulatorFixedWidthNFS.toString(),
      ]);
    }

    // #4?5?
    if (s.shouldCalculateInsulatorWidth) {
      items.add([
        loc.getString(s.hasTwoInsulators() ? 'isoWidthAdjust' : 'isoWidth') + ' ' + loc.getString('mm'), 
        s.thermicCalc.calculatedInsulatorWidthEjot.toString(),
      ]);
    } else {
      items.add([
        loc.getString(s.hasTwoInsulators() ? 'isoWidthAdjust' : 'isoWidth') + ' ' + loc.getString('mm'), 
        s.insulatorWidth.toString(),
      ]);
    }

    items.add([ 
      loc.getString('facadeNoHoles'), roundDouble(rep.facadeSquareWithoutHoles,2).toString(), roundDouble(rep.facadeSquareWithoutHoles,2).toString(),
    ]);

    // #5?6?
    int annualItemIdx = items.length;
    items.add([
      loc.getString('annualLoss') + ', ' + loc.getString('Gcal/year'),
      // + (s.shouldCalculateInsulatorWidth ? ('\n'+ loc.getString('withR0norm')) : '')
      rep.ejotAnnualLoss.toString(),
    ]);

    if (hasComparison) {
      items[0].add(roundDouble(rep.otherAnchor.unitLoss, 3).toString());
      items[1].add(roundDouble(100/(rep.sumLossWithoutAnchorOther/rep.otherAnchorLoss + 1), 1).toString() + ' %');
      items[2].add(roundDouble(1/(rep.sumLossWithoutAnchorOther + rep.otherAnchorLoss), 4).toString());    
      
      int rowN = 4;
      if (s.hasTwoInsulators()) {
        items[rowN].add(s.insulatorFixedWidthNFS.toString());
        ++rowN;
      }
      if (s.shouldCalculateInsulatorWidth)
        items[rowN].add(s.thermicCalc.calculatedInsulatorWidthOther.toString());
      else
        items[rowN].add(s.insulatorWidth.toString());

      items[annualItemIdx].add( s.thermicReport.otherAnnualLoss.toString() );
    }

    return CustomTable.fromTextArray(
      context: context, 
      columnWidths: widths,
      headerDecoration: pw.BoxDecoration(
        borderRadius: 2,
        color: baseColor,
      ),
      headerHeight: 40,
      headerStyle: pw.TextStyle(
        color: _baseTextColor,
        fontSize: 10,
        fontWeight: pw.FontWeight.bold,
      ),
      headers: headers,
      data: items
    );
  }


  pw.Widget _ecoReport(pw.Context context) {
    
    List<pw.Widget> items = [];

    double total = 0;
    
    if (s.ecoCalc.canCalculateAnchorCost()) {
      total += s.ecoCalc.anchorEconomyMoney;
      items.add(_ecoReportAnchors(context));
    }

    if (s.ecoCalc.canCalculateEnergyCostEconomy()) {
      total += s.ecoCalc.energyCostEconomyMoney;
      items.add(_ecoReportEnergyCost(context));
    }

    if (s.ecoCalc.canCalculateInsulatorEconomy()) {
      total += s.ecoCalc.insulatorCostEconomy;
      items.add(pw.SizedBox(height: 10));
      items.add(_ecoReportInsulatorCost(context));
    }

    items.addAll([
      pw.SizedBox(height: 10),
      pw.Text(
        loc.getString('total') + ': ' + _formatCurrency(total),
        style: pw.TextStyle(
          color: baseColor,
          fontSize: 12,
          fontWeight: pw.FontWeight.bold
        ),
      ),
    ]);

    return pw.Column(children: items);
  }

  pw.Widget _ecoReportAnchors(pw.Context context) {
    final Map<int, pw.TableColumnWidth> widths = {
      0: pw.FlexColumnWidth(0.7),
      1: pw.FlexColumnWidth(0.3),
    };

    final List<List<String>> items = [
      [loc.getString('economyAnchor')],
      [loc.getString('ejotHD'), 
        _formatCostByQuantityTotal( s.economyCostAnchor, s.ecoCalc.ejotQty, roundDouble(s.ecoCalc.ejotQty*s.economyCostAnchor, 0), loc.getString('pcs') )],
      [loc.getString('otherAnchor'), 
        _formatCostByQuantityTotal( s.economyCostOtherAnchor, s.ecoCalc.otherQty, roundDouble(s.ecoCalc.ejotQty*s.economyCostOtherAnchor, 0), loc.getString('pcs') )],
      [loc.getString('economy'), _formatCurrency(s.ecoCalc.anchorEconomyMoney)]
    ];

    return CustomTable.fromTextArray(
      context: context, 
      columnWidths: widths,
      headerDecoration: pw.BoxDecoration(
        borderRadius: 2,
        color: baseColor,
      ),
      headerHeight: 30,
      headerStyle: pw.TextStyle(
        color: _baseTextColor,
        fontSize: 10,
        fontWeight: pw.FontWeight.bold,
      ),
      data: items
    );
  }


  pw.Widget _ecoReportEnergyCost(pw.Context context) {
    final Map<int, pw.TableColumnWidth> widths = {
      0: pw.FlexColumnWidth(0.7),
      1: pw.FlexColumnWidth(0.3),
    };

    final List<List<String>> items = [
      [loc.getString('economyHeating')],
      [loc.getString('ejotHD'), 
        _formatCostByQuantityTotal( s.economyCostEnergy, s.thermicReport.ejotAnnualLoss, roundDouble(s.economyCostEnergy*s.thermicReport.ejotAnnualLoss, 0), loc.getString('Gcal/year') )],
      [loc.getString('otherAnchor'), 
        _formatCostByQuantityTotal( s.economyCostEnergy, s.thermicReport.otherAnnualLoss, roundDouble(s.economyCostEnergy*s.thermicReport.otherAnnualLoss, 0), loc.getString('Gcal/year') )],
      [loc.getString('economy'), _formatCurrency(s.ecoCalc.energyCostEconomyMoney)]
    ];

    return CustomTable.fromTextArray(
      context: context, 
      columnWidths: widths,
      headerDecoration: pw.BoxDecoration(
        borderRadius: 2,
        color: baseColor,
      ),
      headerHeight: 30,
      headerStyle: pw.TextStyle(
        color: _baseTextColor,
        fontSize: 10,
        fontWeight: pw.FontWeight.bold,
      ),
      data: items
    );
  }

  
  pw.Widget _ecoReportInsulatorCost(pw.Context context) {
    final Map<int, pw.TableColumnWidth> widths = {
      0: pw.FlexColumnWidth(0.7),
      1: pw.FlexColumnWidth(0.3),
    };

    final List<List<String>> items = [
      [loc.getString('economyInsulator')],
      [loc.getString('ejotHD'), 
        _formatCostByQuantityTotal( s.economyCostInsulator, s.ecoCalc.insulatorVolumeEjot, roundDouble(s.economyCostInsulator * s.ecoCalc.insulatorVolumeEjot, 0), loc.getString('m3') )],
      [loc.getString('otherAnchor'), 
        _formatCostByQuantityTotal( s.economyCostInsulator, s.ecoCalc.insulatorVolumeOther, roundDouble(s.economyCostInsulator * s.ecoCalc.insulatorVolumeOther, 0), loc.getString('m3') )],
      [loc.getString('economy'), _formatCurrency(s.ecoCalc.insulatorCostEconomy)]
    ];

    return CustomTable.fromTextArray(
      context: context, 
      columnWidths: widths,
      headerDecoration: pw.BoxDecoration(
        borderRadius: 2,
        color: baseColor,
      ),
      headerHeight: 30,
      headerStyle: pw.TextStyle(
        color: _baseTextColor,
        fontSize: 10,
        fontWeight: pw.FontWeight.bold,
      ),
      data: items
    );
  }

  pw.Widget _anchorReport(pw.Context context) {
    return s.anchorCalc.hasComparison() ? _anchorReportWithComparison(context) : _anchorReportNoComparison(context);
  }

}

String _formatCurrency(double amount) {
  return '₽ ${formatCurrency( amount.toStringAsFixed(2) )}';
}

String _formatDate(String languageCode, DateTime date) {
  final format = DateFormat.yMMMMd(languageCode);
  return format.format(date);
}

String formatFilename(DateTime date) {
  final format = DateFormat.yMMMMd().add_Hms();
  return format.format(date);
}

String _formatCostByQuantityTotal(double cost, dynamic qty, double total, String pcs) {
  return '₽ ' + cost.toString() + ' x ' + qty.toString() + ' ' + pcs + ' = ' + formatCurrency(total.toStringAsFixed(2));
}
