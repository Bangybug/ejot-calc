class MaterialGroup {
  final int id;
  final List<Material> materials;
  final String name;

  MaterialGroup({this.id, this.materials, this.name});

  factory MaterialGroup.fromJson(
      Map<String, dynamic> json, int id, int mtlIdCounter) {
    return MaterialGroup(
        id: id,
        name: json["name"],
        materials: _buildMaterials(json["items"] as List, mtlIdCounter));
  }

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'items': materials.map((element) => element.toJson()).toList()
      };
}

List<Material> _buildMaterials(List<dynamic> json, int mtlIdCounter) {
  List<Material> ret = [];
  for (int i = 0; i < json.length; ++i) {
    ret.add(Material.fromJson(json[i], mtlIdCounter++));
  }
  return ret;
}

class Material {
  final int id;

  // FYI cannot use double here, otherwise I have to write sophisticated parsing logic with type conversion insurance
  final List<dynamic> densityRange;
  final String name;
  final int anchorType;
  final List<double> conductivity;

  Material(
      {this.id,
      this.densityRange,
      this.name,
      this.anchorType,
      this.conductivity});

  factory Material.fromJson(Map<String, dynamic> json, int id) => Material(
      id: id,
      name: json["name"],
      anchorType: json.containsKey("anchorType") ? json["anchorType"] : -1,
      densityRange: List.from(json["densityRange"]),
      conductivity: (json["conductivity"] as List).cast<double>());

  Map<String, dynamic> toJson() => {
        'id': id,
        'densityRange': densityRange,
        'name': name,
        'anchorType': anchorType,
        'conductivity': conductivity
      };
}
