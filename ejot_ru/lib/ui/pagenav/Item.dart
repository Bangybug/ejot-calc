
class Item {
  Item({
    this.title,
    this.subtitle,
  });

  final String title;
  final String subtitle;
}
