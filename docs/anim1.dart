import 'package:flutter/widgets.dart';

class ImgSchemeSlider extends StatefulWidget {
  ImgSchemeSlider({ Key key }) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return ImgSchemeSliderState();
  }
}


class ImgSchemeSliderState extends State<ImgSchemeSlider>  with SingleTickerProviderStateMixin {

  Animation<double> animation;
  AnimationController controller;

  @override            
  void initState() {
    super.initState();
    controller = AnimationController(duration: const Duration(seconds: 2), vsync: this);            
    animation = Tween<double>(begin: 0, end: 300).animate(controller)            
      ..addListener(() {            
        setState(() {            
          // The state that has changed here is the animation object’s value.            
        });            
      });            
    controller.forward(from: 1.0);            
  }

  @override
  void dispose() { 
    controller.dispose();  
    super.dispose(); 
  }

  @override
  Widget build(BuildContext context) {
    return Center(            
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 10),  
        height: animation.value,
        width: animation.value,
        child: Image.asset(
          'assets/icons/FacadeType.SFTK.png'
        )
      ),            
    );
  }


}


///
/////
///
///

import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class ImgSchemeSlider extends StatefulWidget {
  ImgSchemeSlider({ Key key, this.entries, this.selectedIndex, this.buttons }) : super(key: key);
  
  final List<SchemeEntry> entries;
  final List<SchemeButton> buttons;
  final int selectedIndex;

  @override
  State<StatefulWidget> createState() {
    return ImgSchemeSliderState();
  }
}

class SchemeButton {
  final Widget button;
  final GlobalKey buttonKey;
  final String hint;
  Offset _buttonOffset;

  SchemeButton({this.button, this.buttonKey, this.hint});
}

class SchemeEntry {
  final String imgPath;
  final List<double> buttonTargetX;  

  SchemeEntry({this.buttonTargetX, this.imgPath});
}


class ImgSchemeSliderState extends State<ImgSchemeSlider>  with SingleTickerProviderStateMixin, WidgetsBindingObserver {

  Animation<Offset> _inAnim, _outAnim, _slideInLeftAnimation, _slideOutLeftAnimation, _slideInRightAnimation, _slideOutRightAnimation;
  Animation<double> _opacityInAnimation, _opacityOutAnimation;
  AnimationController _controller;

  final GlobalKey _stackKey = GlobalKey();

  final List<Image> images = []; 
  int currentImageIndex = -1;
  int oldImageIndex = -1;
  int windowWidth;

  void select(int index, bool animate) {
    setState(() {
      oldImageIndex = currentImageIndex;
      currentImageIndex = index;
      bool dir = oldImageIndex == -1 ? false : (currentImageIndex - oldImageIndex > 0);
      if (dir) {
        _inAnim = _slideInRightAnimation;
        _outAnim = _slideOutRightAnimation; 
      } else {
        _inAnim = _slideInLeftAnimation;
        _outAnim = _slideOutLeftAnimation;
      }
      if (animate) {
        _controller.forward(from: 0);
        //controller.reverse(from: 1);
      }
    });
  }

  Image _getImage(int index) {

    if (images.length > index) {
      if (images[index] != null) {
        return images[index];
      }
    }
    while (images.length <= index) {
      images.add(null);
    }

    if (images[index] == null) {
      images[index] = Image.asset( widget.entries[index].imgPath, alignment: Alignment.topLeft, fit: BoxFit.fill);
    }

    return images[index];
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    _controller.dispose();
    super.dispose();
  }


  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance.addObserver(this);

    _controller = AnimationController(
      duration: const Duration(seconds: 1),
      vsync: this,
    );
    
    _opacityInAnimation = Tween(begin: 0.0, end: 1.0).animate(CurvedAnimation(
      parent: _controller,
      curve: Curves.easeOutExpo,
    ));

    _opacityOutAnimation = Tween(begin: 1.0, end: 0.0).animate(CurvedAnimation(
      parent: _controller,
      curve: Curves.easeOutExpo,
    ));

    _slideInLeftAnimation = Tween<Offset>(
      begin: const Offset(1.0, 0.0),
      end: Offset.zero,
    ).animate(CurvedAnimation(
      parent: _controller,
      curve: Curves.easeOutExpo,
    ));

    _slideOutLeftAnimation = Tween<Offset>(
      begin: Offset.zero,
      end: const Offset(-1.0, 0.0),
    ).animate(CurvedAnimation(
      parent: _controller,
      curve: Curves.easeOutExpo,
    ));

    _slideInRightAnimation = Tween<Offset>(
      begin: const Offset(-1.0, 0.0),
      end: Offset.zero,
    ).animate(CurvedAnimation(
      parent: _controller,
      curve: Curves.easeOutExpo,
    ));
    
    _slideOutRightAnimation = Tween<Offset>(
      begin: Offset.zero,
      end: const Offset(1.0, 0.0),
    ).animate(CurvedAnimation(
      parent: _controller,
      curve: Curves.easeOutExpo,
    ));


    select(widget.selectedIndex, true);

    WidgetsBinding.instance.addPostFrameCallback( _afterLayout );
  }


  @override void didChangeMetrics() {
    WidgetsBinding.instance.addPostFrameCallback( _afterLayout );
  }


  double _getImgWidth() {
    return min( MediaQuery.of(context).size.width * 0.40, 400 );
  }


  Widget buildConstraints(Widget child) {
    return ConstrainedBox(
      constraints: BoxConstraints(
        maxHeight: 250,
        minHeight: 250,
        maxWidth: _getImgWidth()
      ),
      child: child,
    );
  }

  void _afterLayout(_) {

    for (int i=0; i<widget.buttons.length; ++i) {
      final RenderBox renderBox = widget.buttons[i].buttonKey.currentContext.findRenderObject();
      widget.buttons[i]._buttonOffset =  
        renderBox.localToGlobal( Offset.zero, ancestor: _stackKey.currentContext.findRenderObject())
          .translate(0, renderBox.size.height/2 - 2 );
    }

    setState(() {
      // update
    });
  }

  Widget _buildLine(int i) {
    if (currentImageIndex == -1) {
      return Container(width: 0,height: 0);
    }

    Offset offset = widget.buttons[i]._buttonOffset;
    if (offset == null) {
      offset = Offset.zero;
    }

    double imgWidth = _getImgWidth();

    return AnimatedBuilder(
      animation: _inAnim,
      
      builder: (BuildContext context, Widget child) {
        
        double imgX = widget.entries[currentImageIndex].buttonTargetX[i] * imgWidth;
        double posX = max(0, _inAnim.value.dx * imgWidth + imgX);
        double width = max(0, offset.dx - posX);

        return Container(
          margin: EdgeInsets.only(left: posX, top: offset.dy),
          height: 2,
          width: width,
          color: Colors.blueGrey[300]
        );
       
      },
    );
  }

  Widget _buildButton(int i) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children:  <Widget>[
        Text(widget.buttons[i].hint, style: TextStyle(fontSize: 11),),
        widget.buttons[i].button
      ]
    );
  }


  @override
  Widget build(BuildContext context) {

    List<Widget> children = [
      buildConstraints( FadeTransition(
        opacity: _opacityInAnimation,
        child: SlideTransition(
          position: _inAnim,
          child: currentImageIndex != -1 ? _getImage(currentImageIndex) : Container()
        )
      ) ),

      buildConstraints( FadeTransition(
        opacity: _opacityOutAnimation,
        child: SlideTransition(
          position: _outAnim,
          child: oldImageIndex != -1 ? _getImage(oldImageIndex) : Container()
        )
      ) )
    ];

    children.addAll(List<Widget>.generate( widget.buttons.length, _buildLine));

    // FYI using positioned elements in Stack will not make that stack auto-grow 
    // so I used container positionized by margin

    children.add(
      Container(
        margin: EdgeInsets.only(left: _getImgWidth() + 20, right: 10 ),
        constraints: BoxConstraints(maxWidth: 400, minHeight: 250),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: List<Widget>.generate(widget.buttons.length, _buildButton)
        )
      )
    );

    return Stack(
      key: _stackKey,
      children: children,
    );
  }


}x