import 'package:ejot_ru/data/AppLocalizations.dart';
import 'package:flutter/widgets.dart';

class SftkClass {
  int id;

  SftkClass({this.id});
}

List<SftkClass> sftkClasses = [
  SftkClass(id: 0), 
  SftkClass(id: 1),
  SftkClass(id: 2)
];


String localizeSftkClass(dynamic cl, BuildContext context) {
  if (cl == null) {
    return AppLocalizations.of(context).getString('sftkClass');
  }
  return AppLocalizations.of(context).getString('sftkClass'+(cl as SftkClass).id.toString());
}

String localizeSftkClassLabel(dynamic cl, BuildContext context) {
  if (cl == null) {
    return '';
  }
  return AppLocalizations.of(context).getString('sftkClassLabel'+(cl as SftkClass).id.toString());
}


String localizeInsulatorWidth(dynamic b, BuildContext context) {
  if (!b) {
    return AppLocalizations.of(context).getString('inputInsulatorWidth');
  }
  return AppLocalizations.of(context).getString('calculateInsulatorWidth');
}
