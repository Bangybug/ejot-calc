import 'dart:typed_data';

import 'package:ejot_ru/data/AppLocalizations.dart';
import 'package:ejot_ru/ui/widgets/AutoSizeText.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pdf/pdf.dart';
import 'package:printing/printing.dart';

/*
FYI I was getting this strange error:

type 'PdfAssetPreviewArguments' is not a subtype of type 'PdfAssetPreviewArguments' where
  PdfAssetPreviewArguments is from package:ejot_ru/ui/pdf/pdfAssetPreview.dart
  PdfAssetPreviewArguments is from package:ejot_ru/ui/pdf/PdfAssetPreview.dart

I had to hardcode parameters instead of using arguments
*/

Future<Uint8List> viewPdf(String assetPath, PdfPageFormat pageFormat) async {
  var data = await rootBundle.load(assetPath);
  return data.buffer.asUint8List();
}

class PdfAssetPreview extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    final List<PdfPreviewAction> actions = [];
    final loc = AppLocalizations.of(context);

    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: true,
        title: AutoSizeText(
          loc.getString('exampleTariffs'),
          presetFontSizes: [15.0, 13.0, 11.0],
          maxLines: 4,
        ),
        leading: IconButton(icon:Icon(Icons.arrow_back),
          onPressed:() => Navigator.pop(context, false),
        )
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: PdfPreview(
              //maxPageWidth: 700,
              build: (PdfPageFormat pageFormat) => viewPdf('assets/tariffs.pdf', pageFormat),
              actions: actions,
            ),

          )
        ],
      ),
    );
  }
}
