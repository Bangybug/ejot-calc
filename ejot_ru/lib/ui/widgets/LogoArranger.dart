import 'dart:math' as math;
import 'package:ejot_ru/ui/pagenav/PageNav.dart';
import 'package:ejot_ru/util/TabletDetector.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class LogoArranger extends StatelessWidget {

  LogoArranger({ Key key }) : super(key: key);



  @override
  Widget build(BuildContext context) {
    final Orientation orientation = MediaQuery.of(context).orientation;
    final query = MediaQuery.of(context);
    Size size = query.size;
    final isTablet = TabletDetector.isTablet(query);
    if (isTablet) {
      size = Size(size.width-PageNav.STEPPER_WIDTH, size.height);
    }

    const double ANCHOR_IMG_WIDTH = 600;
    const double LOGO_HEIGHT = 220;
    const double LOGO_RIGHT_OFFSET = 20;
    const double anchorHdAr = 702.0/217.0;
    const double ANCHORHD_LOGO_WIDTH = 100;
    const double ANCHORHD_LOGO_HEIGHT = ANCHORHD_LOGO_WIDTH/anchorHdAr;

    if (orientation == Orientation.portrait) {
      return Stack(      
        children: <Widget>[
          Image.asset('assets/icons/logo_bg.png', alignment: Alignment.topLeft, fit: BoxFit.fill, width: size.width-ANCHOR_IMG_WIDTH-LOGO_RIGHT_OFFSET, height: LOGO_HEIGHT),
          Container(
            margin: EdgeInsets.only(left: size.width-LOGO_RIGHT_OFFSET),
            child: Image.asset('assets/icons/logo_bg.png', alignment: Alignment.topLeft, fit: BoxFit.fill, width: LOGO_RIGHT_OFFSET, height: LOGO_HEIGHT)
          ),
          Container(
            margin: EdgeInsets.only(left: math.max(0,size.width-LOGO_RIGHT_OFFSET-ANCHOR_IMG_WIDTH)),
            child: Image.asset('assets/icons/anchorlogo.png', alignment: Alignment.topLeft, fit: BoxFit.fill, width: ANCHOR_IMG_WIDTH, height: LOGO_HEIGHT, filterQuality:FilterQuality.high, isAntiAlias: true),
          ),
          Container(
            margin: EdgeInsets.only(left: 10, top: 10),
            child: Image.asset('assets/logo.png', width: 160),
          ),
          Container(
            margin: EdgeInsets.only(left: size.width-ANCHORHD_LOGO_WIDTH-10, top: LOGO_HEIGHT-ANCHORHD_LOGO_HEIGHT+10),
            child: Image.asset('assets/icons/anchorhd.png', height: 30),
          )
        ],
      );
    } else if (orientation == Orientation.landscape) {

      final double allowHeight = (0.3 * size.height).floorToDouble();
      final logoHeight = allowHeight;
      final scaleFactor = logoHeight / 274; // FYI calculated based on real and desired image size, real is (1037x475), desired is (600xLOGO_HEIGHT), LOGO_HEIGHT=274 was derived from proportion
      final anchorImgWidth = scaleFactor * ANCHOR_IMG_WIDTH;
      final anchorHDLogoWidth = scaleFactor * ANCHORHD_LOGO_WIDTH;
      final anchorHDLogoHeight = scaleFactor * ANCHORHD_LOGO_HEIGHT;

      return Stack(      
        children: <Widget>[
          Image.asset('assets/icons/logo_bg.png', alignment: Alignment.topLeft, fit: BoxFit.fill, width: size.width-anchorImgWidth-LOGO_RIGHT_OFFSET, height: logoHeight),
          Container(
            margin: EdgeInsets.only(left: size.width-LOGO_RIGHT_OFFSET),
            child: Image.asset('assets/icons/logo_bg.png', alignment: Alignment.topLeft, fit: BoxFit.fill, width: LOGO_RIGHT_OFFSET, height: logoHeight)
          ),
          Container(
            margin: EdgeInsets.only(left: size.width-LOGO_RIGHT_OFFSET-anchorImgWidth),
            child: Image.asset('assets/icons/anchorlogo.png', alignment: Alignment.topLeft, fit: BoxFit.fill, width: anchorImgWidth, height: logoHeight, filterQuality:FilterQuality.high, isAntiAlias: true),
          ),
          Container(
            margin: EdgeInsets.only(left: 10, top: 10),
            child: Image.asset('assets/logo.png', width: 160,),
          ),
          Container(
            margin: EdgeInsets.only(left: size.width-anchorHDLogoWidth-10, top: logoHeight-anchorHDLogoHeight+10),
            child: Image.asset('assets/icons/anchorhd.png', width: anchorHDLogoWidth, height: anchorHDLogoHeight),
          )
        ],
      );
    }

    return Container();
  }

}