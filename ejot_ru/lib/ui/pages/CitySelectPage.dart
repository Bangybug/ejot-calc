import 'package:ejot_ru/data/AppLocalizations.dart';
import 'package:ejot_ru/model/AppStore.dart';
import 'package:ejot_ru/model/City.dart';
import 'package:ejot_ru/model/CityStore.dart';
import 'package:ejot_ru/ui/widgets/FilteredGroupedList.dart';
import 'package:ejot_ru/ui/widgets/MyTextField.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'BasePage.dart';

class CitySelectArguments {
  final AppStore appStore;
  final CityStore cityStore;

  CitySelectArguments(this.appStore, this.cityStore);
}

class CitySelectPage extends StatefulWidget {
  // FYI selectedCity is accessible via AppStore, but I decided to pass it from outside to the constructor, because
  // store resolving requires context which is not yet here, and by convention we need immutable members in Stateful widgets,
  // thus we have immutable controller, and it's good idea to initalize it with the selected city name right in its constructor
  // Alternatively I could use initState to init the controller, the context is accessible there.

  CitySelectPage({Key key, City selectedCity}) : this._searchController = TextEditingController(text: selectedCity?.name), super(key: key);
  
  final TextEditingController _searchController;

  @override
  State<StatefulWidget> createState() {
    return CitySelectPageState();
  }
}

class CitySelectPageState extends State<CitySelectPage> {
  
  final GlobalKey<FilteredGroupedListState<City>> _cityListKey = new GlobalKey<FilteredGroupedListState<City>>();
  final double headerHeight = 40;
  AppStore _appStore;
  CityStore _cityStore;

  Widget _displayDeleteTextIcon() {
    if (widget._searchController.text.length > 0) {
      return IconButton(
        icon: Icon(Icons.close),
        color: Colors.grey,
        onPressed: () {
          // FYI this is going to  produce recoverable issue with selection - invalid text selection: TextSelection(baseOffset:...)
          // Solution: hide keyboard just in case, and delay clear in searchController
          FocusScope.of(context).unfocus();
          WidgetsBinding.instance.addPostFrameCallback( (_) => widget._searchController.clear());
         
          setState(() {
            _cityListKey.currentState.handleFilterTextChanged("");
          });
        },
      );
    } else {
      return null;
    }
  }

  Widget _buildTextfield(BuildContext context) {
    var loc = AppLocalizations.of(context);

    // FYI flutter prohibits changing text property of controller during build
    // so I cannot init my textfield with some value in this place:
    // String selectedCity = Provider.of<AppStore>(context).selectedCity;
    // if (null != selectedCity) {
    //   _searchController.text = selectedCity;
    // }

    return MyTextField(
      prefixIconNoPadding: false,
      prefixIcon: Icon(
        Icons.search,
        size: 26.0,
        color: Colors.grey,
      ),
      suffixIcon: _displayDeleteTextIcon(),
      hintText: loc.getString('searchCity'),
      controller: widget._searchController,
      onChanged: (text) {
        setState(() { 
          _cityListKey.currentState.handleFilterTextChanged(text);
        });
      }
    );
  }


  Widget _buildInfo(BuildContext context) {
    var loc = AppLocalizations.of(context);
    return Material(
      textStyle: TextStyle(color: Theme.of(context).primaryColorDark, fontSize: 10),
      child: Padding(
        padding: EdgeInsets.only(left: 8, bottom: 16),
        child: Text(loc.getString('cityExplain'))
      ),
    );
  }


  Widget _buildHeader(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        _buildTextfield(context),
        _buildInfo(context)
      ]
    );
  }

  void handleCitySelect(dynamic city) {
    setState(() {
      _appStore.selectedCity = city;
      _appStore.lastUpdatedCount++;
      if (_appStore.selectedCity.windZone != null)
        _appStore.windZone = _appStore.selectedCity.windZone;
      WidgetsBinding.instance.addPostFrameCallback( (_) => widget._searchController.text = city.name ); 
    });
  }


  Widget _buildRegionList(BuildContext context) {
    var loc = AppLocalizations.of(context);
    final CitySelectArguments args = ModalRoute.of(context).settings.arguments;
    
    _cityStore.fetchRegions(context, args.appStore.forceLanguage ?? loc.locale.languageCode);

    return Scrollbar( 
      child: Observer(
        builder: (context) => new FilteredGroupedList<City>(
          key: _cityListKey,
          isLoading: _cityStore.regions.isEmpty,
          loadedLabel: loc.getString('loading'),
          noItemsLabel: loc.getString('cityNotFound'),
          data: RegionData( regions: _cityStore.regions ),
          onItemSelected: handleCitySelect
        )
      )
    );
  }


  @override
  Widget build(BuildContext context) {
    final CitySelectArguments args = ModalRoute.of(context).settings.arguments;
    _appStore = args.appStore;
    _cityStore = args.cityStore;

    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        title: Text(AppLocalizations.of(context).getString('selectCity')),
        leading: IconButton(icon:Icon(Icons.check),
          onPressed:() => Navigator.pop(context, false),
        )
      ),

      // FYI numeric keyboards do not have done button on ios, so they are always visible, I hide them with unfocus 
      body: BasePage(
        headerContent: _buildHeader(context),
        mainContent: _buildRegionList(context)
      ) 
    );
  }

}