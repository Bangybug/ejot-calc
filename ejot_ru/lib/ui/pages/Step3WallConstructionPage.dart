import 'package:ejot_ru/data/AppLocalizations.dart';
import 'package:ejot_ru/model/AppStore.dart';
import 'package:ejot_ru/model/Block.dart';
import 'package:ejot_ru/ui/pages/BaseBlockEditorPage.dart';
import 'package:ejot_ru/ui/widgets/MyHeader.dart';
import 'package:ejot_ru/util/ScrollControlMixin.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:provider/provider.dart' as P;

import 'BasePage.dart';

class Step3WallConstructionPage extends StatefulWidget{
  
  @override
  _Step3WallConstructionPageState createState() => _Step3WallConstructionPageState();
}


class _Step3WallConstructionPageState extends State<Step3WallConstructionPage> with ScrollControllerMixin{
  AppStore _appStore;
  GlobalKey<BasePageState> _pageKey = GlobalKey<BasePageState>();

  void didChangeDependencies() {
    super.didChangeDependencies(); 
    _appStore ??= P.Provider.of<AppStore>(context);

    // FYI just tried to observe individual items, by firing list[index] = list[index]
    // it didn't work, but setting list[index] = null did work (reference must be changed in order to trigger the event)
    // _appStore.sftkBlocks.observe((listChange) {
    //   print("sftk change");
    //   if (_appStore.facade == FacadeType.SFTK) {
    //     setState(() {
    //     });
    //   }
    // });
  }

  Widget _buildBlockItem(BuildContext context, List<Block> inList, int index) {
    Block block = inList[index];
    var loc = AppLocalizations.of(context);
    return Observer( builder: (context) =>
      ListTile(
        leading: Switch.adaptive(
          activeColor: Theme.of(context).primaryColor,
          value: block.enabled,
          onChanged: (bool value) async {
            if (value && !block.isValid()) {
              _showBlockEditingPage(context, block);
            } else {
              block.enabled = value;
            }
          },
        ),
        title: Text(loc.getString(block.blockName)),
        subtitle: Text(loc.getString('filledElementsNumber') + ': ' + block.countValidInstances().toString()),
        trailing: Icon(Icons.navigate_next),
        onTap: () {
          _showBlockEditingPage(context, block);
        },
      )
    );
  }
  
  Widget _buildList(BuildContext context) {
    // FYI if i dont wrap listview in Expanded, I get this in the log when I try to scroll the list:
    // "Failed assertion: line 1245 pos 12: 'value.isFinite'".
    return Expanded(
      child: Observer(
        builder: (context) {
          List<Block> items = _appStore.facade == FacadeType.SFTK ? _appStore.sftkBlocks : _appStore.nfsBlocks;
          return ListView.builder
          (
            padding: EdgeInsets.only(bottom: 50),
            controller: scrollController,
            itemCount: items.length*2, // (item + divider)
            itemBuilder: (BuildContext ctxt, int index) {
              if (index % 2 == 0) {
                int itemN = index ~/ 2;
                // FYI cannot make references to the ObservableList, the reference will be initialized with zero elements
                // ObservableList<Block> inList = items;
                return _buildBlockItem(context, items, itemN);
              } else {
                return Divider(color: Colors.black26, height: 1);
              }
            }
          );

          // FYI If I was about to use ListView, I should not have placed it into scrollable, also had to add two lines,
          // otherwise I was getting 'Vertical viewport was given unbounded height'
          // return ListView(
          //   children: listTiles,
          //   scrollDirection: Axis.vertical,
          //   shrinkWrap: true,  
          // );
        } 
      )
    ); 

    // can use AbsorbPointer( absorbing: false,..) to disable widgets
  }

  Widget _buildTopHint(BuildContext context) {
    var loc = AppLocalizations.of(context);
    return Padding( 
      padding: EdgeInsets.only(left: 4),
      child: Container(
        color: Colors.white70,
        alignment: AlignmentDirectional.center,
        padding: EdgeInsets.only(left:10, top: 10, right: 10, bottom: 8),
        child: Text(loc.getString('fillBlockHint')),
      )
    );
    
  }

  void initState() {
    super.initState();
    initScroll(_pageKey);
  }


  void dispose() {
    super.dispose();
    disposeScroll();
  }

  Widget _buildPage(BuildContext context) {
    return BasePage(
      key: _pageKey,
      headerContent: MyHeader(pageIndex: 2),
      topContent: _buildTopHint(context),
      // FYI without scrollbar, the list will be scrollable but without the scrollbar indicator
      // Scrollbar has controller, but if I attach my ScrollControlMixi controller to it, it won't work, 
      // I should inject my scroll controller into the list. 
      mainContent: Scrollbar(  
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[ 
            Divider(height: 1, color: Colors.grey),
            _buildList(context)
          ]
        )
      )
    );
  }


  // FYI await does not work outside async functions - they are executed asynchronously
  // so I had to use callbacks 
  void _showBlockEditingPage(BuildContext context, Block block, {Function callback}  ) async {
    await Navigator.pushNamed(context, '/blockEdit', arguments: BlockEditorArguments(_appStore, block));
    block.enabled = block.isValid();
    if (callback != null) 
      callback();
  }

  @override
  Widget build(BuildContext context) {
    return _buildPage(context);
  }
}