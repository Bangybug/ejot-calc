import 'dart:math';

import 'package:ejot_ru/ui/theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/svg.dart';

const double MAX_IMAGE_WIDTH = 320;
const double FIXED_IMAGE_HEIGHT = 300;

class ImgSchemeSlider extends StatefulWidget {
  ImgSchemeSlider({ Key key, this.entries, this.selectedIndex, this.buttons }) : super(key: key);
  
  final List<SchemeEntry> entries;
  final List<SchemeButton> buttons;
  final int selectedIndex;

  @override
  State<StatefulWidget> createState() {
    return ImgSchemeSliderState();
  }
}

class SchemeButton {
  final Widget button;
  final GlobalKey buttonKey;
  final String hint;

  SchemeButton({this.button, this.buttonKey, this.hint});
}

class SchemeEntry {
  final String imgPath;
  final List<double> buttonTargetX;  

  SchemeEntry({this.buttonTargetX, this.imgPath});
}


class ImgSchemeSliderState extends State<ImgSchemeSlider>  with SingleTickerProviderStateMixin, WidgetsBindingObserver {

  Animation<Offset> _inAnim, _outAnim, _slideInLeftAnimation, _slideOutLeftAnimation, _slideInRightAnimation, _slideOutRightAnimation;
  Animation<double> _opacityInAnimation, _opacityOutAnimation;
  AnimationController _controller;

  // FYI I wanted to update and store this inside Stateful widget/SchemeButton, but this gets invalidated without state reinitialization
  // so I keep calculated fields inside this state widget
  List<Offset> _buttonOffsets = [];

  final GlobalKey _stackKey = GlobalKey();

  final List<Widget> images = []; 
  int currentImageIndex = -1;
  int oldImageIndex = -1;
  int windowWidth;

  void select(int index, bool animate) {
    setState(() {
      oldImageIndex = currentImageIndex;
      currentImageIndex = index;
      bool dir = oldImageIndex == -1 ? false : (currentImageIndex - oldImageIndex > 0);
      if (dir) {
        _inAnim = _slideInRightAnimation;
        _outAnim = _slideOutRightAnimation; 
      } else {
        _inAnim = _slideInLeftAnimation;
        _outAnim = _slideOutLeftAnimation;
      }
      if (animate) {
        _controller.forward(from: 0);
      }
    });
    updateLines();
  }

  void updateLines() {
    WidgetsBinding.instance.addPostFrameCallback( _afterLayout );
  }

  Widget _getImage(int index) {

    if (images.length > index) {
      if (images[index] != null) {
        return images[index];
      }
    }
    while (images.length <= index) {
      images.add(null);
    }

    if (images[index] == null) {
      if (widget.entries[index].imgPath.endsWith('.svg')) { 
        // FYI svg should not have viewbox, instead it should have some width/height - otherwise it won't scale
        // here I also set width=max_image_width and this works as equivalent of maxWidth, in contrast Images don't need that, they merely resize to their bounds
        images[index] = SvgPicture.asset( widget.entries[index].imgPath, alignment: Alignment.topLeft, width: MAX_IMAGE_WIDTH, fit: BoxFit.fill, allowDrawingOutsideViewBox: true,);
      } else {
        images[index] = Image.asset( widget.entries[index].imgPath, alignment: Alignment.topLeft, fit: BoxFit.fill);
      }
    }

    return images[index];
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    _controller.dispose();
    super.dispose();
  }


  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance.addObserver(this);

    _controller = AnimationController(
      duration: const Duration(milliseconds: 1500),
      vsync: this,
    );
    
    _opacityInAnimation = Tween(begin: 0.0, end: 1.0).animate(CurvedAnimation(
      parent: _controller,
      curve: Curves.easeOutExpo,
    ));

    _opacityOutAnimation = Tween(begin: 1.0, end: 0.0).animate(CurvedAnimation(
      parent: _controller,
      curve: Curves.easeOutExpo,
    ));

    _slideInLeftAnimation = Tween<Offset>(
      begin: const Offset(1.0, 0.0),
      end: Offset.zero,
    ).animate(CurvedAnimation(
      parent: _controller,
      curve: Curves.easeOutExpo,
    ));

    _slideOutLeftAnimation = Tween<Offset>(
      begin: Offset.zero,
      end: const Offset(-1.0, 0.0),
    ).animate(CurvedAnimation(
      parent: _controller,
      curve: Curves.easeOutExpo,
    ));

    _slideInRightAnimation = Tween<Offset>(
      begin: const Offset(-1.0, 0.0),
      end: Offset.zero,
    ).animate(CurvedAnimation(
      parent: _controller,
      curve: Curves.easeOutExpo,
    ));
    
    _slideOutRightAnimation = Tween<Offset>(
      begin: Offset.zero,
      end: const Offset(1.0, 0.0),
    ).animate(CurvedAnimation(
      parent: _controller,
      curve: Curves.easeOutExpo,
    ));


    select(widget.selectedIndex, true);
  }


  @override void didChangeMetrics() {
    WidgetsBinding.instance.addPostFrameCallback( _afterLayout );
  }


  double _getImgWidth() {
    return min( MediaQuery.of(context).size.width * 0.40, MAX_IMAGE_WIDTH );
  }


  Widget buildConstraints(Widget child) {
    return ConstrainedBox(
      constraints: BoxConstraints(
        maxHeight: FIXED_IMAGE_HEIGHT,
        minHeight: FIXED_IMAGE_HEIGHT,
        maxWidth: _getImgWidth()
      ),
      child: child,
    );
  }

  void _afterLayout(_) {
    _buttonOffsets = [];
    for (int i=0; i<widget.buttons.length; ++i) {
      if (widget.buttons[i].buttonKey.currentContext == null) {
        _buttonOffsets.add(Offset.zero);
        continue;
      }
      final RenderBox renderBox = widget.buttons[i].buttonKey.currentContext.findRenderObject();
      _buttonOffsets.add(
        renderBox.localToGlobal( Offset.zero, ancestor: _stackKey.currentContext.findRenderObject())
          .translate(0, renderBox.size.height/2 - 2 )
      );
    }

    setState(() {
      // update
    });
  }

  Widget _buildLineWithTransition(int i) {
    return FadeTransition(
      opacity: _opacityInAnimation,
      child: _buildLine(i)
    );
  }

  Widget _buildLine(int i) {
    if (currentImageIndex == -1 || widget.entries[currentImageIndex].buttonTargetX[i] == 0.0) {
      return Container(width: 0,height: 0);
    }

    Offset offset = _buttonOffsets.length  > 0 ? _buttonOffsets[i] : null;
    if (offset == null) {
      offset = Offset.zero;
    }

    double imgWidth = _getImgWidth();

    return AnimatedBuilder(
      animation: _inAnim,
      
      builder: (BuildContext context, Widget child) {
        
        double imgX = widget.entries[currentImageIndex].buttonTargetX[i] * imgWidth;
        double posX = max(0, _inAnim.value.dx * imgWidth + imgX);
        double width = max(0, offset.dx - posX);

        return Container(
          margin: EdgeInsets.only(left: posX, top: offset.dy),
          width: width,
          child: Row(
            
            children: <Widget>[
              Container(
                width: 6,
                height: 6,
                decoration: new BoxDecoration(
                  color: Colors.green, //Theme.of(context).secondaryHeaderColor,
                  shape: BoxShape.circle,
                  boxShadow: [
                    new BoxShadow(
                      color: Colors.blue[100],
                      offset: new Offset(0.0, 0.0),
                      blurRadius: 3.0,
                      spreadRadius: 3.0
                    )
                  ],
                )
              ),
              Expanded(child:Container(
                height: 2,
                color: Colors.grey[400]
              )),
            ]
          ),
         
        );
       
      },
    );
  }

  Widget _buildButton(int i) {
    if (currentImageIndex == -1 || widget.entries[currentImageIndex].buttonTargetX[i] == 0.0) {
      return Container(width: 0, height: 0);
    }
    List<Widget> children = [];
    if (null != widget.buttons[i].hint) {
      children.add(Text(widget.buttons[i].hint, style: scriptTextStyle()));
    }
    children.add(widget.buttons[i].button);

    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: children
    );
  }


  @override
  Widget build(BuildContext context) {

    List<Widget> children = [
      buildConstraints( FadeTransition(
        opacity: _opacityInAnimation,
        child: SlideTransition(
          position: _inAnim,
          child: currentImageIndex != -1 ? _getImage(currentImageIndex) : Container()
        )
      ) ),

      buildConstraints( FadeTransition(
        opacity: _opacityOutAnimation,
        child: SlideTransition(
          position: _outAnim,
          child: oldImageIndex != -1 ? _getImage(oldImageIndex) : Container()
        )
      ) )
    ];

    children.addAll(List<Widget>.generate( widget.buttons.length, _buildLineWithTransition));

    // FYI using positioned elements in Stack will not make that stack auto-grow 
    // so I used container positionized by margin

    children.add(
      Container(
        margin: EdgeInsets.only(left: _getImgWidth() + 20, right: 10 ),
        constraints: BoxConstraints(maxWidth: MAX_IMAGE_WIDTH, minHeight: FIXED_IMAGE_HEIGHT),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: List<Widget>.generate(widget.buttons.length, _buildButton)
        )
      )
    );

    return Stack(
      key: _stackKey,
      children: children,
    );
  }


}