// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'CityStore.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$CityStore on _CityStore, Store {
  final _$regionsAtom = Atom(name: '_CityStore.regions');

  @override
  List<Region> get regions {
    _$regionsAtom.context.enforceReadPolicy(_$regionsAtom);
    _$regionsAtom.reportObserved();
    return super.regions;
  }

  @override
  set regions(List<Region> value) {
    _$regionsAtom.context.conditionallyRunInAction(() {
      super.regions = value;
      _$regionsAtom.reportChanged();
    }, _$regionsAtom, name: '${_$regionsAtom.name}_set');
  }

  final _$fetchRegionsAsyncAction = AsyncAction('fetchRegions');

  @override
  Future<List<Region>> fetchRegions(BuildContext context, String languageCode) {
    return _$fetchRegionsAsyncAction
        .run(() => super.fetchRegions(context, languageCode));
  }

  @override
  String toString() {
    final string = 'regions: ${regions.toString()}';
    return '{$string}';
  }
}
