import 'dart:convert';

import 'package:ejot_ru/data/AppLocalizations.dart';
import 'package:ejot_ru/data/persistence.dart';
import 'package:ejot_ru/model/Material.dart';
import 'package:ejot_ru/ui/pages/MaterialSelectorPage.dart';
import 'package:flutter/widgets.dart';
import 'package:mobx/mobx.dart';
import 'package:flutter/services.dart' show rootBundle;

part 'MaterialStore.g.dart';

class MaterialStore extends _MaterialStore with _$MaterialStore {
  MaterialStore(AppLocalizations loc) : super(loc);
}

abstract class _MaterialStore with Store {
  _MaterialStore(this.loc);

  final AppLocalizations loc;

  @observable
  List<MaterialGroup> matInsulators = [];

  List<MaterialGroup> fixedMatInsulators = [];
  MaterialGroup customMatInsulators;

  @observable
  List<MaterialGroup> matCores = [];

  List<MaterialGroup> fixedMatCores = [];
  MaterialGroup customMatCores;

  String prefKeyByMatType(MatType matType) =>
      matType == MatType.INSULATION ? 'customMtlIso' : 'customMtlCore';

  Future<List<MaterialGroup>> _fetchMaterials(
      BuildContext context, List<MaterialGroup> dst, String fileName) async {
    if (dst.isEmpty) {
      int mtlGroupId = 0;
      int mtlIdCounter = 0;
      return rootBundle.loadString(fileName).then((groupString) {
        // debugging inside Futures may be a pain

        Iterable mtlIt = json.decode(groupString);
        dst.addAll(mtlIt.map((mtlJson) {
          var group =
              MaterialGroup.fromJson(mtlJson, mtlGroupId++, mtlIdCounter);
          mtlIdCounter += group.materials.length;
          return group;
        }));
        return dst;
      });
    } else {
      return Future.value(dst);
    }
  }

  Future<MaterialGroup> _fetchMaterialsFromPreferences(BuildContext context,
      String key, int mtlGroupId, int mtlIdCounter) async {
    Persistence persistence = Persistence();
    try {
      Map<String, dynamic> jgroup = await persistence.read(key);
      return MaterialGroup.fromJson(jgroup, mtlGroupId++, mtlIdCounter);
    } catch (e) {
      print(e);
      return Future.value(null);
    }
  }

  Future<void> _saveMaterialsToPreferences(
      String key, MaterialGroup mtlGroup) async {
    Persistence persistence = Persistence();
    try {
      await persistence.save(key, mtlGroup.toJson());
    } catch (e) {
      print(e);
    }
  }

  @action
  Future<List<MaterialGroup>> fetchMaterialsInsulator(
      BuildContext context, String languageCode) async {
    fixedMatInsulators = await _fetchMaterials(
        context, fixedMatInsulators, 'assets/mtliso_ru.json');
    if (null == customMatInsulators)
      customMatInsulators = await _fetchMaterialsFromPreferences(
          context,
          prefKeyByMatType(MatType.INSULATION),
          fixedMatInsulators.last.id + 1,
          fixedMatInsulators.last.materials.last.id + 1);
    if (null == customMatInsulators || customMatInsulators.materials.isEmpty) {
      matInsulators = fixedMatInsulators;
    } else {
      matInsulators = List<MaterialGroup>();
      matInsulators.addAll(fixedMatInsulators);
      matInsulators.add(customMatInsulators);
    }
    return matInsulators;
  }

  @action
  Future<List<MaterialGroup>> fetchMaterialsCores(
      BuildContext context, String languageCode) async {
    fixedMatCores =
        await _fetchMaterials(context, fixedMatCores, 'assets/mtlwall_ru.json');
    if (null == customMatCores)
      customMatCores = await _fetchMaterialsFromPreferences(
          context,
          prefKeyByMatType(MatType.OTHER),
          fixedMatCores.last.id + 1,
          fixedMatCores.last.materials.last.id + 1);
    if (null == customMatCores || customMatCores.materials.isEmpty) {
      matCores = fixedMatCores;
    } else {
      matCores = List<MaterialGroup>();
      matCores.addAll(fixedMatCores);
      matCores.add(customMatCores);
    }
    return matCores;
  }

  @action
  Future<Material> createCustomMaterial(MatType matType, String name,
      double lambdaA, double lambdaB, double rho) async {
    MaterialGroup dst;
    int mtlId;

    if (matType == MatType.INSULATION) {
      mtlId = matInsulators.last.materials.last.id + 1;
      if (null == customMatInsulators) {
        customMatInsulators = MaterialGroup(
            id: fixedMatInsulators.last.id + 1,
            materials: [],
            name: loc.getString('customMaterials'));
        matInsulators.add(customMatInsulators);
      }
      dst = customMatInsulators;
    } else {
      mtlId = matCores.last.materials.last.id + 1;
      if (null == customMatCores) {
        customMatCores = MaterialGroup(
            id: fixedMatCores.last.id + 1,
            materials: [],
            name: loc.getString('customMaterials'));
        matCores.add(customMatCores);
      }
      dst = customMatCores;
    }

    int anchorType = -1;
    if (rho != null) {
      if (rho > 1800)
        anchorType = 0;
      else if (rho > 1600)
        anchorType = 1;
      else if (rho > 1400)
        anchorType = 2;
      else if (rho > 1200)
        anchorType = 3;
      else if (rho > 400) anchorType = 4;
    }

    Material mtl = Material(
        id: mtlId,
        name: name,
        densityRange: rho == null ? [] : [rho],
        conductivity: [lambdaA, lambdaB],
        anchorType: anchorType);
    dst.materials.add(mtl);

    await _saveMaterialsToPreferences(prefKeyByMatType(matType), dst);

    return mtl;
  }
}
