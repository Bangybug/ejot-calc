import 'package:ejot_ru/ui/widgets/GroupedList.dart';
import 'package:ejot_ru/util/Tuple.dart';
import 'package:flutter/material.dart';

class FilteredGroupedList<T> extends StatefulWidget {
  FilteredGroupedList({
    Key key, 
    this.data,
    this.onItemSelected,
    this.noItemsLabel,
    this.loadedLabel,
    this.isLoading,
    this.onLongTap, 
    this.canCollapse = false,
  }): super(key: key);

  final GroupData<T> data;
  final String noItemsLabel;
  final String loadedLabel;
  final bool isLoading;

  // FYI cannot specify T, otherwise I get "(City) => void is not a subtype of (dynamic) => void".
  // It seems that the T has turned to dynamic somehow under the hood. 
  final ValueChanged<dynamic> onItemSelected; 
  final ValueChanged<dynamic> onLongTap; 
  final bool canCollapse;

  @override
  State<StatefulWidget> createState() {
    return FilteredGroupedListState<T>();
  }
}


class FilteredGroupedListState<T> extends State<FilteredGroupedList> {
  T selectedItem;
  String filterText;
  GroupData<T> filteredData;

  void clearSelection() {
    setState(() {
      selectedItem = null;
    });
  }

  void handleFilterTextChanged(String text) {
    if (filterText != text) {
      filterText = text;
      setState(() {
        if (text == "") {
          filteredData = widget.data;
        } else {
          filteredData = widget.data.filter(text);
          if (filteredData.hasSingleItem(text)) {
            if (null != widget.onItemSelected) { 
              widget.onItemSelected(filteredData.getItem(0, 0));
            }
          }
        }
      });
    }
  }

  T _handleItemSelected(Tuple2<int,int> itemId) {
    T item = filteredData.getItem(itemId.item1, itemId.item2);
    if (null != widget.onItemSelected) {
      widget.onItemSelected( item );
    }
    setState(() {
      this.selectedItem = item;
    });
    return item;
  }

  void _handleItemLongTapped(Tuple2<int,int> itemId) {
    T item = _handleItemSelected(itemId);
    if (null != widget.onLongTap) {
      widget.onLongTap( item );
    }
  }

  void _handleCollapseToggle(int index) {
    setState(() {
      filteredData.toggleCollapseGroup(index);
    });
  }

  @override
  Widget build(BuildContext context) {

    // FYI this is quite a nonintuitive operation, as the widget.data is controlled by mobx Observer outside and is read asynchronously 
    // starting with empty data, then it gets back with some data here.
    // At the same time, we have filteredData state controlled by this widget, it is started as null, but will populate from widget.data asap.
    // Technically, I should have populated it in initState and updated it in componentDidUpdate (called just before build). I decided
    // to save on initState and just populate filterData when it is null and if the widget.data is non empty.
    if (widget.data.getGroupCount() > 0) {
      filteredData ??= widget.data;
    }

    if (filteredData != null && filteredData.getGroupCount() > 0) {
      return GroupedList<T>(
        onItemSelected: _handleItemSelected,
        onItemLongTapped: _handleItemLongTapped,
        data: filteredData,
        selectedItem: selectedItem,
        onCollapseToggle: _handleCollapseToggle,
        canCollapse: widget.canCollapse,
      );
    } else {
      return Text(widget.isLoading ? widget.loadedLabel : widget.noItemsLabel);
    }
  }

}