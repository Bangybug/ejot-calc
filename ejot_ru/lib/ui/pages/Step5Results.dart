import 'dart:collection';
import 'package:ejot_ru/data/AppLocalizations.dart';
import 'package:ejot_ru/model/AppStore.dart';
import 'package:ejot_ru/ui/pages/BaseBlockEditorPage.dart';
import 'package:ejot_ru/ui/widgets/MyHeader.dart';
import 'package:ejot_ru/ui/widgets/cells/cellwidgets.dart';
import 'package:ejot_ru/util/Tuple.dart';
import 'package:ejot_ru/util/misc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:provider/provider.dart' as P;

import 'BasePage.dart';

class Step5Results extends StatefulWidget {
  final ValueChanged<Tuple2<int,bool>> handlePageChange;

  const Step5Results({Key key, this.handlePageChange}) : super(key: key);

  @override
  _Step5ResultsState createState() => _Step5ResultsState();
}

class _Step5ResultsState extends State<Step5Results> {
  AppStore _appStore;

  void didChangeDependencies() {
    super.didChangeDependencies(); 
    _appStore ??= P.Provider.of<AppStore>(context);
  }


  Widget _buildRowName(String text, {bool noMargin = true, bool needBottomBorder = true})  {
    final innerPadding = EdgeInsets.all(10);
    final headingStyle = TextStyle(fontWeight: FontWeight.bold);
    final bside = needBottomBorder ? null : BorderSide(color: Colors.grey, width: 1);

    return Container(
      child: Text(text, style: headingStyle),
      margin: noMargin ? null : EdgeInsets.only(top: 10),
      padding: innerPadding,
      decoration: BoxDecoration(
        border: needBottomBorder ? Border.all(color: Colors.grey, width: 1) : Border(left: bside, right: bside, top: bside)
      ),
    );
  }

  Widget _buildRowValues(dynamic value1, dynamic value2, {bool needTopBorder = false, bool isWarning = false, ValueToWidget widgetFactory}) {
    final bside = BorderSide(color: Colors.grey, width: 1);
    final innerPadding = EdgeInsets.all(10);
    final innerColumnDecorationStart = needTopBorder ? BoxDecoration( border: Border(left: bside, bottom: bside, right: bside, top: bside) ) :  BoxDecoration( border: Border(left: bside, bottom: bside, right: bside) );
    final innerColumnDecorationEnd = needTopBorder ? BoxDecoration( border: Border(bottom: bside, right: bside, top: bside) ) : BoxDecoration( border: Border(bottom: bside, right: bside) );

    if (null == widgetFactory) {
      widgetFactory = textWidget;
    }

    return Row( 
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Expanded(
          child: Container(
            child: Center(child: widgetFactory(value1, 0) ),
            padding: innerPadding,
            decoration: innerColumnDecorationStart
          )
        ),
        Expanded(
          child: Container(
            child: Center(child: widgetFactory(value2, 1, isWarning: isWarning) ),
            padding: innerPadding,
            decoration: innerColumnDecorationEnd
          )
        )
      ]
    );
  }

  Widget _buildRowValue(dynamic value1, {bool needTopBorder = false, bool isWarning = false}) {
    final bside = BorderSide(color: Colors.grey, width: 1);
    final innerPadding = EdgeInsets.all(10);
    final innerColumnDecorationStart = needTopBorder ? BoxDecoration( border: Border(left: bside, bottom: bside, right: bside, top: bside) ) :  BoxDecoration( border: Border(left: bside, bottom: bside, right: bside) );

    final TextStyle style = isWarning ? TextStyle(color: Colors.red, fontWeight: FontWeight.bold) : null;

    return Container(
      child: Center(child: value1 is Widget ? value1 : Text(value1, style: style) ),
      padding: innerPadding,
      decoration: innerColumnDecorationStart
    );
  }




  Widget _buildAnchorResultsNoComparison(BuildContext context) {
    final loc = AppLocalizations.of(context);
    final calc = _appStore.anchorCalc.cache;
    final calcOrdinary = calc[1];
    final calcBoundary = calc[0];

    List<Widget> children = [
      _buildRowName(loc.getString('pBreakForce') + ', ' + loc.getString('kN') ),
      _buildRowValue(doubleToString(calcOrdinary['breakForce']), isWarning: calcOrdinary['breakForce'] < calcOrdinary['fNorm']),
      _buildRowName(loc.getString('breakForceNorm') + ', ' + loc.getString('kN')),
      _buildRowValue(doubleToString(calcOrdinary['fNorm'])),
      _buildRowName(loc.getString('anchorCategory')),
      _buildRowValue(['A','B','C','D','E'][calcOrdinary['category']]),
      _buildRowName(loc.getString('numberAnchorsPerM2') + ', ' + loc.getString('pcs/m2')),
      _buildRowValues(loc.getString('sftkOrdinaryZone'), loc.getString('sftkBoundaryZone')),
    ];

    int heightZoneCount = _appStore.anchorCalc.heightZoneCount();

    for (int i=0; i<heightZoneCount; ++i) {
      if (heightZoneCount > 1)
        children.add(_buildHeightZone(context, i));
      children.add(
        _buildRowValues(doubleToString(calcOrdinary['nm'][i]), doubleToString(calcBoundary['nm'][i]))
      );
    }
    
    children.addAll([
      _buildRowName(loc.getString('peakWindLoad') + ', ' + loc.getString('pressureUnits')),
      _buildRowValues(loc.getString('sftkOrdinaryZone'), loc.getString('sftkBoundaryZone')),
    ]);

    for (int i=0; i<heightZoneCount; ++i) {
      if (heightZoneCount > 1)
        children.add(_buildHeightZone(context, i));
      children.add(
        _buildRowValues(doubleToString(calcOrdinary['wp'][i][0]) + ' / ' + doubleToString(calcOrdinary['wp'][i][1]), 
          doubleToString(calcBoundary['wp'][i][0]) + ' / ' + doubleToString(calcBoundary['wp'][i][1]) ),
      );
    }


    return Center(
      child: Container(
        constraints: BoxConstraints(maxWidth: 400),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: children
        )
      )
    );
  }

  Widget _buildHeightZone(BuildContext context, int index) {
    final loc = AppLocalizations.of(context);
    if (index == 0)
      return _buildRowValue(loc.getString('heightBuilding') + ' ' + loc.getString('upto') + ' 16 '+ loc.getString('m'));
    else if (index == 1)
      return _buildRowValue(loc.getString('heightBuilding') + ' ' + loc.getString('from') + ' 16 '+ loc.getString('upto') + ' 40 ' + loc.getString('m'));
    else if (index == 2)
      return _buildRowValue(loc.getString('heightBuilding') + ' ' + loc.getString('above') + ' 40 ' + loc.getString('m'));
    return null;
  }


  Widget _buildAnchorResultsComparison(BuildContext context) {
    final loc = AppLocalizations.of(context);
    final calc = _appStore.anchorCalc.cache;
    final calcOrdinaryEjot = calc[1];
    final calcBoundaryEjot = calc[0];
    final calcOrdinaryOther = calc[3];
    final calcBoundaryOther = calc[2];

    List<Widget> children =  [
      Center(
        child: Text(loc.getString('sftkOrdinaryZone')),
      ),
      SizedBox(height: 10) 
    ];

    int heightZoneCount = _appStore.anchorCalc.heightZoneCount();

    children.add( _buildRowName(loc.getString('numberAnchorsPerM2') + ', ' + loc.getString('pcs/m2')) );
    for (int i=0; i<heightZoneCount; ++i) {
      if (heightZoneCount > 1)
        children.add(_buildHeightZone(context, i));
      children.addAll([
        _buildRowValues(loc.getString('ejotHD'), loc.getString('otherAnchor')),
        _buildRowValues(doubleToString(calcOrdinaryEjot['nm'][i]), doubleToString(calcOrdinaryOther['nm'][i])),
      ]);
    }

    children.add( _buildRowName(loc.getString('peakWindLoad') + ', ' + loc.getString('pressureUnits')) );
    for (int i=0; i<heightZoneCount; ++i) {
      if (heightZoneCount > 1)
        children.add(_buildHeightZone(context, i));
      children.addAll([ 
        _buildRowValue(doubleToString(calcOrdinaryEjot['wp'][i][0]) + ' / ' + doubleToString(calcOrdinaryEjot['wp'][i][1]))
      ]);
    }

    final TextStyle errStyle = TextStyle(color: Colors.red, fontWeight: FontWeight.bold);

    children.addAll([
      _buildRowName(loc.getString('breakForceNorm') + ', ' + loc.getString('kN')),
      _buildRowValue(doubleToString(calcOrdinaryEjot['fNorm'])),
      _buildRowName(loc.getString('pBreakForce') + ', ' + loc.getString('kN')),
      _buildRowValues(loc.getString('ejotHD'), loc.getString('otherAnchor')),
      _buildRowValues(
        Text( doubleToString(calcOrdinaryEjot['breakForce']), style: calcOrdinaryEjot['breakForce'] < calcOrdinaryEjot['fNorm'] ? errStyle : null ),
        Text( doubleToString(calcOrdinaryOther['breakForce']), style: calcOrdinaryOther['breakForce'] < calcOrdinaryEjot['fNorm'] ? errStyle : null )
      ),
      _buildRowName(loc.getString('anchorCategory')),
      _buildRowValue(['A','B','C','D','E'][calcOrdinaryEjot['category']]),

      SizedBox(height: 30),
      Center(
        child: Text(loc.getString('sftkBoundaryZone')),
      ),
      SizedBox(height: 10),
    ]);

    children.add( _buildRowName(loc.getString('numberAnchorsPerM2') + ', ' + loc.getString('pcs/m2')) );
    for (int i=0; i<heightZoneCount; ++i) {
      if (heightZoneCount > 1)
        children.add(_buildHeightZone(context, i));
      children.addAll([
        _buildRowValues(loc.getString('ejotHD'), loc.getString('otherAnchor')),
        _buildRowValues(doubleToString(calcBoundaryEjot['nm'][i]), doubleToString(calcBoundaryOther['nm'][i])),
      ]);      
    }

    children.add( _buildRowName(loc.getString('peakWindLoad') + ', ' + loc.getString('pressureUnits')) );
    for (int i=0; i<heightZoneCount; ++i) {
      if (heightZoneCount > 1)
        children.add(_buildHeightZone(context, i));
      children.addAll([ 
        _buildRowValue(doubleToString(calcBoundaryEjot['wp'][i][0]) + ' / ' + doubleToString(calcBoundaryEjot['wp'][i][1]))
      ]);
    }    

    children.addAll([ 
      _buildRowName(loc.getString('breakForceNorm') + ', ' + loc.getString('kN')),
      _buildRowValue(doubleToString(calcBoundaryEjot['fNorm'])),
      _buildRowName(loc.getString('pBreakForce') + ', ' + loc.getString('kN')),
      _buildRowValues(loc.getString('ejotHD'), loc.getString('otherAnchor')),
      _buildRowValues(
        Text( doubleToString(calcBoundaryEjot['breakForce']), style: calcBoundaryEjot['breakForce'] < calcBoundaryEjot['fNorm'] ? errStyle : null ),
        Text( doubleToString(calcBoundaryOther['breakForce']), style: calcBoundaryOther['breakForce'] < calcBoundaryEjot['fNorm'] ? errStyle : null )
      ),
      _buildRowName(loc.getString('anchorCategory')),
      _buildRowValue(['A','B','C','D','E'][calcBoundaryEjot['category']]),
    ]);

    return Center(
      child: Container(
        constraints: BoxConstraints(maxWidth: 400),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: children
        )
      )
    );
  }

  Widget _buildAnchorResults(BuildContext context) {
    if (_appStore.anchorCalc.hasComparison()) {
      return _buildAnchorResultsComparison(context);
    } else {
      return _buildAnchorResultsNoComparison(context);
    }
  }

  Widget _buildThermicResults(BuildContext context) {
    if (_appStore.thermicCalc.hasComparison()) {
      return _buildThermicResultsComparison(context);
    } else {
      return _buildThermicResultsNoComparison(context);
    }
  }

  Widget _buildEcononmicResults(BuildContext context) {
    final loc = AppLocalizations.of(context);

    final List<Widget> children = [
      Center(
        child: Text(loc.getString('economicResults'), style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold) )
      ),
      SizedBox(height: 20),
    ];

    double total = 0;

    if (_appStore.ecoCalc.anchorEconomyMoney != null) {
      children.add(_buildRowName(loc.getString('economyAnchor')));
      children.add(_buildRowValues(loc.getString('ejotHD'), 
        costByQuantityTotal( _appStore.economyCostAnchor, _appStore.ecoCalc.ejotQty, loc.getString('pcs')) ));
      children.add(_buildRowValues(loc.getString('otherAnchor'), 
        costByQuantityTotal( _appStore.economyCostOtherAnchor, _appStore.ecoCalc.otherQty, loc.getString('pcs')) ));
      children.add(_buildRowValues(loc.getString('economy'), 
        roubleWidget(_appStore.ecoCalc.anchorEconomyMoney) ));
      total += _appStore.ecoCalc.anchorEconomyMoney;
    }

    if (_appStore.ecoCalc.energyCostEconomyMoney != null) {
      children.add(_buildRowName(loc.getString('economyHeating')));
      children.add(_buildRowValues(loc.getString('ejotHD'), 
        costByQuantityTotal( _appStore.economyCostEnergy, _appStore.thermicReport.ejotAnnualLoss, loc.getString('Gcal/year')) ));
      children.add(_buildRowValues(loc.getString('otherAnchor'), 
        costByQuantityTotal( _appStore.economyCostEnergy, _appStore.thermicReport.otherAnnualLoss, loc.getString('Gcal/year')) ));
      children.add(_buildRowValues(loc.getString('economy'), 
        roubleWidget(_appStore.ecoCalc.energyCostEconomyMoney) ));
      
      total += _appStore.ecoCalc.energyCostEconomyMoney;
    }

    if (_appStore.ecoCalc.insulatorCostEconomy != null) {
      children.add(_buildRowName(loc.getString('economyInsulator')));
      children.add(_buildRowValues(loc.getString('ejotHD'), 
        costByQuantityTotal( _appStore.economyCostInsulator, _appStore.ecoCalc.insulatorVolumeEjot, loc.getString('m3')) ));
      children.add(_buildRowValues(loc.getString('otherAnchor'), 
        costByQuantityTotal( _appStore.economyCostInsulator, _appStore.ecoCalc.insulatorVolumeOther, loc.getString('m3')) ));
      children.add(_buildRowValues(loc.getString('economy'), 
        roubleWidget(_appStore.ecoCalc.insulatorCostEconomy) ));

      total += _appStore.ecoCalc.insulatorCostEconomy;
    } 

    children.add(_buildRowValues(loc.getString('total'),  roubleWidget(total) ));

    children.add(SizedBox(height: 70));

    return Center(
      child: Container(
        constraints: BoxConstraints(maxWidth: 400),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: children
        )
      )
    ); 
  }

  Widget _buildThermicResultsComparison(BuildContext context) {
    final rep = _appStore.thermicReport;
    final loc = AppLocalizations.of(context);

    final children =  [
      _buildRowName(loc.getString('unitHeatLossAnchor') + ', ' + loc.getString('W/m2C')),
      _buildRowValues(loc.getString('ejotHD'), roundDouble(rep.rows[1].unitLoss, 3).toString() ),
      _buildRowValues(loc.getString('otherAnchor'), roundDouble(rep.otherAnchor.unitLoss, 3).toString() ),

      _buildRowName(loc.getString('rateAnchorLoss') + ', %'),
      _buildRowValues(loc.getString('ejotHD'), roundDouble(100/(rep.sumLossWithoutAnchorEjot/rep.ejotAnchorLoss + 1), 1).toString() + ' %'),
      _buildRowValues(loc.getString('otherAnchor'), roundDouble(100/(rep.sumLossWithoutAnchorOther/rep.otherAnchorLoss + 1), 1).toString() + ' %' ),

      _buildRowName(loc.getString('estimatedResistance') + ', ' + loc.getString('m2C/W')),
      _buildRowValues(loc.getString('ejotHD'), roundDouble(1/(rep.sumLossWithoutAnchorEjot + rep.ejotAnchorLoss), 4).toString() ),
      _buildRowValues(loc.getString('otherAnchor'), roundDouble(1/(rep.sumLossWithoutAnchorOther + rep.otherAnchorLoss), 4).toString() ),

      _buildRowName(loc.getString('rNorm') + ', ' + loc.getString('m2C/W')),
      _buildRowValue(roundDouble(rep.r0norm, 4).toString()),
    ];

    if (_appStore.facade == FacadeType.NFS && _appStore.insulatorFixedWidthNFS != null && _appStore.insulatorFixedWidthNFS > 0) {
      children.addAll([
        _buildRowName(loc.getString('isoWidthFixed')),
        _buildRowValue( _appStore.insulatorFixedWidthNFS.toString() + ' ' + loc.getString('mm') ),
      ]);
    }

    if (_appStore.shouldCalculateInsulatorWidth) {
      children.addAll([
        _buildRowName(loc.getString(_appStore.hasTwoInsulators() ? 'isoWidthAdjust' : 'isoWidth')),
        _buildRowValues(loc.getString('ejotHD'), _appStore.thermicCalc.calculatedInsulatorWidthEjot.toString() + ' ' + loc.getString('mm')),
        _buildRowValues(loc.getString('otherAnchor'), _appStore.thermicCalc.calculatedInsulatorWidthOther.toString() + ' ' + loc.getString('mm') ),
      ]);
    } else {
      children.addAll([
        _buildRowName(loc.getString(_appStore.hasTwoInsulators() ? 'isoWidthAdjust' : 'isoWidth')),
        _buildRowValues(loc.getString('ejotHD'), _appStore.insulatorWidth.toString() + ' ' + loc.getString('mm')),
        _buildRowValues(loc.getString('otherAnchor'), _appStore.insulatorWidth.toString() + ' ' + loc.getString('mm') ),
      ]);
    }

    children.addAll([
      _buildRowName(loc.getString('annualLoss') + ', ' + loc.getString('Gcal/year') + 
        (_appStore.shouldCalculateInsulatorWidth ? ('\n'+ loc.getString('withR0norm')) : '') ),
      _buildRowValue('S = '+roundDouble(rep.facadeSquareWithoutHoles,2).toString() + ' ' + loc.getString('m2')),
      _buildRowValues(loc.getString('ejotHD'), rep.ejotAnnualLoss.toString() ),
      _buildRowValues(loc.getString('otherAnchor'), rep.otherAnnualLoss.toString() ),
    ]);


    return Center(
      child: Container(
        constraints: BoxConstraints(maxWidth: 400),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: children
        )
      )
    );
  }


  Widget _buildThermicResultsNoComparison(BuildContext context) {
    final rep = _appStore.thermicReport;
    final loc = AppLocalizations.of(context); 

    final children = [
      _buildRowName(loc.getString('unitHeatLossAnchor') + ', ' + loc.getString('W/m2C')),
      _buildRowValue( roundDouble(rep.rows[1].unitLoss, 3).toString() ),

      _buildRowName(loc.getString('rateAnchorLoss') + ', %'),
      _buildRowValue( roundDouble(100/(rep.sumLossWithoutAnchorEjot/rep.ejotAnchorLoss + 1), 2).toString() + ' %' ),
      
      _buildRowName(loc.getString('estimatedResistance') + ', ' + loc.getString('m2C/W')),
      _buildRowValue(  roundDouble(1/(rep.sumLossWithoutAnchorEjot + rep.ejotAnchorLoss), 4).toString() ),

      _buildRowName(loc.getString('rNorm') + ', ' + loc.getString('m2C/W')),
      _buildRowValue(roundDouble(rep.r0norm, 5).toString()),
    ];

    if (_appStore.facade == FacadeType.NFS && _appStore.insulatorFixedWidthNFS != null && _appStore.insulatorFixedWidthNFS > 0) {
      children.addAll([
        _buildRowName(loc.getString('isoWidthFixed')),
        _buildRowValue( _appStore.insulatorFixedWidthNFS.toString() + ' ' + loc.getString('mm') ),
      ]);
    }

    if (_appStore.shouldCalculateInsulatorWidth) {
      children.addAll([
        _buildRowName(loc.getString(_appStore.hasTwoInsulators() ? 'isoWidthAdjust' : 'isoWidth')),
        _buildRowValue(_appStore.thermicCalc.calculatedInsulatorWidthEjot.toString() + ' ' + loc.getString('mm'))
      ]);
    } else {
      children.addAll([
        _buildRowName(loc.getString(_appStore.hasTwoInsulators() ? 'isoWidthAdjust' : 'isoWidth')),
        _buildRowValue(_appStore.insulatorWidth.toString() + ' ' + loc.getString('mm'))
      ]);
    }

    children.addAll([
      _buildRowName(loc.getString('annualLoss') + ', ' + loc.getString('Gcal/year')),
      _buildRowValue('S = '+roundDouble(rep.facadeSquareWithoutHoles,2).toString() + ' ' + loc.getString('m2')),
      _buildRowValue(rep.ejotAnnualLoss.toString() ),
    ]);

    return Center(
      child: Container(
        constraints: BoxConstraints(maxWidth: 400),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: children
        )
      )
    );
  }


  void _onFixError(String errCode) async {
    if (errCode == 'pBreakForceEjot' || errCode == 'pAvgPerMeterSq' || errCode == 'ejotAnchorPerM2Err') {
      await Navigator.pushNamed(context, '/blockEdit', arguments: BlockEditorArguments(_appStore, _appStore.blockInsulationBinding));
      setState(() {
        _appStore.recalculate();
      });
    } 
    else if (errCode == 'buildingType' || errCode == 'selectCity') {
      widget.handlePageChange(Tuple2(0,true));
    }
    else {
      widget.handlePageChange(Tuple2(1,true));
    }

  }


  Widget _buildErrorResults(BuildContext context, LinkedHashSet<String> errCodesSet) {
    var loc = AppLocalizations.of(context);
    List<String> errCodes = errCodesSet.toList();
    List<Widget> children = [
      Divider(height: 1, color: Colors.red),
      Container(
        color: Colors.yellow[200],
        child: ListTile(
          onTap: () => _onFixError(null),
          trailing: Icon(Icons.error_outline, color: Colors.red),
          title: Text(loc.getString('inputErrors')),
          subtitle: Text(loc.getString('pleaseFixInputErrors')),
        )
      ),
      Divider(height: 1, color: Colors.red)
    ];
    children.addAll(List.generate(errCodes.length*2, (i) {
      if (i % 2 == 1) {
        return Divider(height: 1, color: Colors.grey);
      } else {
        final String errCode = errCodes[i ~/2];
        return ListTile( 
          title: Text(loc.getString(errCode)),
          onTap: () => _onFixError(errCode),
        );
      }
    }));

    return Column(
      children: children
    );
  }


  Widget _buildResults(BuildContext context) {
    var loc = AppLocalizations.of(context);

    if (_appStore.anchorCalc == null) 
      _appStore.recalculate();

    List<Widget> children = [
      Divider(height: 1, color: Colors.grey),
      SizedBox(height: 20),
      Center(        
        child: Builder(
          builder: (_) {
            return Text( _appStore.facade == FacadeType.SFTK ? loc.getString('resultsSftk') : loc.getString('resultsNfs'), style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold) );
          }
        ) 
      ),
      SizedBox(height: 20),
      Builder(
        builder: (_) {
          if (_appStore.lastUpdatedCount > -1) {
            if (_appStore.facade == FacadeType.SFTK) {
              if (_appStore.errCodesAnchor.isEmpty) {
                return _buildAnchorResults(context);
              } else {
                return _buildErrorResults(context, _appStore.errCodesAnchor);
              }
            }
          }
          return Container();
        }
      ), 
      SizedBox(height: 40),
      Center(        
        child: Text(loc.getString('thermicResults'), style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold) )
      ),
      SizedBox(height: 20),
      Builder(
        builder: (context) {
          if (_appStore.lastUpdatedCount > -1) {
            if (_appStore.errCodesThermic.isEmpty) {
              return _buildThermicResults(context);
            } else {
              return _buildErrorResults(context, _appStore.errCodesThermic);
            }
          }
          // should not get here
          return Container();
        }
      ), 
      SizedBox(height: 40),
      Builder(
        builder: (context) {
          if (_appStore.lastUpdatedCount > -1) {
            if (_appStore.ecoCalc.canCalculate()) {
              return _buildEcononmicResults(context);
            }
          }
          return Container();
        }
      )
    ];

    return ListView(children: children);
  }


  Widget _buildPage(BuildContext context) {
    return BasePage(
      headerContent: MyHeader(pageIndex: 4),
      mainContent: Scrollbar(  
        child: Observer( 
          builder: (context) { 
            if (_appStore.lastCalculatedCount > -1)
              return _buildResults(context);
            else
              return Container();
          }
        )
      )
    );
  }

  @override
  Widget build(BuildContext context) {
    return _buildPage(context);
  }
}


