import 'package:ejot_ru/util/misc.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  test('Counter value should be incremented', () {
    final table = [
      [null, 0.0, 1.0],
      [ 0.0, 0.0, 1.0],
      [ 1.0, 1.0, 1.0]
    ];
    double d = lerpZxy(0.5, 0.5, table);
    expect(d, 0.75);
  });
}
