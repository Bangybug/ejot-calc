import 'package:ejot_ru/ui/widgets/GroupedList.dart';
import 'package:geolocator/geolocator.dart';
import 'WindZone.dart';
import 'dart:math' as math;

class Region {
  final int id;
  final List<City> cities;
  final String name;

  Region({this.id, this.cities, this.name});

  factory Region.fromJson(Map<String, dynamic> json) => Region(
      id: json["id"],
      name: json["name"],
      cities: List<City>.from(
          (json["cities"] as List).map((cityJson) => City.fromJson(cityJson))));
}

class City {
  final int id;
  final double lat;
  final double lng;
  final String name;
  final WindZone windZone;
  final int zone;
  final List<double> values;

  City(
      {this.lat,
      this.lng,
      this.id,
      this.name,
      this.values,
      this.zone,
      this.windZone});

  factory City.fromJson(Map<String, dynamic> json) => City(
      id: json["id"],
      name: json["name"],
      lat: json["lat"],
      lng: json["lng"],
      zone: json['zone'],
      windZone: (json.containsKey('windZone') && json['windZone'] != -1)
          ? windZones[json['windZone']]
          : null,
      values: (json["values"] as List<dynamic>)
          .map((e) {
            if (e is int)
              return e.toDouble();
            else
              return e;
          })
          .toList()
          .cast<double>());

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'lat': lat,
        'lng': lng,
        'values': values,
        'zone': zone,
        'windZone': windZone == null ? -1 : windZones.indexOf(windZone)
      };

  String toString() {
    return 'City: $name';
  }
}

class RegionData extends GroupData<City> {
  RegionData({this.regions});

  List<Region> regions;

  @override
  GroupData filter(String filter) {
    String upFilter = filter.toUpperCase();

    List<Region> filtered = new List<Region>();

    regions.forEach((region) {
      List<City> citiesFiltered = region.cities
          .where((city) => city.name.toUpperCase().contains(upFilter))
          .toList();
      if (citiesFiltered.isNotEmpty) {
        filtered.add(
            Region(cities: citiesFiltered, id: region.id, name: region.name));
      }
    });

    return RegionData(regions: filtered);
  }

  @override
  int getGroupCount() {
    //print("regs "+regions.length.toString());
    return regions.length;
  }

  @override
  int getGroupItemCount(int groupIndex) {
    return regions[groupIndex].cities.length;
  }

  @override
  String getGroupItemLabel(int groupIndex, int itemIndex) {
    return regions[groupIndex].cities[itemIndex].name;
  }

  @override
  String getGroupName(int groupIndex) {
    return regions[groupIndex].name;
  }

  @override
  City getItem(int groupIndex, int itemIndex) {
    return regions[groupIndex].cities[itemIndex];
  }
}

double _distance(double lat, double lng, double lat1, double lng1) {
  var R = 6371e3; // metres
  var df = (lat1 - lat);
  var dl = (lng1 - lng);
  var a = math.sin(df / 2) * math.sin(df / 2) +
      math.cos(lat) * math.cos(lat1) * math.sin(dl / 2) * math.sin(dl / 2);
  var c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a));
  return R * c;
}

dynamic _handleError(dynamic err) {
  return null;
}

Future<City> geolocate(List<Region> inRegions) async {
  // FYI Geolocator.checkGeolocationPermissionStatus checks permissions that are already available (if any), but is not asking them,
  // so it's not really useful for implementing exceptionless code
  // getLastKnownPosition actually asks permissions and fails when none given
  // getLastKnownPosition on iOS simulator returns null
  try {
    return Geolocator()
        .getLastKnownPosition(desiredAccuracy: LocationAccuracy.lowest)
        .then((Position position) {
      if (null == position) return null;

      var latR = (math.pi * position.latitude) / 180;
      var lngR = (math.pi * position.longitude) / 180;
      double minMeasure;
      City ret;

      for (var i = 0; i < inRegions.length; ++i) {
        var region = inRegions[i];
        var cities = region.cities;
        for (var j = 0; j < cities.length; ++j) {
          var jCity = cities[j];
          if (null == jCity.lat || null == jCity.lng) continue;
          var d = _distance(latR, lngR, (math.pi * jCity.lat) / 180,
              (math.pi * jCity.lng) / 180);
          if (minMeasure == null || minMeasure > d) {
            minMeasure = d;
            ret = jCity;
          }
        }
      }
      return ret;
    }).catchError(_handleError);
  } catch (_) {
    return null;
  }
}
