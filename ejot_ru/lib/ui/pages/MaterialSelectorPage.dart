import 'package:ejot_ru/data/AppLocalizations.dart';
import 'package:ejot_ru/model/Material.dart' as Mtl; // FYI this is how I resolve ambiguity in Dart
import 'package:ejot_ru/model/MaterialStore.dart';
import 'package:ejot_ru/ui/pages/BasePage.dart';
import 'package:ejot_ru/ui/widgets/FilteredGroupedList.dart';
import 'package:ejot_ru/ui/widgets/GroupedList.dart';
import 'package:ejot_ru/ui/widgets/MyTextField.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

enum MatType {
  INSULATION,
  OTHER
}

// FYI since MaterialSelectorPage is displayed in its dedicated screen, I cannot use its context to access dependencies
// from other contexts, so I will pass them explicitly via MaterialSelectArguments
class MaterialSelectArguments {
  final MaterialStore mtlStore;

  MaterialSelectArguments(this.mtlStore);
}

class MaterialSelectResult {
  final Mtl.Material material;
  final MatType matType;

  MaterialSelectResult(this.material, this.matType);
}

class MaterialCreateArguments {
  final MaterialStore mtlStore;
  final MatType matType;

  MaterialCreateArguments(this.mtlStore, this.matType);
}

class MaterialCreateResult {
  final Mtl.Material material;
  final MatType matType;

  MaterialCreateResult(this.material, this.matType);
}


class MaterialSelectorPage extends StatefulWidget {

  MaterialSelectorPage({Key key, this.matType}): super(key: key);

  final MatType matType;

  @override
  _MaterialSelectorPageState createState() => _MaterialSelectorPageState();
}

class _MaterialSelectorPageState extends State<MaterialSelectorPage> {

  final TextEditingController _searchController = TextEditingController();
  final GlobalKey<FilteredGroupedListState<Mtl.Material>> _materialListKey = new GlobalKey<FilteredGroupedListState<Mtl.Material>>();
  Mtl.Material selectedMaterial;

  Widget _displayDeleteTextIcon() {
    if (_searchController.text.length > 0) {
      return IconButton(
        icon: Icon(Icons.close),
        color: Colors.grey,
        onPressed: () {
          FocusScope.of(context).unfocus();
          WidgetsBinding.instance.addPostFrameCallback( (_) => _searchController.clear());
         
          setState(() {
            _materialListKey.currentState.handleFilterTextChanged("");
          });
        },
      );
    } else {
      return null;
    }
  }

  Widget _buildTextfield(BuildContext context) {
    var loc = AppLocalizations.of(context);

    return MyTextField(
      prefixIconNoPadding: false,
      prefixIcon: Icon(
        Icons.search,
        size: 26.0,
        color: Colors.grey,
      ),
      suffixIcon: _displayDeleteTextIcon(),
      hintText: loc.getString('searchMaterial'),
      controller: _searchController,
      onChanged: (text) {
        setState(() { 
          _materialListKey.currentState.handleFilterTextChanged(text);
        });
      }
    );
  }


  void handleMaterialPreSelect(dynamic material) {
    setState(() {
      selectedMaterial = material;
      WidgetsBinding.instance.addPostFrameCallback( (_) => _searchController.text = material.name ); 
    });
  }

  // https://flutter.dev/docs/get-started/flutter-for/android-devs
  // FYI this is how I can return from this screen with some results, on the receiving end there must be await on push
  void handleMaterialSelect(dynamic material) {
    Navigator.of(context).pop<MaterialSelectResult>( MaterialSelectResult(material, widget.matType) );
  }

  void handleMaterialCreate() async {
    final MaterialSelectArguments args = ModalRoute.of(context).settings.arguments;   
    var mtlStore = args.mtlStore;
    var ret = await Navigator.pushNamed(context, '/materialCreate', arguments: MaterialCreateArguments(mtlStore, widget.matType));
    if (ret == false) {
      Navigator.pop(context, false);
    } else {
      handleMaterialSelect( (ret as MaterialCreateResult).material );
    }
  }


  Widget _buildMaterialList(BuildContext context) {
    var loc = AppLocalizations.of(context);

    final MaterialSelectArguments args = ModalRoute.of(context).settings.arguments;
   
    var mtlStore = args.mtlStore;
    if (widget.matType == MatType.INSULATION) {
      if (mtlStore.matInsulators.isEmpty)
        mtlStore.fetchMaterialsInsulator(context, loc.locale.languageCode);
    } else {
      if (mtlStore.matCores.isEmpty)
        mtlStore.fetchMaterialsCores(context, loc.locale.languageCode);
    }

    return Scrollbar( 
      child: Observer(
        builder: (context) => new FilteredGroupedList<Mtl.Material>(
          key: _materialListKey,
          canCollapse: true,
          noItemsLabel: loc.getString('materialNotFound'),
          loadedLabel: loc.getString('loading'),
          isLoading: (widget.matType == MatType.INSULATION ? mtlStore.matInsulators : mtlStore.matCores).isEmpty,
          data: MaterialData( 
            groups: widget.matType == MatType.INSULATION ? mtlStore.matInsulators : mtlStore.matCores,
            kgm3: loc.getString('kg/m3')),
          onItemSelected: handleMaterialPreSelect,
          onLongTap: handleMaterialSelect,
        )
      )
    );
  }


  Widget _buildHeader(BuildContext context) {
    return _buildTextfield(context);
  }


  Widget _buildBody(BuildContext context) {
    return BasePage(
      headerContent: _buildHeader(context),
      mainContent: _buildMaterialList(context)
    );
  }


  Widget _buildFab(BuildContext context) {
    bool isSelectedMaterial = (selectedMaterial != null);
    var loc = AppLocalizations.of(context);
    return FloatingActionButton.extended(
      onPressed: () {
        if (isSelectedMaterial)
          handleMaterialSelect(selectedMaterial);
        else 
          handleMaterialCreate();
      },
      label: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [Text( isSelectedMaterial ? loc.getString('selectThisMaterial') : loc.getString('createMaterial'))]
      ),
      icon: Icon(isSelectedMaterial ? Icons.check : Icons.add),
      backgroundColor: Colors.green
    );
  }


  @override
  Widget build(BuildContext context) {
    var loc = AppLocalizations.of(context);
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: true,
        //`true` if you want Flutter to automatically add Back Button when needed,
        //or `false` if you want to force your own back button every where
        title: Text(loc.getString('selectMaterial')),
        leading: IconButton(icon:Icon(Icons.arrow_back),
          onPressed:() => Navigator.pop(context, false),
        )
      ),
      body: GestureDetector( 
        onTap: () => FocusScope.of(context).unfocus(),
        child: _buildBody(context)
      ),

      floatingActionButton: _buildFab(context),
    );
  }
}




class MaterialData extends GroupData<Mtl.Material> {
  MaterialData({this.groups, this.kgm3});
  
  List<Mtl.MaterialGroup> groups;
  String kgm3;

  @override
  GroupData filter(String filter) {
    String upFilter = filter.toUpperCase();

    List <Mtl.MaterialGroup> filtered = new List <Mtl.MaterialGroup>();

    groups.forEach(
      (group) {
        List<Mtl.Material> mtlFiltered = group.materials.where(
          (mtl) => mtl.name.toUpperCase().contains(upFilter)
        ).toList();
        if (mtlFiltered.isNotEmpty) {
          filtered.add( Mtl.MaterialGroup(materials: mtlFiltered, id: group.id, name: group.name));
        }
      }
    );

    return MaterialData(groups: filtered, kgm3: kgm3);
  }

  @override
  int getGroupCount() {
    return groups.length;
  }

  @override
  int getGroupItemCount(int groupIndex) {
    return groups[groupIndex].materials.length;
  }

  @override
  String getGroupItemLabel(int groupIndex, int itemIndex) {
    return groups[groupIndex].materials[itemIndex].name;
  }

  String _densityRangeToString(List<dynamic> range) {
    switch (range.length) {
      case 1: 
        return ', ρ = ' + range[0].toString() + ' ' + kgm3;
      case 2: 
        return ', ρ = ' + range[0].toString() + ' - ' + range[1].toString() + ' ' + kgm3;
      default:
        return '';
    }
  }


  @override
  String getGroupItemSubtitle(int groupIndex, int itemIndex) {
    return 'λ = '+ groups[groupIndex].materials[itemIndex].conductivity[0].toString() + ' / ' + groups[groupIndex].materials[itemIndex].conductivity[1].toString()
     + _densityRangeToString(groups[groupIndex].materials[itemIndex].densityRange);
  }

  @override
  String getGroupName(int groupIndex) {
    return groups[groupIndex].name;
  }

  @override
  Mtl.Material getItem(int groupIndex, int itemIndex) {
    return groups[groupIndex].materials[itemIndex];
  }

  @override
  bool isGroupCollapsed(int groupIndex) {
    return null == collapsed[groupIndex] ? true : collapsed[groupIndex];
  }
}