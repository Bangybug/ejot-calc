import 'package:ejot_ru/data/AppLocalizations.dart';
import 'package:ejot_ru/model/Block.dart';
import 'package:ejot_ru/ui/widgets/Dropdown.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'BaseBlockEditorPage.dart';

class BlockEditorPage extends StatefulWidget {
  BlockEditorPage(this.args) : super();
  final BlockEditorArguments args;

  @override
  State createState() => _BlockEditorPageState(args);
}

class _BlockEditorPageState extends BaseBlockPageState {
  _BlockEditorPageState(BlockEditorArguments args) : super(args);
}

class BlockAlBindingPage extends StatefulWidget {
  BlockAlBindingPage(this.args) : super();
  final BlockEditorArguments args;

  @override
  State createState() => _BlockALBindingPageState(args);
}

class _BlockALBindingPageState extends BaseBlockPageState {
  _BlockALBindingPageState(BlockEditorArguments args) : super(args);

  final GlobalKey<DropdownState> _sizeKey = GlobalKey<DropdownState>();

  List<DropdownMenuItem> getDropdownItems(
      BuildContext context, Block block, BlockParams instance, Param param) {
    if (param.name == 'pBracketPredefinedSize' &&
        instance.data['pBracketType'].value == BracketType.halfRect) {
      var loc = AppLocalizations.of(context);
      if (null != param.items) {
        return List.generate(
            param.items.length - 1,
            (int index) => DropdownMenuItem(
                value: param.items[index].id,
                child: Text(loc.getString(param.items[index].id.toString()) +
                    (param.suffixText == null
                        ? ''
                        : (' ' + loc.getString(param.suffixText))))));
      }
      return [];
    } else {
      return super.getDropdownItems(context, block, instance, param);
    }
  }

  @override
  Widget buildParam(Block block, BlockParams instance, Param param, {Key key}) {
    final loc = AppLocalizations.of(context);
    if (param.name == 'pBracketType') {
      return Dropdown(
          value: param.value,
          dropdownName: loc.getString(param.name),
          items: getDropdownItems(context, block, instance, param),
          valueToLabel: (value, ctx) =>
              dropdownValueToLabelLocalized(value, ctx, param.suffixText),
          onChanged: (enumValue) {
            param.value = enumValue;
            ++args.appStore.lastUpdatedCount;

            if (param.value == BracketType.halfRect &&
                instance.data['pBracketPredefinedSize'].value ==
                    BracketSize.s200) {
              instance.data['pBracketPredefinedSize'].value = BracketSize.s150;
              if (_sizeKey.currentState != null) {
                _sizeKey.currentState.updateValue(BracketSize.s150);
              }
            }

            setState(() {});
          });
    }

    if (param.name == 'pBracketPredefinedSize') {
      return super.buildParam(block, instance, param, key: _sizeKey);
    }

    return super.buildParam(block, instance, param, key: key);
  }
}
