import 'package:ejot_ru/ui/pages/BasePage.dart';
import 'package:ejot_ru/ui/widgets/AutoSizeText.dart';
import 'package:ejot_ru/ui/widgets/ZoomableImage.dart';
import 'package:flutter/material.dart';

class ZoomableImagePageArguments {
  final String imgPath;
  final String headerText;

  ZoomableImagePageArguments({this.headerText, this.imgPath});
}

class ZomableImagePage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    final ZoomableImagePageArguments args = ModalRoute.of(context).settings.arguments;
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: true,
        title: AutoSizeText(
          args.headerText,
          presetFontSizes: [15.0, 13.0, 11.0],
          maxLines: 4,
        ),
        leading: IconButton(icon:Icon(Icons.arrow_back),
          onPressed:() => Navigator.pop(context, false),
        )
      ),
      body: BasePage(
        mainContent: Hero( 
          tag: args.imgPath,
          child: ZoomableImage(AssetImage(args.imgPath), backgroundColor: Colors.white)
        )
      )
    );
  }
}
