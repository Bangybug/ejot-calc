import 'dart:convert';

import 'package:ejot_ru/model/City.dart';
import 'package:flutter/widgets.dart';
import 'package:mobx/mobx.dart';
import 'package:flutter/services.dart' show rootBundle;

part 'CityStore.g.dart';

class CityStore extends _CityStore with _$CityStore {
}

abstract class _CityStore with Store {
  
  @observable
  List<Region> regions = [];

  @action
  Future<List<Region>> fetchRegions(BuildContext context, String languageCode) async {
    if (regions.isEmpty) {  
      return rootBundle.loadString('assets/regions_$languageCode.json')
        .then( (regionString) {
          Iterable regionsIt = json.decode(regionString);
          regions = List<Region>.from(regionsIt.map(
            (regJson) => Region.fromJson(regJson)
          ));
          return regions;
        });
    } else {
      return Future.value(regions);
    }
  }

}