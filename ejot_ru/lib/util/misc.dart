import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'Tuple.dart';

String doubleToString(double value) {
  if (value == null || value.isNaN || value.isInfinite) {
    return '';
  }
  return value.toString();
}

double stringToDouble(String value) {
  if (value == null || value == '') {
    return null;
  }
  return double.tryParse(value) ?? null;
}

double roundDouble(double value, int places) {
  double mod = pow(10.0, places);
  return ((value * mod).round().toDouble() / mod);
}

class DecimalTextInputFormatter extends TextInputFormatter {
  DecimalTextInputFormatter({this.decimalRange, this.activatedNegativeValues})
      : assert(decimalRange == null || decimalRange >= 0,
            'DecimalTextInputFormatter declaretion error');

  final int decimalRange;
  final bool activatedNegativeValues;

  @override
  TextEditingValue formatEditUpdate(
    TextEditingValue oldValue, // unused.
    TextEditingValue newValue,
  ) {
    TextSelection newSelection = newValue.selection;
    String truncated = newValue.text;

    if (newValue.text.contains(",")) {
      truncated = newValue.text.replaceFirst(RegExp(','), '.');
    }

    if (newValue.text.contains(' ')) {
      return oldValue;
    }

    if (newValue.text.isEmpty) {
      return newValue;
    } else if (double.tryParse(newValue.text) == null &&
        !(newValue.text.length == 1 &&
            (activatedNegativeValues == true ||
                activatedNegativeValues == null) &&
            newValue.text == '-')) {
      return oldValue;
    }

    if (activatedNegativeValues == false &&
        double.tryParse(newValue.text) < 0) {
      return oldValue;
    }

    if (decimalRange != null) {
      String value = newValue.text;

      if (decimalRange == 0 && value.contains(".")) {
        truncated = oldValue.text;
        newSelection = oldValue.selection;
      }

      if (value.contains(".") &&
          value.substring(value.indexOf(".") + 1).length > decimalRange) {
        truncated = oldValue.text;
        newSelection = oldValue.selection;
      } else if (value == ".") {
        truncated = "0.";

        newSelection = newValue.selection.copyWith(
          baseOffset: min(truncated.length, truncated.length + 1),
          extentOffset: min(truncated.length, truncated.length + 1),
        );
      }

      return TextEditingValue(
        text: truncated,
        selection: newSelection,
        composing: TextRange.empty,
      );
    }
    return newValue;
  }
}

double lerp(double x, double x0, double x1, double y0, double y1) {
  double t = ((x - x0) / (x1 - x0));
  return (y0 + t * (y1 - y0));
}

double lerpTable(
    int vIndex, double tValue, List<List<double>> table, bool canExtrapolate) {
  // bool outLeft = false, outRight = false;
  var rowCount = table.length;

  for (var i = 0; i < rowCount; ++i) {
    var a = table[i][0];

    if (i == 0 && tValue < a) {
      // outLeft = true;

      if (canExtrapolate)
        return lerp(
            tValue, a, table[i + 1][0], table[i][vIndex], table[i + 1][vIndex]);
      else
        return table[i][vIndex];
    }

    if (i == rowCount - 1 && tValue >= a) {
      // outRight = true;

      if (canExtrapolate)
        return lerp(
            tValue, table[i - 1][0], a, table[i - 1][vIndex], table[i][vIndex]);
      else
        return table[i][vIndex];
    }

    if (tValue >= a && tValue <= table[i + 1][0]) {
      return lerp(
          tValue, a, table[i + 1][0], table[i][vIndex], table[i + 1][vIndex]);
    }
  }

  return 0;
}

// x-values in first row starting from i=1,  y-values in first column starting from j=1
double lerpZxy(double x, double y, List<List<double>> xyz) {
  double x1, y1, x2, y2;
  double q11, q21, q12, q22;

  int rowCount = xyz.length;
  int colCount = xyz[0].length;
  int xIndex = 1, yIndex = 1;

  if (x > xyz[0][colCount - 1]) {
    xIndex = colCount - 2;
  } else if (x > xyz[0][1]) {
    for (int i = 1; i < colCount - 1; ++i) {
      if (x >= xyz[0][i] && x <= xyz[0][i + 1]) {
        xIndex = i;
        break;
      }
    }
  }

  if (y > xyz[rowCount - 1][0]) {
    yIndex = rowCount - 2;
  } else if (y > xyz[1][0]) {
    for (int i = 1; i < rowCount - 1; ++i) {
      if (y >= xyz[i][0] && y <= xyz[i + 1][0]) {
        yIndex = i;
        break;
      }
    }
  }

  x1 = xyz[0][xIndex];
  x2 = xyz[0][xIndex + 1];
  y1 = xyz[yIndex][0];
  y2 = xyz[yIndex + 1][0];
  q11 = xyz[yIndex][xIndex];
  q21 = xyz[yIndex + 1][xIndex];
  q12 = xyz[yIndex][xIndex + 1];
  q22 = xyz[yIndex + 1][xIndex + 1];

  return bilerp(q11, q12, q21, q22, x1, x2, y1, y2, x, y);
}

/*    Q12         Q22
  y2    *            *  
            Q(x,y)
  y          o    

  y1    *            *
        x1   x      x2  
        Q11         Q21
  */
double bilerp(double q11, double q12, double q21, double q22, double x1,
    double x2, double y1, double y2, double x, double y) {
  double x2x1, y2y1, x2x, y2y, yy1, xx1;
  x2x1 = x2 - x1;
  y2y1 = y2 - y1;
  x2x = x2 - x;
  y2y = y2 - y;
  yy1 = y - y1;
  xx1 = x - x1;
  return 1.0 /
      (x2x1 * y2y1) *
      (q11 * x2x * y2y + q21 * xx1 * y2y + q12 * x2x * yy1 + q22 * xx1 * yy1);
}

double lerpZaxy(double a, double x, double y,
    List<Tuple2<double, List<List<double>>>> axyz) {
  int aIndex = 0;
  int rowCount = axyz.length;
  if (a > axyz[rowCount - 1].item1) {
    aIndex = rowCount - 2;
  } else if (a > axyz[0].item1) {
    for (int i = 0; i < rowCount - 1; ++i) {
      if (a >= axyz[i].item1 && a <= axyz[i + 1].item1) {
        aIndex = i;
        break;
      }
    }
  }

  final tableZxy = [
    axyz[aIndex].item1,
    lerpZxy(x, y, axyz[aIndex].item2),
    axyz[aIndex + 1].item1,
    lerpZxy(x, y, axyz[aIndex + 1].item2),
  ];

  return lerp(a, tableZxy[0], tableZxy[2], tableZxy[1], tableZxy[3]);
}

Future<void> showModalDialog(
    BuildContext context, String caption, String line1, String line2) async {
  return showDialog<void>(
    context: context,
    barrierDismissible: false, // user must tap button!
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text(caption),
        content: SingleChildScrollView(
          child: ListBody(
            children: <Widget>[
              Text(line1),
              Text(line2),
            ],
          ),
        ),
        actions: <Widget>[
          FlatButton(
            child: Text('Ok'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}


Future<void> showConfirmationDialog(
    BuildContext context, String caption, String line1, String line2, String cancelText, String okText) async {
  return showDialog<void>(
    context: context,
    barrierDismissible: false, // user must tap button!
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text(caption),
        content: SingleChildScrollView(
          child: ListBody(
            children: <Widget>[
              Text(line1),
              Text(line2),
            ],
          ),
        ),
        actions: <Widget>[
          FlatButton(
            child: Text(okText),
            onPressed: () {
              Navigator.of(context).pop(true);
            },
          ),

          FlatButton(
            child: Text(cancelText),
            onPressed: () {
              Navigator.of(context).pop(false);
            },
          ),

        ],
      );
    },
  );
}

