import 'package:ejot_ru/data/AppLocalizations.dart';
import 'package:ejot_ru/data/constants.dart';
import 'package:ejot_ru/model/AppStore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:provider/provider.dart' as P;

List<Widget> localizeStep(int currentStep, BuildContext context) {
  var loc =  AppLocalizations.of(context);
  if (STEPS_TOTAL == currentStep) {
    return [
      Text(loc.getString('saveToPdf'))
    ];
  } else {
    return [
      Text( loc.getString('step'+(currentStep+1).toString()) ),
      Text( loc.getString('nextStep') + ' '+(currentStep+1).toString()+' '+loc.getString('stepFrom')+' '+STEPS_TOTAL.toString()+' ', style: TextStyle(fontSize: 12))
    ];
  }
}



class Stepfab extends StatefulWidget {
  Stepfab({ Key key, this.handlePageChange, this.showPdf }) : super(key: key);

  final ValueChanged<int> handlePageChange;
  final Function showPdf;

  @override
  StepfabState createState() => StepfabState();
}

class StepfabState extends State<Stepfab> {
  @override
  Widget build(BuildContext context) {
    // FYI hide fab when keyboard is on
    final bool showFab = MediaQuery.of(context).viewInsets.bottom==0.0;
    final loc =  AppLocalizations.of(context);
    
    if (!showFab) {
      return Container();
    }
    
    return Observer(

      builder: (_) { 
        AppStore s = P.Provider.of<AppStore>(context, listen: false);
        int index = s.pageIndex;
        bool isError = s.hasErrors();
        bool isShowingResults = index == STEPS_TOTAL;
        bool isVisible = index < STEPS_TOTAL || isShowingResults && !isError;

        // print("Index "+index.toString()+", e="+isError.toString()+", r="+isShowingResults.toString()+", v="+isVisible.toString());

        return AnimatedOpacity(
          opacity: isVisible ? 1.0 : 0.0,
          duration: Duration(milliseconds: isVisible ? 200 : 0),
          child: Align(
            alignment: Alignment.bottomRight,
            child: Container( 
              margin: EdgeInsets.only(bottom: index == 0 ? 50 : 0), 
              child: FloatingActionButton.extended(
                onPressed: () {
                  if (!isShowingResults) {
                    widget.handlePageChange(index);
                  } else {
                    if (!isError)
                      widget.showPdf(context);
                  }
                },
                label: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: index == 0 ? [Text(loc.getString('start'))] : localizeStep(index, context)
                ),
                icon: Icon(isShowingResults ? Icons.picture_as_pdf : Icons.navigate_next),
                backgroundColor: Colors.green
              )
            )
          )
        );
      }

    );
  }
}