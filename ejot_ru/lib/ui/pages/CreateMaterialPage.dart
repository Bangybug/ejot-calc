import 'package:ejot_ru/data/AppLocalizations.dart';
import 'package:ejot_ru/util/misc.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:ejot_ru/model/Material.dart' as Mtl;

import '../theme.dart';
import 'BasePage.dart';
import 'MaterialSelectorPage.dart';

class MaterialCreatePage extends StatefulWidget {
  MaterialCreatePage({Key key}) : super(key: key);

  @override
  _MaterialCreatePageState createState() => _MaterialCreatePageState();
}

class _MaterialCreatePageState extends State<MaterialCreatePage> {
  Mtl.Material createdMaterial;
  final ctlLambdaA = TextEditingController();
  final ctlLambdaB = TextEditingController();
  final ctlRho = TextEditingController();
  final ctlName = TextEditingController();
  bool isValid = false;

  void _checkValid() {
    final MaterialCreateArguments args =
        ModalRoute.of(context).settings.arguments;
    double la = stringToDouble(ctlLambdaA.text);
    double lb = stringToDouble(ctlLambdaB.text);
    bool _isValid = ctlName.text.isNotEmpty &&
        (la != null && la > 0 && lb != null && lb > 0);
    if (_isValid && args.matType == MatType.OTHER) {
      double rho = stringToDouble(ctlRho.text);
      _isValid = rho != null && rho > 0;
    }
    if (_isValid != isValid) {
      setState(() {
        isValid = _isValid;
      });
    }
  }

  void _handleCreateMaterial() async {
    final MaterialCreateArguments args =
        ModalRoute.of(context).settings.arguments;
    double la = stringToDouble(ctlLambdaA.text);
    double lb = stringToDouble(ctlLambdaB.text);
    double rho =
        (args.matType == MatType.OTHER) ? stringToDouble(ctlRho.text) : null;

    Mtl.Material mtl = await args.mtlStore
        .createCustomMaterial(args.matType, ctlName.text, la, lb, rho);
    Navigator.pop(context, MaterialCreateResult(mtl, args.matType));
  }

  Widget _buildMaterialInput(BuildContext context) {
    var loc = AppLocalizations.of(context);
    final MaterialCreateArguments args =
        ModalRoute.of(context).settings.arguments;
    return Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          SizedBox(height: 20),
          createInputGroup(context, children: <Widget>[
            buildTextInput(context, loc.getString('inputMaterialName'), ctlName,
                (text) {
              _checkValid();
            }),
            buildNumericInput(context, 'λa = ', ctlLambdaA, (text) {
              _checkValid();
            }, suffixText: loc.getString('W/m2C'), needBorder: true, indent: 4),
            buildNumericInput(context, 'λb = ', ctlLambdaB, (text) {
              _checkValid();
            }, suffixText: loc.getString('W/m2C'), needBorder: true, indent: 4),
            args.matType == MatType.INSULATION
                ? SizedBox()
                : buildNumericInput(context, 'ρ = ', ctlRho, (text) {
                    _checkValid();
                  }, suffixText: loc.getString('kg/m3'), needBorder: true, indent: 4),
          ], useDecoration: true),
          SizedBox(height: 60)
        ]);
  }

  Widget _buildBody(BuildContext context) {
    return BasePage(mainContent: _buildMaterialInput(context));
  }

  Widget _buildFab(BuildContext context) {
    var loc = AppLocalizations.of(context);
    return AnimatedOpacity(
        opacity: isValid ? 1.0 : 0.0,
        duration: Duration(milliseconds: 200),
        child: FloatingActionButton.extended(
            onPressed: _handleCreateMaterial,
            label: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [Text(loc.getString('createMaterial'))]),
            icon: Icon(Icons.add),
            backgroundColor: Colors.green));
  }

  @override
  Widget build(BuildContext context) {
    var loc = AppLocalizations.of(context);
    return Scaffold(
      appBar: AppBar(
          automaticallyImplyLeading: true,
          title: Text(loc.getString('createMaterial')),
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () => Navigator.pop(context, false),
          )),
      body: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: _buildBody(context)),
      floatingActionButton: _buildFab(context),
    );
  }
}
