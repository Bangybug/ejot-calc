
import 'dart:convert';

import 'package:ejot_ru/model/AppStore.dart';
import 'package:ejot_ru/ui/widgets/MyHeader.dart';
import 'package:ejot_ru/util/Tuple.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'BasePage.dart';

class TestPage extends StatefulWidget {
  final ValueChanged<Tuple2<int,bool>> handlePageChange;
  final TextEditingController _controller = new TextEditingController();

  TestPage({Key key, this.handlePageChange}) : super(key: key);

  @override
  _TestState createState() => _TestState();
}

class _TestState extends State<TestPage> {

  AppStore _appStore;

  Widget _buildResults(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback( (_) => widget._controller.text = jsonEncode(_appStore.toJson()) ); 

    return SingleChildScrollView(
    child: Column(
        children: <Widget>[
        TextField(
          
            decoration: InputDecoration(border: InputBorder.none),
            autofocus: true,
            keyboardType: TextInputType.multiline,
            maxLines: null,
            controller: widget._controller,
            ),
        ],
    ),
    );
  }

 @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _appStore ??= Provider.of<AppStore>(context);
  }

  Widget _buildPage(BuildContext context) {
    return BasePage(
      headerContent: MyHeader(pageIndex: 4),
      mainContent: Scrollbar(  
        child: _buildResults(context)
      )
    );
  }

  @override
  Widget build(BuildContext context) {
    return _buildPage(context);
  }
}


