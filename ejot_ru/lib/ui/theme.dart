import 'package:ejot_ru/util/misc.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

typedef ValueToString = String Function(BuildContext context);

dynamic createTheme(BuildContext context) {
  final ThemeData theme = Theme.of(context);

  final headerBgColor = Color.fromARGB(255, 187, 43, 55);

  return ThemeData(
      primarySwatch: Colors.grey,
      primaryColor: Colors.grey,
      brightness: Brightness.light,
      primaryColorBrightness: Brightness.light,
      backgroundColor: const Color(0xFFE5E5E5),
      accentColor: Colors.white,
      accentColorBrightness: Brightness.light,
      floatingActionButtonTheme:
          FloatingActionButtonThemeData(foregroundColor: Colors.white),
      dividerColor: Colors.white54,
      secondaryHeaderColor: headerBgColor,
      bottomAppBarColor: Colors.grey[850],
      appBarTheme: AppBarTheme(
          color: Colors.white,
          textTheme: theme.primaryTextTheme.copyWith(
              headline6: theme.textTheme.headline6.copyWith(
            color: headerBgColor,
          ))));
}

TextStyle scriptTextStyle() {
  return TextStyle(fontSize: 11);
}

TextStyle hintTextStyle() {
  return TextStyle(fontSize: 11, color: Colors.grey[400]);
}

Widget createInputGroup(BuildContext context,
    {List<Widget> children, Widget title, bool useDecoration = true, BoxDecoration decoration}) {
  if (title != null) {
    children.insert(0, title);
  }

  return Container(
      constraints: BoxConstraints(maxWidth: 400),
      margin: EdgeInsets.only(bottom: 20, left: 2),
      padding: EdgeInsets.only(left: 12, right: 12, bottom: 12),
      child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch, children: children),
      // FYI decorations based on shadows affect elevation (they may remove the elevation)
      decoration: decoration ?? (useDecoration ? 
          new BoxDecoration(
              color: Colors.white,
              boxShadow: [
                new BoxShadow(
                    color: Theme.of(context).buttonColor,
                    offset: new Offset(0.0, 0.0),
                    blurRadius: 20.0,
                    spreadRadius: 10.0)
              ],
              shape: BoxShape.rectangle) : null)
  );
}

// FYI labels in Flutter textfields cannot occupy multiple lines, so imitate them
// if needed
Widget _wrapTextfieldForLongLabel(
    BuildContext context, String hint, int wrapLength, double indent, Widget child) {
  if (hint != null && wrapLength > 0 && hint.length > wrapLength) {
    return Container(margin: EdgeInsets.symmetric(vertical: indent), child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[Text(hint, maxLines: 4), child]));
  } else {
    return Container(margin: EdgeInsets.symmetric(vertical: indent), child: child);
  }
}

InputDecoration _getBorder(BuildContext context, {bool needBorder, bool needOutlineBorder, int wrapLength, String hint, String suffixText, Widget prefix, String isError}) {
  return InputDecoration(
    labelText: (hint != null && wrapLength > 0 && hint.length > wrapLength) ? null : hint,
    suffixText: suffixText,
    prefix: prefix,
    errorText: isError,
    border: needBorder
        ? ( needOutlineBorder ? new OutlineInputBorder() : UnderlineInputBorder(
            borderSide: BorderSide(color: Theme.of(context).primaryColor)) )          
        : InputBorder.none,
    focusedBorder: needOutlineBorder ? new OutlineInputBorder(borderSide: BorderSide(width: 2, color: Theme.of(context).primaryColor)) : UnderlineInputBorder(
        borderSide:
            BorderSide(width: 2, color: Theme.of(context).primaryColor))
  );
}

Widget buildNumericInput(BuildContext context, String hint,
    TextEditingController controller, ValueChanged<String> onChanged,
    {String suffixText,
    Widget prefix,
    bool needBorder = false,
    bool outlineBorder = false,
    String isError,
    bool readOnly,
    int wrapLength = 0,
    double indent = 0,
    ValueChanged<String> onEditingComplete}) {
  return _wrapTextfieldForLongLabel(
      context,
      hint,
      wrapLength,
      indent,
      Focus(
          onFocusChange: (hasFocus) {
            if (!hasFocus) {
              if (onEditingComplete != null)
                onEditingComplete(controller.text);
            }
          },
          child: TextField(
            controller: controller,
            textInputAction: TextInputAction.done,
            readOnly: readOnly ?? false,
            decoration: _getBorder(context, needBorder: needBorder, needOutlineBorder: outlineBorder, wrapLength: wrapLength, hint: hint, suffixText: suffixText, prefix: prefix, isError: isError),
            // FYI numeric keyboard on iOS does not allow decimal separator unless I set signed=true,
            // but with signed=true we get extra non-numeric characters :(
            keyboardType: TextInputType.numberWithOptions(signed: true),
            onChanged: onChanged,
            // FYI onEditingComplete is not called when moving focus (only on 'enter')
            onSubmitted: (value) => FocusScope.of(context).unfocus(),
            onEditingComplete: onEditingComplete == null
                ? null
                : () => onEditingComplete(controller.text),

            inputFormatters: [
              // FYI cannot use digitsOnly - the only available formatter, as it is not letting decimal separators in
              //WhitelistingTextInputFormatter.digitsOnly
              DecimalTextInputFormatter()
            ], // Only numbers can be entered
          )));
}

Widget buildTextInput(BuildContext context, String hint,
    TextEditingController controller, ValueChanged<String> onChanged,
    {String suffixText}) {
  return TextField(
      controller: controller,
      textInputAction: TextInputAction.done,
      decoration: InputDecoration(
          labelText: hint,
          border: InputBorder.none,
          suffixText: suffixText,
          focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Theme.of(context).primaryColor))),
      keyboardType: TextInputType.text,
      onChanged: onChanged);
}
