import 'package:ejot_ru/data/AppLocalizations.dart';
import 'package:ejot_ru/model/AppStore.dart';
import 'package:ejot_ru/ui/theme.dart';
import 'package:ejot_ru/ui/widgets/LinkText.dart';
import 'package:ejot_ru/ui/widgets/MyHeader.dart';
import 'package:ejot_ru/util/misc.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart' as P;

import 'BasePage.dart';

class Step4Economic extends StatefulWidget {

  @override
  _Step4EconomicState createState() => _Step4EconomicState();
}

class _Step4EconomicState extends State<Step4Economic> {
  AppStore _appStore;
  TextEditingController _ctlEjotHd, _ctlOtherAnchor, _ctlInsulation, _ctlEnergy;

  void dispose() {
    super.dispose();
    _ctlEjotHd.dispose();
    _ctlOtherAnchor.dispose();
    _ctlInsulation.dispose();
    _ctlEnergy.dispose();
  }



  void didChangeDependencies() {
    super.didChangeDependencies(); 
    _appStore ??= P.Provider.of<AppStore>(context);
    _ctlEjotHd ??= TextEditingController(text: doubleToString( _appStore.economyCostAnchor ));
    _ctlOtherAnchor ??= TextEditingController(text: doubleToString( _appStore.economyCostOtherAnchor ));
    _ctlInsulation ??= TextEditingController(text: doubleToString( _appStore.economyCostInsulator ));
    _ctlEnergy ??= TextEditingController(text: doubleToString( _appStore.economyCostEnergy ));
  }

  Widget _buildTopHint(BuildContext context) {
    var loc = AppLocalizations.of(context);
    return Padding( 
      padding: EdgeInsets.only(left: 4),
      child: Container(
        color: Colors.white70,
        alignment: AlignmentDirectional.center,
        padding: EdgeInsets.only(left:10, top: 10, right: 10, bottom: 8),
        child: Text(loc.getString('economicHint')),
      )
    );
  }


  Widget _buildList(BuildContext context) {
    var loc = AppLocalizations.of(context);
    const edgeInsets = EdgeInsets.only(left: 4, top:8, bottom: 8, right: 4);
    return ListView(
      children: <Widget>[
        Divider(color: Colors.grey, height: 1),

        ListTile(
          contentPadding: edgeInsets,
          leading: Switch.adaptive(
            activeColor: Theme.of(context).primaryColor,
            value: _appStore.econonmyAnchorCostEnabled,
            onChanged: (bool value) {
              setState(() {
                _appStore.econonmyAnchorCostEnabled = value;
              });
              _appStore.lastUpdatedCount++; 
            }
          ),
          title: Column(children: <Widget>[
            buildNumericInput(context, loc.getString('ecoCostEjot') + ', ' +loc.getString('perPiece'), _ctlEjotHd, 
              (text) { 
                _appStore.economyCostAnchor = stringToDouble(text);
                if (_appStore.economyCostOtherAnchor != null && _appStore.economyCostAnchor != null) {
                  setState(() {
                    _appStore.econonmyAnchorCostEnabled = true;
                  });
                  _appStore.lastUpdatedCount++; 
                }
              }, 
              prefix: Container( margin: EdgeInsets.only(right: 16), child: SvgPicture.asset('assets/icons/rouble.svg', width: 16, height: 12,))
            ),
            buildNumericInput(context, loc.getString('ecoCostOther') + ', ' + loc.getString('perPiece'), _ctlOtherAnchor, 
              (text) { 
                _appStore.economyCostOtherAnchor = stringToDouble(text);
                if (_appStore.economyCostOtherAnchor != null && _appStore.economyCostAnchor != null) {
                  setState(() {
                    _appStore.econonmyAnchorCostEnabled = true;
                  });
                }
                _appStore.lastUpdatedCount++; 
              }, 
              prefix: Container( margin: EdgeInsets.only(right: 16), child: SvgPicture.asset('assets/icons/rouble.svg', width: 16, height: 12,))
            )

          ],)
        ),

        Divider(color: Colors.grey, height: 1),

        ListTile(
          contentPadding: edgeInsets,
          leading: Switch.adaptive(
            activeColor: Theme.of(context).primaryColor,
            value: _appStore.econonmyInsulatorCostEnabled,
            onChanged: (bool value) {
              setState(() {
                _appStore.econonmyInsulatorCostEnabled = value;
              });
              _appStore.lastUpdatedCount++; 
            }
          ),

          title: Column(children: <Widget>[
            buildNumericInput(context, loc.getString('ecoCostInsulation') + ', ' + loc.getString('perM3'), _ctlInsulation, 
              (text) { 
                _appStore.economyCostInsulator = stringToDouble(text);
                if (_appStore.economyCostInsulator != null) {
                  setState(() {
                    _appStore.econonmyInsulatorCostEnabled = true;
                  });
                }
                _appStore.lastUpdatedCount++; 
              }, 
              prefix: Container( margin: EdgeInsets.only(right: 16), child: SvgPicture.asset('assets/icons/rouble.svg', width: 16, height: 12,))
            )
          ],)
        ),

        Divider(color: Colors.grey, height: 1),

        ListTile(
          contentPadding: edgeInsets,
          leading: Switch.adaptive(
            activeColor: Theme.of(context).primaryColor,
            value: _appStore.econonmyEnergyCostEnabled,
            onChanged: (bool value) {
              setState(() {
                _appStore.econonmyEnergyCostEnabled = value;                   
              });
              _appStore.lastUpdatedCount++; 
            }
          ),
          title: Column(children: <Widget>[
            buildNumericInput(context, loc.getString('ecoCostEnergy') + ', ' + loc.getString('perGCal'), _ctlEnergy, 
              (text) { 
                _appStore.economyCostEnergy = stringToDouble(text);
                if (_appStore.economyCostEnergy != null) {
                  setState(() {
                    _appStore.econonmyEnergyCostEnabled = true;
                  });
                }
                _appStore.lastUpdatedCount++; 
              }, 
              prefix: Container( margin: EdgeInsets.only(right: 16), child: SvgPicture.asset('assets/icons/rouble.svg', width: 16, height: 12,)),
            ),
          ],)
        ),

        Divider(color: Colors.grey, height: 1),

        Container(
          padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(loc.getString('tariffExplain'), style: TextStyle(color: Colors.grey[600])),
              SizedBox(height: 15),
              LinkText(
                hyperlink: 'http://docs.cntd.ru/document/563636330',
                title: loc.getString('exampleTariffs') 
              ),
              // OutlineButton(
              //   splashColor: Colors.green,
              //   textColor: Colors.grey,
              //   disabledTextColor: Colors.black,
              //   padding: EdgeInsets.all(8.0),
              //   onPressed: () {
              //     Navigator.pushNamed(context, '/pdfPreviewAsset');
              //   },
              //   child: Text(
              //     loc.getString('exampleTariffs'),
              //     style: TextStyle(fontSize: 15.0),
              //   ),
              // )
            ]
          )
        )
  

      ]
    ); 
  }

  Widget _buildPage(BuildContext context) {
    return BasePage(
      headerContent: MyHeader(pageIndex: 3),
      topContent: _buildTopHint(context),
      mainContent: Scrollbar(  
        child: _buildList(context)
      )
    );
  }

  @override
  Widget build(BuildContext context) {
    return _buildPage(context);
  }
}