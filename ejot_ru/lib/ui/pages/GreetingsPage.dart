import 'package:ejot_ru/ui/pages/BasePage.dart';
import 'package:ejot_ru/ui/widgets/IntroText.dart';
import 'package:ejot_ru/ui/widgets/IntroTextScrollable.dart';
import 'package:ejot_ru/ui/widgets/LogoArranger.dart';
import 'package:ejot_ru/util/TabletDetector.dart';
import 'package:flutter/material.dart';
import 'package:ejot_ru/ui/bottombar/Bottombar.dart';


class GreetingsPage extends StatelessWidget {

  GreetingsPage({Key key, this.handlePageChange}): super(key: key);

  final ValueChanged<int> handlePageChange;

  Widget buildScrollable( { Widget child }) {
    return SingleChildScrollView(
      child: ConstrainedBox(
        constraints: BoxConstraints(
        ),
        child: child
      ),
    );
  }

  Widget _buildTextPart(BuildContext context) {
    final isTablet = TabletDetector.isTablet(MediaQuery.of(context));
    return !isTablet ? IntroText(handlePageChange) : IntroTextScrollable(handlePageChange);
  }


  @override
  Widget build(BuildContext context) {
    return BasePage(
      mainContent: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(left: TabletDetector.isTablet(MediaQuery.of(context)) ? 3 : 0),
            child: LogoArranger()
          ),
          Expanded(child: _buildTextPart(context))
        ],
      ),
      bottomContent: BottomBar(),
    );
  }

}
