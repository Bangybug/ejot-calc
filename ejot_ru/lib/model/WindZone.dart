import 'package:ejot_ru/data/AppLocalizations.dart';
import 'package:flutter/material.dart';
import 'City.dart';
import 'package:ejot_ru/util/misc.dart';

class WindZone {
  WindZone(this.id, this.w0Pressure);
  final String id;
  final double w0Pressure;
}

final List<WindZone> windZones = [ WindZone('Ia', 0.17), WindZone('I', 0.23), WindZone('II', 0.3), WindZone('III', 0.38), WindZone('IV', 0.48), WindZone('V', 0.6), WindZone('VI', 0.73), WindZone('VII', 0.85) ];


String localizeWindZone(dynamic wz, BuildContext context) {
  if (wz == null) {
    return AppLocalizations.of(context).getString('windZoneSelect');
  }
  return wz.id;
}

String localizeWindZoneHint(City city, WindZone wz, BuildContext context) {
  if (wz == null) {
    return '';
  }
  var loc = AppLocalizations.of(context);
  if (city != null && city.windZone == wz) {
    return loc.getString('windZoneForCity') + ' ' + city.name + ', ' + doubleToString( wz.w0Pressure ) + ' ' + loc.getString('pressureUnits');
  } else {
    return loc.getString('windZoneManual') + ' ' + doubleToString( wz.w0Pressure ) + ' ' + loc.getString('pressureUnits');
  }
}