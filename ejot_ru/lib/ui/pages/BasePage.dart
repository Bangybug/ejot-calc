import 'package:flutter/widgets.dart';

class BasePage extends StatefulWidget { 
  BasePage({Key key, this.headerContent, this.topContent, this.bottomContent, this.mainContent}) : super(key: key);

  final Widget headerContent, topContent, bottomContent, mainContent;

  @override
  BasePageState createState() => BasePageState();
}

class BasePageState extends State<BasePage>  with SingleTickerProviderStateMixin {
  bool _showTopContent = true;

  @override
  Widget build(BuildContext context) {
    var widgets = new List<Widget>(); 
    var scrollableWidgets = new List<Widget>(); 

    if (null != widget.headerContent) {
      widgets.add( widget.headerContent );
    }

    if (this.widget.topContent != null) {
      // FYI with this approach, I get debug assertions (luckily they are non-fatal and gracefully skipped)
      // 'package:flutter/src/widgets/scrollable.dart': Failed assertion: line 493 pos 12: '_drag == null': is not true.
      // Seen in android emulator, when I scroll up and down without releasing a finger, after topContent is hidden, the remaining place is sufficient to hide scrollbar
      
      scrollableWidgets.add( 
        AnimatedSize(
          duration: Duration(milliseconds: 200),
          vsync: this,
          child: Container( 
            child: widget.topContent,
            height: _showTopContent ? 60 : 0,
          )
        )
      );
    }
    if (this.widget.mainContent != null) {
      scrollableWidgets.add(Expanded(child: this.widget.mainContent));
    } else {
      scrollableWidgets.add(Expanded(child: Container()));
    }
    if (this.widget.bottomContent != null) {
      scrollableWidgets.add(this.widget.bottomContent);
    }

    widgets.add(Expanded(child: Column(children:scrollableWidgets)));
   
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: widgets 
    );

  }

  void animateTopContent(bool visible) {
    setState(() {
      _showTopContent = visible;
    });
  }
}