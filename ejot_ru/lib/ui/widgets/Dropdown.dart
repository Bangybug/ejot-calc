import 'package:ejot_ru/data/AppLocalizations.dart';
import 'package:flutter/material.dart';

typedef ValueToLabel = String Function(dynamic, BuildContext context);

String valueToLabelLocalized(dynamic value, BuildContext context) {
  if (value == null)
    return null;
  return AppLocalizations.of(context).getString(value.toString());
}

class Dropdown extends StatefulWidget {
  final String dropdownName;
  final List<DropdownMenuItem> items;
  final ValueToLabel valueToLabel;
  final ValueChanged<dynamic> onChanged;
  final Icon icon;
  final double iconSize;
  final dynamic value;
  final bool likeButton;
  
  // FYI the stateful widget's state may stay unchanged when Stateful widget is updated via this constructor
  // state update cannot go through constructor
  Dropdown({Key key, this.dropdownName, this.items, this.valueToLabel, this.onChanged, this.value, this.icon, this.iconSize = 30.0, this.likeButton = true}) : super(key: key);

  @override
  State<StatefulWidget> createState() => DropdownState(this.value);
}

class DropdownState extends State<Dropdown> {
  DropdownState(dynamic value) : this._dropdownValue = value;
  dynamic _dropdownValue;
  GlobalKey _dropdownButtonKey = GlobalKey();

  void updateValue(dynamic value) {
    _dropdownValue = value;
  }


  void openDropdown() {
    GestureDetector detector;
    void searchForGestureDetector(BuildContext element) {
      element.visitChildElements((element) {
        if (element.widget != null && element.widget is GestureDetector) {
          detector = element.widget;
          return false;

        } else {
          searchForGestureDetector(element);
        }

        return true;
      });
    }

    searchForGestureDetector(_dropdownButtonKey.currentContext);
    assert(detector != null);

    detector.onTap();
  }
  
  @override
  Widget build(BuildContext context) {
    Widget dd = DropdownButton(
      key: _dropdownButtonKey,
      hint: _dropdownValue == null
          ? Text(this.widget.dropdownName)
          : Text(
              widget.valueToLabel(_dropdownValue, context),
              style: TextStyle(color: Theme.of(context).primaryColorDark),
            ),
      isExpanded: true,
      value: _dropdownValue,
      underline: Container(
        height: 0,
      ),
      iconSize: widget.iconSize,
      icon: widget.icon,
      style: TextStyle(color: Theme.of(context).primaryColorDark),
      items: widget.items,
      onChanged: (val) {
        setState(
          () {
            _dropdownValue = val;
            if (null != widget.onChanged) {
              widget.onChanged(val);
            }
          },
        );
      },
    );

    return !widget.likeButton ? dd : Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Offstage(child: dd),
        FlatButton(
          onPressed: openDropdown, 
          color: Colors.grey[300],
          child: Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              _dropdownValue == null
                ? Text(this.widget.dropdownName)
                : Text(
                    widget.valueToLabel(_dropdownValue, context),
                    style: TextStyle(color: Theme.of(context).primaryColorDark),
                  ),
              Icon(Icons.arrow_drop_down)
            ],
          ),
        ),
      ],
    );
  }
  
}