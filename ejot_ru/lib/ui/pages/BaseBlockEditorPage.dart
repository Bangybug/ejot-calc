import 'package:ejot_ru/data/AppLocalizations.dart';
import 'package:ejot_ru/model/AppStore.dart';
import 'package:ejot_ru/model/Block.dart';
import 'package:ejot_ru/ui/pages/GalleryPage.dart';
import 'package:ejot_ru/ui/pages/ZoomableImagePage.dart';
import 'package:ejot_ru/ui/theme.dart';
import 'package:ejot_ru/ui/widgets/Dropdown.dart';
import 'package:ejot_ru/ui/widgets/Expandable.dart';
import 'package:ejot_ru/util/ScrollControlMixin.dart';
import 'package:ejot_ru/util/Tuple.dart';
import 'package:ejot_ru/util/misc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'BasePage.dart';
import 'BlockEditorPage.dart';
import 'BlockInsulatorEditorPage.dart';

class BlockEditorArguments {
  final AppStore appStore;
  final Block block;

  BlockEditorArguments(this.appStore, this.block);
}

class BaseBlockEditorPageWrapper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final BlockEditorArguments args = ModalRoute.of(context).settings.arguments;
    if (args.block.blockBinding == BlockBinding.INSULATION_BINDING) {
      return BlockInsulatorEditorPage(args);
    } else if (args.block.blockBinding == BlockBinding.BRACKET_AL_BINDING) {
      return BlockAlBindingPage(args);
    }
    return BlockEditorPage(args);
  }
}

class BaseBlockPageState extends State
    with ScrollControllerMixin, SingleTickerProviderStateMixin {
  BaseBlockPageState(this.args);

  final GlobalKey<BasePageState> _pageKey = GlobalKey<BasePageState>();
  final BlockEditorArguments args;

  final List<ExpandableController> _instanceExpandControllers =
      List<ExpandableController>();
  final List<Map<String, dynamic>> _instanceFieldControllers =
      List<Map<String, dynamic>>();
  final List<dynamic> _disposables = List<dynamic>();

  Animation<double> _onRemoveAnimation;
  AnimationController _controller;
  int _removeIndex;

  void initState() {
    super.initState();
    initScroll(_pageKey);

    _controller = AnimationController(
        duration: const Duration(milliseconds: 500), vsync: this);
    _onRemoveAnimation =
        Tween<double>(begin: 1.0, end: 0.0).animate(_controller)
          ..addListener(() {
            setState(() {
              // update elements
            });

            // FYI removal call disposes controllers (expandable controller and field controllers)
            // If I put this call before setState, I have exceptions in disposing widgets complaining about disposed controllers
            // It turns out, the safest place where controllers can be disposed is the dispose method of the state widget where they belong to
            // so I invented _disposables to sync up the disposal, which is the antipattern, see below why
            if (_onRemoveAnimation.isCompleted) {
              _handleRemoveInstance(_removeIndex);
              _removeIndex = -1;
            }
          });
  }

  void dispose() {
    super.dispose();
    disposeScroll();
    _controller.dispose();
    _instanceExpandControllers.forEach((e) {
      _disposables.add(e);
    });
    _instanceExpandControllers.clear();

    _instanceFieldControllers.forEach((controllers) {
      controllers.values.forEach((controller) {
        if (controller is TextEditingController) {
          _disposables.add(controller);
        }
      });
      controllers.clear();
    });
    _instanceFieldControllers.clear();

    // FYI looks like _disposables is the antipattern here, I should have made stateful widgets to manage their controllers themselves,
    // however I still need to expand/collapse a group of widgets which is directly done via their controllers
    _disposables.forEach((d) => d.dispose());
    _disposables.clear();
  }

  void unfocus() {
    FocusScope.of(context).unfocus();
  }

  void _handleRemoveInstance(int removeIndex) {
    // FYI if I dispose controller here, the controlled widget can be disposed later and thus will try to access that controller in disposed state
    // If I don't dispose controller, it may leak
    _disposables.add(_instanceExpandControllers[removeIndex]);

    _instanceExpandControllers.removeAt(removeIndex);
    args.block.instances.removeAt(removeIndex);

    _instanceFieldControllers[removeIndex].values.forEach((controller) {
      if (controller is TextEditingController) {
        _disposables.add(controller);
      }
    });
    _instanceFieldControllers.removeAt(removeIndex);
  }

  String dropdownValueToLabelLocalized(
      dynamic value, BuildContext context, String suffixText) {
    if (value == null) return null;
    final loc = AppLocalizations.of(context);
    return loc.getString(value.toString()) +
        (null == suffixText ? '' : ' ' + loc.getString(suffixText));
  }

  ExpandableController _getExpandableInstanceController(int instanceIndex) {
    var ctl = _instanceExpandControllers.length > instanceIndex
        ? _instanceExpandControllers[instanceIndex]
        : null;
    if (ctl == null) {
      ctl = ExpandableController(initialExpanded: true);
      _instanceExpandControllers.add(ctl);
    }
    return ctl;
  }

  dynamic _getInstanceFieldTextController(int instanceIndex, Param param) {
    var ctls = _instanceFieldControllers.length > instanceIndex
        ? _instanceFieldControllers[instanceIndex]
        : null;
    if (ctls == null) {
      ctls = Map<String, dynamic>();
      _instanceFieldControllers.add(ctls);
    }
    if (ctls[param.name] == null) {
      ctls[param.name] = TextEditingController(text: param.textValue);
    } else {
      (ctls[param.name] as TextEditingController).text = param.textValue;
    }
    return ctls[param.name];
  }

  List<DropdownMenuItem> getDropdownItems(
      BuildContext context, Block block, BlockParams instance, Param param) {
    var loc = AppLocalizations.of(context);
    if (null != param.items) {
      return List.generate(
          param.items.length,
          (int index) => DropdownMenuItem(
              value: param.items[index].id,
              child: Text(loc.getString(param.items[index].id.toString()) +
                  (param.suffixText == null
                      ? ''
                      : (' ' + loc.getString(param.suffixText))))));
    }
    return [];
  }

  void _checkLimits(TextEditingController controller, Block block,
      BlockParams instance, Param param, String text) {
    if (text.isNotEmpty && param.limits != null && !param.checkLimits(text)) {
      final loc = AppLocalizations.of(context);
      String limitStr = '';
      if (param.limits.length == 1) {
        limitStr = loc.getString('from') + ' ' + param.limits[0].toString();
      } else if (param.limits.length == 2) {
        limitStr = param.limits[0].toString() +
            ' — ' +
            param.limits[1].toString() +
            ' ' +
            loc.getString(param.suffixText);
      }

      showModalDialog(context, loc.getString(param.name),
          loc.getString('complyLimits'), limitStr);

      controller.clear();
      param.value = '';
      ++args.appStore.lastUpdatedCount;
    }
  }

  void _handleParamValueChange(
      Block block, BlockParams instance, Param param, String text) {
    param.value = text;
    ++args.appStore.lastUpdatedCount;
  }

  void _collapseAllInstances() {
    _instanceExpandControllers.forEach((e) => e.expanded = false);
  }

  List<Tuple2<String, String>> _getImages(Block b, BlockParams params) {
    final loc = AppLocalizations.of(context);
    return blockParamsDef[b.blockBinding].item2(b, params).map((e) {
      return Tuple2(e.item1, loc.getString(e.item2.toString()));
    }).toList();
  }

  void _showImages(List<Tuple2<String, String>> images) {
    final loc = AppLocalizations.of(context);
    if (images.length == 1) {
      Navigator.pushNamed(context, '/imageView',
          arguments: ZoomableImagePageArguments(
              headerText: images[0].item2, imgPath: images[0].item1));
    } else {
      Navigator.pushNamed(context, '/imageGallery',
          arguments: GalleryPageArguments(
              headerText: loc.getString('hint'), images: images));
    }
  }

  @override
  Widget build(BuildContext context) {
    final loc = AppLocalizations.of(context);
    final images = _getImages(args.block, null);
    return Scaffold(
        appBar: AppBar(
            automaticallyImplyLeading: true,
            title: Text(loc.getString('editBlock')),
            leading: IconButton(
              icon: Icon(Icons.check),
              onPressed: () => Navigator.pop(context, false),
            )),
        body: GestureDetector(onTap: unfocus, child: _buildBody(context)),
        floatingActionButton: images.isEmpty
            ? Container()
            : AnimatedOpacity(
                opacity: images.isEmpty ? 0.0 : 1.0,
                duration: Duration(milliseconds: 200),
                child: FloatingActionButton.extended(
                    onPressed: () {
                      _showImages(images);
                    },
                    label: Text(loc.getString('hint')),
                    icon: Icon(Icons.image),
                    backgroundColor: Colors.green)));
  }

  Widget buildGlobalParams(BuildContext context) {
    return Container();
  }

  Widget _buildBody(BuildContext context) {
    return BasePage(
        key: _pageKey,
        topContent: buildTopHint(context),
        mainContent: Container(
            child: Padding(
                padding: const EdgeInsets.only(
                    top: 10, left: 0, bottom: 0, right: 0),
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      Expanded(child: _buildInstancesList(context))
                    ]))));
  }

  Widget buildTopHint(BuildContext context) {
    var loc = AppLocalizations.of(context);
    return Material(
        color: Colors.white70,
        type: MaterialType.canvas,
        child: Container(
          alignment: AlignmentDirectional.center,
          padding: EdgeInsets.only(left: 10, top: 10, right: 10, bottom: 4),
          child: Text(loc.getString('fillBlockHint')),
        ));
  }

  Widget _buildInstancesList(BuildContext context) {
    var loc = AppLocalizations.of(context);

    // FYI we are observing on the list 'args.block.instances' - update triggers when something is added/removed in that list
    // this might be a drawback, since all instances are to be rebuilt
    return Observer(builder: (context) {
      if (args.block.instances == null || args.block.instances.isEmpty) {
        return Column(children: <Widget>[
          buildGlobalParams(context),
          args.block.instances != null
              ? (RaisedButton(
                  onPressed: () {
                    args.block.instances.add(
                        blockParamsDef[args.block.blockBinding]
                            .item1(args.block.blockBinding));
                  },
                  child: Text(loc.getString('addNewElement'),
                      style: TextStyle(fontSize: 20)),
                ))
              : Container()
        ]);
      }
      return ListView.builder(
          controller: scrollController,
          itemCount: args.block.instances.length + 1,
          itemBuilder: (BuildContext ctxt, int index) {
            if (index == 0) {
              return buildGlobalParams(context);
            } else {
              index--;
            }

            Widget panel = Container(
                margin: index == args.block.instances.length - 1
                    ? EdgeInsets.only(top: 16, bottom: 40)
                    : EdgeInsets.only(top: 16),
                decoration: BoxDecoration(
                    color: Colors.white,
                    boxShadow: [
                      new BoxShadow(
                          color: Theme.of(context).buttonColor,
                          offset: new Offset(0.0, 0.0),
                          blurRadius: 20.0,
                          spreadRadius: 10.0)
                    ],
                    shape: BoxShape.rectangle),
                child: ExpandablePanel(
                    controller: _getExpandableInstanceController(index),
                    header: _buildInstanceHeader(context, args.block, index),
                    collapsed: Container(),
                    expanded: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: _buildInstanceParams(args.block.instances[index])))
                    );

            if (_removeIndex == index) {
              return FadeTransition(opacity: _onRemoveAnimation, child: panel);
            } else {
              return panel;
            }
          });
    });
  }

  Widget _buildInstanceHeader(
      BuildContext context, Block block, int instanceIndex) {
    var loc = AppLocalizations.of(context);
    String title = loc.getString(args.block.blockName) +
        (args.block.instances.length > 1
            ? (' ' + (instanceIndex + 1).toString())
            : '');

    return Stack(
        alignment: AlignmentDirectional.centerStart,
        children: <Widget>[
          _buildInstanceActionMenu(context, block, instanceIndex),
          Container(
              margin: EdgeInsets.only(left: needActionMenu() ? 40 : 8),
              padding: EdgeInsets.only(top: 10, bottom: 10),
              child: Text(title,
                  style: TextStyle(fontWeight: FontWeight.bold),
                  textScaleFactor: 1.1))
        ]);
  }

  bool needActionMenu() {
    return true;
  }

  Widget _buildInstanceActionMenu(
      BuildContext context, Block block, int instanceIndex) {
    if (needActionMenu()) {
      var loc = AppLocalizations.of(context);
      return PopupMenuButton<String>(
        icon: Icon(Icons.more_vert, color: Theme.of(context).primaryColor),
        onSelected: (text) {
          if (text == InstanceActionMenu.Delete) {
            _removeIndex = instanceIndex;
            _controller.forward(from: 0.0);
          } else {
            _collapseAllInstances();
            block.instances.add(BlockParams.fromList(block
                .instances[instanceIndex]
                .data
                .values)); // New instance will be expanded
          }
        },
        itemBuilder: (BuildContext context) {
          return InstanceActionMenu.choices.map((String choice) {
            return PopupMenuItem<String>(
              value: choice,
              child: Text(loc.getString(choice)),
            );
          }).toList();
        },
      );
    } else {
      return Container();
    }
  }

  List<Widget> _buildInstanceParams(BlockParams instance) {
    return instance.data.entries.map((MapEntry<String, Param> entry) {
      return Container(
          padding: EdgeInsets.only(left: 6, right: 6),
          child: buildParam(args.block, instance, entry.value));
    }).toList();
  }

  Widget buildParam(Block block, BlockParams instance, Param param, {Key key}) {
    var loc = AppLocalizations.of(context);
    int instanceIndex = block.instances.indexOf(instance);
    if (param.type == ParamTypeDesc.NUMERIC) {
      dynamic ctl = _getInstanceFieldTextController(instanceIndex, param);

      return buildNumericInput(
          context,
          loc.getString(param.name),
          ctl,
          (text) {
            _handleParamValueChange(block, instance, param, text);
          },
          suffixText: loc.getString(param.suffixText),
          onEditingComplete: (text) {
            // FYI in attempt to remove focus using screen keyboard "done" button,
            // we are prohibiting focus traversal across fields by tap gesture
            // So, don't use unfocus here.
            // unfocus();
            _checkLimits(ctl, block, instance, param, text);
          }, needBorder: true, indent: 4);
    } else if (param.type == ParamTypeDesc.TEXT) {
      return buildTextInput(context, loc.getString(param.name),
          _getInstanceFieldTextController(instanceIndex, param), (text) {
        _handleParamValueChange(block, instance, param, text);
      }, suffixText: loc.getString(param.suffixText));
    } else if (param.type == ParamTypeDesc.DROPDOWN) {
      return Dropdown(
          key: key,
          value: param.value,
          dropdownName: loc.getString(param.name),
          items: getDropdownItems(context, block, instance, param),
          valueToLabel: (value, ctx) =>
              dropdownValueToLabelLocalized(value, ctx, param.suffixText),
          onChanged: (enumValue) {
            param.value = enumValue;
            ++args.appStore.lastUpdatedCount;
          });
    }
    return Container();
  }
}

class InstanceActionMenu {
  static const String Delete = 'removeThisBlock';
  static const String Add = 'addNewBlock';

  static const List<String> choices = <String>[Delete, Add];
}
