import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';

typedef ValueToWidget = Widget Function(dynamic value, int col, {bool isWarning});

String formatCurrency(String src) {
  int i1 = src.indexOf('.');
  int i2 = src.indexOf(',');
  int i = i1 == -1 ? i2 : i1;
  if (i == -1) 
    i = src.length-1;

  int tripletCount = (i / 3).floor();
  int startingTripletPosition = i - tripletCount * 3;
  String ret = src.substring(0, startingTripletPosition);
  int count3 = 0;
  for (int q = ret.length; q<src.length; ++q) {
    if (q < i && count3 == 0 && ret.isNotEmpty) {
      ret += ' ';
    }
    ret += src[q];
    if (++count3 == 3) count3 = 0;
  }
  return ret;
}

Widget textWidget(dynamic value, _, {bool isWarning}) {
  if (value is Widget)
    return value;
  return Text(value);
}

Widget costByQuantityTotal(double cost, dynamic qty, String pcs) {
  return Row(
    children: [
      SvgPicture.asset('assets/icons/rouble.svg', width: 16, height: 12,),
      SizedBox(width: 8),
      Text(formatCurrency(cost.toString())),
      SizedBox(width: 9),
      Text('x'),
      SizedBox(width: 9),
      Text(qty.toString() + ' ' + pcs)
    ]
  );
}


Widget roubleWidget(double rouble) {
  return Row(
    children: [
      SvgPicture.asset('assets/icons/rouble.svg', width: 16, height: 12,),
      SizedBox(width: 8),
      Text(formatCurrency(rouble.toString()))
    ]
  );
}