// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'AppStore.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$AppStore on _AppStore, Store {
  final _$pageIndexAtom = Atom(name: '_AppStore.pageIndex');

  @override
  int get pageIndex {
    _$pageIndexAtom.context.enforceReadPolicy(_$pageIndexAtom);
    _$pageIndexAtom.reportObserved();
    return super.pageIndex;
  }

  @override
  set pageIndex(int value) {
    _$pageIndexAtom.context.conditionallyRunInAction(() {
      super.pageIndex = value;
      _$pageIndexAtom.reportChanged();
    }, _$pageIndexAtom, name: '${_$pageIndexAtom.name}_set');
  }

  final _$selectedCityAtom = Atom(name: '_AppStore.selectedCity');

  @override
  City get selectedCity {
    _$selectedCityAtom.context.enforceReadPolicy(_$selectedCityAtom);
    _$selectedCityAtom.reportObserved();
    return super.selectedCity;
  }

  @override
  set selectedCity(City value) {
    _$selectedCityAtom.context.conditionallyRunInAction(() {
      super.selectedCity = value;
      _$selectedCityAtom.reportChanged();
    }, _$selectedCityAtom, name: '${_$selectedCityAtom.name}_set');
  }

  final _$temperatureAtom = Atom(name: '_AppStore.temperature');

  @override
  int get temperature {
    _$temperatureAtom.context.enforceReadPolicy(_$temperatureAtom);
    _$temperatureAtom.reportObserved();
    return super.temperature;
  }

  @override
  set temperature(int value) {
    _$temperatureAtom.context.conditionallyRunInAction(() {
      super.temperature = value;
      _$temperatureAtom.reportChanged();
    }, _$temperatureAtom, name: '${_$temperatureAtom.name}_set');
  }

  final _$humidityAtom = Atom(name: '_AppStore.humidity');

  @override
  int get humidity {
    _$humidityAtom.context.enforceReadPolicy(_$humidityAtom);
    _$humidityAtom.reportObserved();
    return super.humidity;
  }

  @override
  set humidity(int value) {
    _$humidityAtom.context.conditionallyRunInAction(() {
      super.humidity = value;
      _$humidityAtom.reportChanged();
    }, _$humidityAtom, name: '${_$humidityAtom.name}_set');
  }

  final _$facadeAtom = Atom(name: '_AppStore.facade');

  @override
  FacadeType get facade {
    _$facadeAtom.context.enforceReadPolicy(_$facadeAtom);
    _$facadeAtom.reportObserved();
    return super.facade;
  }

  @override
  set facade(FacadeType value) {
    _$facadeAtom.context.conditionallyRunInAction(() {
      super.facade = value;
      _$facadeAtom.reportChanged();
    }, _$facadeAtom, name: '${_$facadeAtom.name}_set');
  }

  final _$shouldCalculateInsulatorWidthAtom =
      Atom(name: '_AppStore.shouldCalculateInsulatorWidth');

  @override
  bool get shouldCalculateInsulatorWidth {
    _$shouldCalculateInsulatorWidthAtom.context
        .enforceReadPolicy(_$shouldCalculateInsulatorWidthAtom);
    _$shouldCalculateInsulatorWidthAtom.reportObserved();
    return super.shouldCalculateInsulatorWidth;
  }

  @override
  set shouldCalculateInsulatorWidth(bool value) {
    _$shouldCalculateInsulatorWidthAtom.context.conditionallyRunInAction(() {
      super.shouldCalculateInsulatorWidth = value;
      _$shouldCalculateInsulatorWidthAtom.reportChanged();
    }, _$shouldCalculateInsulatorWidthAtom,
        name: '${_$shouldCalculateInsulatorWidthAtom.name}_set');
  }

  final _$sftkClassAtom = Atom(name: '_AppStore.sftkClass');

  @override
  SftkClass get sftkClass {
    _$sftkClassAtom.context.enforceReadPolicy(_$sftkClassAtom);
    _$sftkClassAtom.reportObserved();
    return super.sftkClass;
  }

  @override
  set sftkClass(SftkClass value) {
    _$sftkClassAtom.context.conditionallyRunInAction(() {
      super.sftkClass = value;
      _$sftkClassAtom.reportChanged();
    }, _$sftkClassAtom, name: '${_$sftkClassAtom.name}_set');
  }

  final _$terrainTypeAtom = Atom(name: '_AppStore.terrainType');

  @override
  TerrainType get terrainType {
    _$terrainTypeAtom.context.enforceReadPolicy(_$terrainTypeAtom);
    _$terrainTypeAtom.reportObserved();
    return super.terrainType;
  }

  @override
  set terrainType(TerrainType value) {
    _$terrainTypeAtom.context.conditionallyRunInAction(() {
      super.terrainType = value;
      _$terrainTypeAtom.reportChanged();
    }, _$terrainTypeAtom, name: '${_$terrainTypeAtom.name}_set');
  }

  final _$insulationMaterial1Atom = Atom(name: '_AppStore.insulationMaterial1');

  @override
  Material get insulationMaterial1 {
    _$insulationMaterial1Atom.context
        .enforceReadPolicy(_$insulationMaterial1Atom);
    _$insulationMaterial1Atom.reportObserved();
    return super.insulationMaterial1;
  }

  @override
  set insulationMaterial1(Material value) {
    _$insulationMaterial1Atom.context.conditionallyRunInAction(() {
      super.insulationMaterial1 = value;
      _$insulationMaterial1Atom.reportChanged();
    }, _$insulationMaterial1Atom,
        name: '${_$insulationMaterial1Atom.name}_set');
  }

  final _$insulationMaterial2Atom = Atom(name: '_AppStore.insulationMaterial2');

  @override
  Material get insulationMaterial2 {
    _$insulationMaterial2Atom.context
        .enforceReadPolicy(_$insulationMaterial2Atom);
    _$insulationMaterial2Atom.reportObserved();
    return super.insulationMaterial2;
  }

  @override
  set insulationMaterial2(Material value) {
    _$insulationMaterial2Atom.context.conditionallyRunInAction(() {
      super.insulationMaterial2 = value;
      _$insulationMaterial2Atom.reportChanged();
    }, _$insulationMaterial2Atom,
        name: '${_$insulationMaterial2Atom.name}_set');
  }

  final _$coreMaterialAtom = Atom(name: '_AppStore.coreMaterial');

  @override
  Material get coreMaterial {
    _$coreMaterialAtom.context.enforceReadPolicy(_$coreMaterialAtom);
    _$coreMaterialAtom.reportObserved();
    return super.coreMaterial;
  }

  @override
  set coreMaterial(Material value) {
    _$coreMaterialAtom.context.conditionallyRunInAction(() {
      super.coreMaterial = value;
      _$coreMaterialAtom.reportChanged();
    }, _$coreMaterialAtom, name: '${_$coreMaterialAtom.name}_set');
  }

  final _$windZoneAtom = Atom(name: '_AppStore.windZone');

  @override
  WindZone get windZone {
    _$windZoneAtom.context.enforceReadPolicy(_$windZoneAtom);
    _$windZoneAtom.reportObserved();
    return super.windZone;
  }

  @override
  set windZone(WindZone value) {
    _$windZoneAtom.context.conditionallyRunInAction(() {
      super.windZone = value;
      _$windZoneAtom.reportChanged();
    }, _$windZoneAtom, name: '${_$windZoneAtom.name}_set');
  }

  final _$lastUpdatedCountAtom = Atom(name: '_AppStore.lastUpdatedCount');

  @override
  int get lastUpdatedCount {
    _$lastUpdatedCountAtom.context.enforceReadPolicy(_$lastUpdatedCountAtom);
    _$lastUpdatedCountAtom.reportObserved();
    return super.lastUpdatedCount;
  }

  @override
  set lastUpdatedCount(int value) {
    _$lastUpdatedCountAtom.context.conditionallyRunInAction(() {
      super.lastUpdatedCount = value;
      _$lastUpdatedCountAtom.reportChanged();
    }, _$lastUpdatedCountAtom, name: '${_$lastUpdatedCountAtom.name}_set');
  }

  final _$lastCalculatedCountAtom = Atom(name: '_AppStore.lastCalculatedCount');

  @override
  int get lastCalculatedCount {
    _$lastCalculatedCountAtom.context
        .enforceReadPolicy(_$lastCalculatedCountAtom);
    _$lastCalculatedCountAtom.reportObserved();
    return super.lastCalculatedCount;
  }

  @override
  set lastCalculatedCount(int value) {
    _$lastCalculatedCountAtom.context.conditionallyRunInAction(() {
      super.lastCalculatedCount = value;
      _$lastCalculatedCountAtom.reportChanged();
    }, _$lastCalculatedCountAtom,
        name: '${_$lastCalculatedCountAtom.name}_set');
  }

  @override
  String toString() {
    final string =
        'pageIndex: ${pageIndex.toString()},selectedCity: ${selectedCity.toString()},temperature: ${temperature.toString()},humidity: ${humidity.toString()},facade: ${facade.toString()},shouldCalculateInsulatorWidth: ${shouldCalculateInsulatorWidth.toString()},sftkClass: ${sftkClass.toString()},terrainType: ${terrainType.toString()},insulationMaterial1: ${insulationMaterial1.toString()},insulationMaterial2: ${insulationMaterial2.toString()},coreMaterial: ${coreMaterial.toString()},windZone: ${windZone.toString()},lastUpdatedCount: ${lastUpdatedCount.toString()},lastCalculatedCount: ${lastCalculatedCount.toString()}';
    return '{$string}';
  }
}
