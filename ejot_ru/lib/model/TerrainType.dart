import 'package:ejot_ru/data/AppLocalizations.dart';
import 'package:flutter/material.dart';

class TerrainType {
  int id;
  double alpha;
  double k10;
  double s10;

  TerrainType(this.alpha, this.k10, this.s10, {this.id});
}

List<TerrainType> terrainTypes = [ 
  TerrainType(0.15, 1.0, 0.76, id: 0), 
  TerrainType(0.2, 0.65, 1.06, id: 1), 
  TerrainType(0.25, 0.4, 1.78, id: 2), 
];

String localizeTerrainType(dynamic tt, BuildContext context) {
  if (tt == null) {
    return AppLocalizations.of(context).getString('terrainType');
  }
  return AppLocalizations.of(context).getString('terrainType'+(tt as TerrainType).id.toString());
}

String localizeTerrainTypeLabel(dynamic tt, BuildContext context) {
  if (tt == null) {
    return '';
  }
  return AppLocalizations.of(context).getString('terrainTypeLabel'+(tt as TerrainType).id.toString());
}