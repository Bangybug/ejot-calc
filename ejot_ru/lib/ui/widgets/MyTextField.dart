import 'package:flutter/material.dart';

class MyTextField extends StatelessWidget {

  MyTextField({Key key, this.onChanged, this.controller, this.hintText, this.suffixIcon, this.prefixIcon, this.margin, this.prefixIconNoPadding, this.needBorder = false,
    this.indent = 12.0}) : super(key: key);

  final ValueChanged<String> onChanged;
  final TextEditingController controller;
  final String hintText;
  final Widget suffixIcon;
  final Widget prefixIcon;
  final EdgeInsets margin;
  final bool prefixIconNoPadding;
  final bool needBorder;
  final double indent;

  InputDecoration _buildDecoration(BuildContext context, Widget usePrefixIcon) {
    if (needBorder) {
      return InputDecoration(
        labelText: hintText,
        contentPadding: EdgeInsets.symmetric(vertical: indent, horizontal: indent),
        labelStyle: TextStyle(backgroundColor: Colors.transparent),
        border: new OutlineInputBorder(
          borderRadius: new BorderRadius.circular(2.0),
          borderSide:  BorderSide(color: Theme.of(context).primaryColor)
        ),
        prefixIcon: usePrefixIcon,
        suffixIcon: suffixIcon
      );
    } else {
      return InputDecoration(
        labelText: hintText,
        border: InputBorder.none,
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: Theme.of(context).primaryColor)
        ),
        prefixIcon: usePrefixIcon,
        suffixIcon: suffixIcon
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    Widget usePrefixIcon = prefixIcon;
    if (prefixIcon != null && prefixIconNoPadding) {
      usePrefixIcon = Container( 
        width: 0,
        alignment: Alignment(-0.99, 0.0),  // FYI this hack removes padding around prefixIcon inside TextField
        child: prefixIcon
      );
    }

    return Container(
      margin: margin != null ? margin : EdgeInsets.only(
        top: indent,
        bottom: indent,
        left: 0.0,
        right: 0.0,
      ),
      child: TextField(
        controller: controller,
        onChanged: (text) {
          onChanged(text);
        },
        style: TextStyle( // FYI I had to tweak line height to make icon and text aligned on their middle baseline
          fontSize: 15,
          height: 1.4
        ),
        decoration: _buildDecoration(context, usePrefixIcon)
      )
    );
  }

}