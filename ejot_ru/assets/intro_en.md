## Application for calculations

- Estimated thermal resistance of the walls
- Estimate required amount of anchors for ETICS
- Economy estimations for choosing EJOT H5  

The calculations are based on standards in Russian Federation:

- СП 50.13330.2012 «Thermal protection»
- СП 230.1325800.2015 «Bounding constructions. Thermal engineering, characteristics of discontinuitites»
- СП 131.13330.2018 «Climatology for Construction engineering»
- СП 20.13330.2016 «Loads and impacts»
- СП 293.1325800.2017 «External thermal insulation composite systems (ETICS). Engineering regulations»
- ГОСТ Р 56707-2015 «ETICS. General technological conditions»
- ГОСТ Р 58360-2019 «Anchors for ETICS, Tests»
- ГОСТ 30494-2011 «Microclimate characteristics in buildings»

## Anchor Ejot H5

Universal anchor EJOT H5 is used to attach thermal insulation layer in an ETICS system. Thanks to innovative technologies, Ejot H5 has the lowest thermal losses of 0,001 W/K.  

Universal Anchor EJOT H5 is made of these parts:

1. **Sleeve** 
- Made of 100% polyethylene: withstands cold weather, has high load capacity under cold temperature.
- Advanced dish shape for precise fixation.
- Anchor length from 115 to 295 mm.

2. **Thermal plug** 
- Made of strenghtened fiber-filled polyamide: high mechanical resistance.
- Enlarged plug: lowers thermal conductivity, prevention of cold bridges.
- Plug length from 52 to 112 mm.

3. **Spacer** (steel nail with a notch)
- Protective coating width 10 um: corrosion protection.
- Notch prevents tear-down.
- Small minimal anchor depth 25 mm: faster installation, swirl economy.
- Universal spacer for all kinds of base materials.

#  
#  
**Technical characteristics**

|             |               |
| :---        | :---          |
|Nominal dowel diameter  | **8 mm** |
|Disc diameter | **60 mm** |
|Anchoring depth | from **35(65) mm** |
|Anchoring zone | from **25(55) mm** |
|Class | A, B, C, D, E |
|License | № 5460-18 |

## About Ejot

International holding EJOT is developing fasteners for construction, automotive and electronic industry.

Ejot History began in German city Bad-Berleburg almost a century ago.

Todays Ejot employee count reached 3300, in the headquarters and production area. 

By 2017 Finnish company Sormat joined Ejot. Sormat is in the top 4 anchor production companies.

Ejot – is one of the leaders of fastener production in Western Europe. Ejot is highly oriented to its customers: all products being developed in partnership with big customers, to meet their specific demands for innovative fastening solutions.

EJOT fasteners while providing high reliability, also let customers lower their expenses.

**EJOT – is more than innovative fasteners.**