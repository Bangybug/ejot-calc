import 'package:ejot_ru/calculator/ThermicCalc.dart';
import 'package:ejot_ru/model/AppStore.dart';
import 'package:ejot_ru/util/misc.dart';

class EconomicCalc {
  EconomicCalc(AppStore appStore, ThermicCalc thermicCalc) : this.s = appStore, this.thermicCalc = thermicCalc;

  AppStore s;
  ThermicCalc thermicCalc;
  Report thermicReport;

  int ejotQty;
  int otherQty;
  double anchorEconomyQty;
  double anchorEconomyMoney;

  double energyCostEconomyMoney;
  double ejotAnnualLoss;

  double insulatorCostEconomy, insulatorVolumeEjot, insulatorVolumeOther;
   

  bool canCalculate() {
    return !s.hasErrors() && thermicCalc.hasComparison() && (
      this.canCalculateAnchorCost()
      || this.canCalculateEnergyCostEconomy()
      || this.canCalculateInsulatorEconomy()
    );
  }

  bool canCalculateAnchorCost() {
    return s.econonmyAnchorCostEnabled && s.economyCostAnchor != null && s.economyCostAnchor > 0.0 && s.economyCostOtherAnchor != null && 
      s.economyCostOtherAnchor > 0.0 && thermicCalc.otherAnchorPerM2 != null;
  }

  bool canCalculateEnergyCostEconomy() {
    return !s.shouldCalculateInsulatorWidth && s.econonmyEnergyCostEnabled && s.economyCostEnergy != null && s.economyCostEnergy > 0.0;
  } 
  
  bool canCalculateInsulatorEconomy() {
    return s.shouldCalculateInsulatorWidth && s.econonmyInsulatorCostEnabled && s.economyCostInsulator != null && s.economyCostInsulator > 0.0;
  }

  void calculate(Report thermicReport) {

    anchorEconomyQty = null;
    anchorEconomyMoney = null;

    if (canCalculateAnchorCost()) {
      ejotQty = ( thermicReport.facadeSquareWithoutHoles * thermicCalc.ejotAnchorPerM2 ).ceil();
      otherQty = ( thermicReport.facadeSquareWithoutHoles * thermicCalc.otherAnchorPerM2 ).ceil();

      anchorEconomyQty = thermicCalc.ejotAnchorPerM2 - thermicCalc.otherAnchorPerM2;
      anchorEconomyMoney = roundDouble(
        otherQty * s.economyCostOtherAnchor - ejotQty * s.economyCostAnchor, 
        2 );
    }

    energyCostEconomyMoney = null;

    if (canCalculateEnergyCostEconomy()) {
      energyCostEconomyMoney = roundDouble( (thermicReport.otherAnnualLoss - thermicReport.ejotAnnualLoss) * s.economyCostEnergy, 2 );
    }

    insulatorCostEconomy = null;
    insulatorVolumeEjot = null;
    insulatorVolumeOther = null;

    if (canCalculateInsulatorEconomy()) {
      insulatorVolumeEjot = roundDouble( thermicReport.facadeSquareWithoutHoles * thermicCalc.calculatedInsulatorWidthEjot * 0.001, 2 );
      insulatorVolumeOther = roundDouble( thermicReport.facadeSquareWithoutHoles * thermicCalc.calculatedInsulatorWidthOther * 0.001, 2 );

      final double insulatorCostEconomyVolume = roundDouble( insulatorVolumeOther - insulatorVolumeEjot, 2);
      insulatorCostEconomy = roundDouble( s.economyCostInsulator * insulatorCostEconomyVolume, 0 );
    }

  }


}